# Webstudio
Official webclient for managing and organizing multiple [Odysseus](https://odysseus.informatik.uni-oldenburg.de/)-Server.

## Run with Docker
You need to have [Docker Compose](https://docs.docker.com/compose/install/) installed. It is shipped with [Docker Desktop](https://www.docker.com/products/docker-desktop).

### Run with official Images
You can run the Webstudio with the official images on [Docker Hub](https://hub.docker.com/). There are images for the [frontend](https://hub.docker.com/r/odysseusol/webstudio_frontend) and for the [backend](https://hub.docker.com/r/odysseusol/webstudio_backend).

You need to place the file [docker-compose.yml](https://git.swl.informatik.uni-oldenburg.de/projects/API_APPS/repos/webstudio/browse/deploy/docker-compose.yml) under `docker-compose.yml` on your filesystem. After that you can run `docker-compose up` from an terminal in the directory you placed the `docker-compose.yml`. This will download all necessary images and will create an docker network to run the Webstudio. After the startup you can access the Webstudio on `http://localhost:4200/`.

*Important*: When using webstudio with linux, you will need to create some folders BEFORE running docker-compose:

<pre>mkdir backend
chmod 777 backend
docker-compose pull
docker-compose up</pre>

If you checked out this repository you can also switch to folder deploy and just run `docker-compose up` from there.

*Remark*: You can also use the latest development versions. For this, change inside the docker-compose file the image tag from latest to dev-latest or use this file [docker-compose.yml](https://git.swl.informatik.uni-oldenburg.de/projects/API_APPS/repos/webstudio/browse/deploy-latest/docker-compose.yml) 

### Run in development
If you want to make changes to the source files and then run the Webstudio with Docker Compose, you just need to run `docker-compose up --build` from the root directory.
This will build the images for the backend and frontend based on the source files. You can edit the Dockerfiles for each and build your own images.
Please have a look at the [official docker documentation](https://docs.docker.com/) for further information about docker.

## Contribute information
The Webstudio consist of an backend and an frontend. See below for more informations on how to contribute.

### Prerequisites

#### Node.js
To contribute an develop you need to have [Node.js](https://nodejs.dev) installed. Its recommended to use the last long time support (LTS) release
but its also compatible with at least 18.14.x which is also an LTS release. Node.js is used to run the development server for frontend and backend. Node.js is shipped with the packet manager npm which is used to manage the dependecies of the Webstudio.

#### MongoDB
To persist the data the backend uses MongoDB. To install MongoDB locally follow the steps in the [official documentation](https://docs.mongodb.com/manual/administration/install-community/). The Webstudio was tested and developed with MongoDB 6 Community Edition.

You could also use the compose file inside the folder dev-mongo to install/start mongo for development

### Odysseus
In folder odysseus-example is an example for some Odysseus nodes that can be used for testing.

### Angular
You should install angular cli
npm install -g @angular/cli

### Frontend
The frontend was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.0.
Navigate to the folder `frontend` to execute the following commands.

#### Clear local storage
The frontend uses states which are persisted in the local storage of your browser. If you switch between Webstudio instances (eg. from production to development) you should clear the local storage of your browser to avoid errors.

#### Development server
The first time you have to run `npm install` to install the dependencies. If you use Windows you might have to run `npm install --unsafe-perm` if the process gets stuck.
If all dependencies are installed you can run `npm start` or `ng serve` for a dev server.
Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

#### Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

#### Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

#### Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

#### Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

#### Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### Backend
Navigate to the folder `backend` to execute the following commands.

#### Development server
The first time you have to run `npm install` to install the dependencies. If you use Windows you might have to run `npm install --unsafe-perm` if the process gets stuck.
If all dependencies are installed you can run `npm start` for a dev server. The app will automatically reload if you change any of the source files.

## License
Licensed under the Apache License, Version 2.0. For further information view [LICENSE](https://git.swl.informatik.uni-oldenburg.de/projects/API_APPS/repos/webstudio/browse/LICENSE)
