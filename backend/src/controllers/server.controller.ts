import { Request, Response } from 'express';

import * as ServerService from '../services/server.service';
import Server from '../models/server.model';
import logger from '../services/logger.service';
import { mapToServerDto } from '../assemblers/server.assembler';

export default class ServerController {
  findAllByOwner = async (req: Request, res: Response): Promise<void> => {
    try {
      const servers: Server[] = await ServerService.findAllByOwner(
        res.locals.user.id
      );
      servers.sort((a, b) => a.priority - b.priority);

      res
        .status(200)
        .json(servers.map((x) => mapToServerDto(x)))
        .end();
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };

  find = async (req: Request, res: Response): Promise<void> => {
    try {
      const server: Server = await ServerService.findByName(
        req.params.name,
        res.locals.user.id
      );

      if (server) {
        res.status(200).json(mapToServerDto(server)).end();
      } else {
        res.status(404).end();
      }
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };

  insert = async (req: Request, res: Response): Promise<void> => {
    try {
      const serverWithSameName: Server = await ServerService.findByName(
        req.body.name,
        res.locals.user.id
      );

      if (serverWithSameName) {
        res.status(409).end();
      } else {
        req.body.owner = res.locals.user.id;
        const response: Server = await ServerService.insert(req.body);
        res.status(201).json(mapToServerDto(response)).end();
      }
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };

  update = async (req: Request, res: Response): Promise<void> => {
    try {
      const server: Server = await ServerService.findByName(
        req.params.name,
        res.locals.user.id
      );

      if (server) {
        const serverNewSameName: Server = await ServerService.findByName(
          req.body.name,
          res.locals.user.id
        );

        if (serverNewSameName && serverNewSameName.name !== server.name) {
          res.status(409).end();
        } else {
          const response: Server = await ServerService.update(
            server._id.toHexString(),
            {
              _id: server._id,
              owner: server.owner,
              name: req.body.name,
              scheme: req.body.scheme,
              host: req.body.host,
              port: req.body.port,
              description: req.body.description,
              priority: req.body.priority
            }
          );
          res.status(200).json(mapToServerDto(response)).end();
        }
      } else {
        res.status(404).end();
      }
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };

  remove = async (req: Request, res: Response): Promise<void> => {
    try {
      const server: Server = await ServerService.findByName(
        req.params.name,
        res.locals.user.id
      );

      if (server) {
        await ServerService.remove(server._id.toHexString());
        res.status(204).end();
      } else {
        res.status(404).end();
      }
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };
}
