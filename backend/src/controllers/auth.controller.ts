import { Request, Response } from 'express';

import * as UserService from '../services/user.service';
import * as AuthService from '../services/auth.service';
import User from '../models/user.model';
import logger from '../services/logger.service';

export default class AuthController {
  obtainToken = async (req: Request, res: Response): Promise<void> => {
    try {
      const user: User = await UserService.findByUsername(req.body.username);

      const authenticated = await AuthService.verifyAuthorization(
        req.body.password,
        user.password
      );

      if (!authenticated) {
        res.status(400).end();
      }

      const response = await AuthService.createToken(user._id.toString());

      res.status(200).json(response).end();
    } catch (error) {
      if (!error.status) {
        logger.error(error);
        res.status(500).end();
      }

      res.status(error.status).end();
    }
  };

  userInfo = async (req: Request, res: Response): Promise<void> => {
    try {
      res.status(200).json(res.locals.user).end();
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };
}
