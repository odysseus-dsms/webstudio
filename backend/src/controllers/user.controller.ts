import { Request, Response } from 'express';

import * as userService from '../services/user.service';
import * as nodeService from '../services/node.service';
import User from '../models/user.model';
import logger from '../services/logger.service';
import { mapToUserDto } from '../assemblers/user.assembler';

export default class UserController {
  findAll = async (req: Request, res: Response): Promise<void> => {
    try {
      if (res.locals.user.role !== 'admin') {
        res.status(403).end();
      } else {
        const users: User[] = await userService.findAll();
        res
          .status(200)
          .send(users.map((x) => mapToUserDto(x)))
          .end();
      }
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };

  find = async (req: Request, res: Response): Promise<void> => {
    try {
      const user: User = await userService.findByUsername(req.params.username);

      if (user) {
        res.status(200).json(mapToUserDto(user)).end();
      } else {
        res.status(404).end();
      }
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };

  insert = async (req: Request, res: Response): Promise<void> => {
    try {
      const userWithSameUsername: User = await userService.findByUsername(
        req.params.username
      );

      if (userWithSameUsername) {
        res.status(409).end();
      } else {
        const user: User = await userService.insert(req.body);
        res.status(201).json(mapToUserDto(user)).end();
      }
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };

  update = async (req: Request, res: Response): Promise<void> => {
    try {
      const userWithSameUsername: User = await userService.findByUsername(
        req.body.username
      );

      if (
        userWithSameUsername &&
        userWithSameUsername.username !== req.params.username
      ) {
        res.status(409).end();
      } else {
        const user: User = await userService.update(
          req.params.username,
          req.body
        );
        res.status(200).json(mapToUserDto(user)).end();
      }
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };

  remove = async (req: Request, res: Response): Promise<void> => {
    try {
      let user = await userService.findByUsername(req.params.username);

      if (user) {
        await userService.remove(user._id.toHexString());
        await nodeService.removePath(user._id.toHexString());
      }

      res.status(200).end();
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };
}
