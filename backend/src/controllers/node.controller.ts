import { Request, Response } from 'express';

import * as nodeService from '../services/node.service';
import logger from '../services/logger.service';

export default class NodeController {
  create = async (req: Request, res: Response): Promise<void> => {
    try {
      let requestedPath = req.params[0].substring(1);
      let { serverName, path } = this.extractPath(requestedPath);
      const node: { name?: string; isDir?: boolean; copyOf: string } = req.body;

      if (node.copyOf) {
        res
          .status(201)
          .json(
            await nodeService.copyNode(
              res.locals.user.id,
              serverName,
              path,
              node.name,
              node.copyOf
            )
          )
          .end();
      } else {
        if (node.name == undefined) {
          throw { status: 400 };
        }

        if (path === '' || node.isDir == undefined) {
          node.isDir = true;
        }

        res
          .status(201)
          .json(
            await nodeService.create(
              res.locals.user.id,
              serverName,
              path,
              node.name,
              node.isDir
            )
          )
          .end();
      }
    } catch (error) {
      if (!error.status) {
        logger.error(error);
        res.status(500).end();
      }

      res.status(error.status).end();
    }
  };

  find = async (req: Request, res: Response): Promise<void> => {
    try {
      let requestedPath = req.params[0].substring(1);
      let { serverName, path } = this.extractPath(requestedPath);
      const parents = req.query.parents == 'true';

      if (req.get('Content-Type') === 'application/octet-stream') {
        const downloadPath = await nodeService.download(
          res.locals.user.id,
          serverName,
          path
        );

        res.sendFile(downloadPath, { dotfiles: 'allow' }, async () => {
          if (downloadPath.includes('.zip')) {
            await nodeService.removePath(downloadPath);
          }
        });
      } else {
        res
          .status(200)
          .json(
            await nodeService.find(
              res.locals.user.id,
              serverName,
              path,
              parents
            )
          )
          .end();
      }
    } catch (error) {
      if (!error.status) {
        logger.error(error);
        res.status(500).end();
      }

      res.status(error.status).end();
    }
  };

  update = async (req: Request, res: Response): Promise<void> => {
    try {
      let requestedPath = req.params[0].substring(1);
      let { serverName, path } = this.extractPath(requestedPath);

      res
        .status(200)
        .json(
          await nodeService.update(
            res.locals.user.id,
            serverName,
            path,
            req.body
          )
        )
        .end();
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };

  delete = async (req: Request, res: Response): Promise<void> => {
    try {
      let requestedPath = req.params[0].substring(1);
      let { serverName, path } = this.extractPath(requestedPath);

      await nodeService.remove(res.locals.user.id, serverName, path);
      res.status(200).end();
    } catch (error) {
      logger.error(error);
      res.status(500).end();
    }
  };

  /**
   * Transforms requestPath to workspace and path.
   * e.g. extractPath('/127.0.0.1/FirstSteps/queries') => { workspace: '127.0.0.1', path: 'FirstSteps/queries }
   */
  extractPath = function (requestPath: string): {
    serverName: string;
    path: string;
  } {
    let seperatorIndex = requestPath.indexOf('/');

    if (seperatorIndex === -1) {
      // workspace
      return { serverName: requestPath, path: '' };
    }

    let serverName = requestPath.substring(0, seperatorIndex);
    let path = requestPath.substring(seperatorIndex + 1);

    return { serverName, path };
  };
}
