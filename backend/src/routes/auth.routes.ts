import express from 'express';

import AuthController from '../controllers/auth.controller';
import { verifyToken } from '../middlewares/auth';

export default function setAuthRoutes(app) {
  const router = express.Router();

  const authCrtl = new AuthController();
  router.route('/token').post(authCrtl.obtainToken);
  router.route('/userinfo').get(verifyToken, authCrtl.userInfo);

  app.use('/auth', router);
}
