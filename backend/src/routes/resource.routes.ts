import express from 'express';

import UserController from '../controllers/user.controller';
import ServerController from '../controllers/server.controller';
import { verifyToken } from '../middlewares/auth';
import NodeController from '../controllers/node.controller';

export default function setResourceRoutes(app) {
  const router = express.Router();

  // Users
  const userCtrl = new UserController();
  router.route('/users').get(verifyToken, userCtrl.findAll);
  router.route('/users').post(verifyToken, userCtrl.insert);
  router.route('/users/:username').get(verifyToken, userCtrl.find);
  router.route('/users/:username').put(verifyToken, userCtrl.update);
  router.route('/users/:username').delete(verifyToken, userCtrl.remove);

  // Servers
  const serverCtrl = new ServerController();
  router.route('/servers').get(verifyToken, serverCtrl.findAllByOwner);
  router.route('/servers').post(verifyToken, serverCtrl.insert);
  router.route('/servers/:name').get(verifyToken, serverCtrl.find);
  router.route('/servers/:name').put(verifyToken, serverCtrl.update);
  router.route('/servers/:name').delete(verifyToken, serverCtrl.remove);

  // Nodes
  const nodeCtrl = new NodeController();
  router.route('/nodes*').post(verifyToken, nodeCtrl.create);
  router.route('/nodes*').get(verifyToken, nodeCtrl.find);
  router.route('/nodes*').patch(verifyToken, nodeCtrl.update);
  router.route('/nodes*').delete(verifyToken, nodeCtrl.delete);

  app.use('/v1', router);
}
