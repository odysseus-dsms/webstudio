import { getModelForClass } from '@typegoose/typegoose';
import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

import User from '../models/user.model';

const DefaultTransform = {
  schemaOptions: {
    toJSON: {
      virtuals: true,
      versionKey: false,
      transform: (doc, user, options) => {
        delete user.password;
        return user;
      },
    },
  },
};
const UserModel = getModelForClass(User, DefaultTransform);

export const findAll = async (): Promise<User[]> => {
  return UserModel.find().exec();
};

export const findById = async (id: string): Promise<User> => {
  return UserModel.findById(new mongoose.Types.ObjectId(id)).exec();
};

export const findByUsername = async (username: string): Promise<User> => {
  return UserModel.findOne({ username }).exec();
};

export const insert = async (user: User): Promise<User> => {
  user.password = await bcrypt.hash(user.password, 10);
  const obj = new UserModel(user);

  return obj.save();
};

export const update = async (username: string, user: User): Promise<User> => {
  if (user.password) {
    user.password = await bcrypt.hash(user.password, 10);
  } else {
    delete user.password;
  }

  return UserModel.findOneAndUpdate({ username: username }, user).exec();
};

export const remove = async (id: string): Promise<User> => {
  return UserModel.findOneAndRemove({ _id: id }).exec();
};
