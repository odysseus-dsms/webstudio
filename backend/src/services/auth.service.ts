import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import moment from 'moment';

export const verifyAuthorization = async (
  plainPassword: string,
  hashedPassword: string
): Promise<any> => {
  return bcrypt.compare(plainPassword, hashedPassword);
};

export const createToken = async (
  subject: string,
): Promise<any> => {
  const iat = moment();
  const exp = moment(iat).add(process.env.ACCESS_TOKEN_LIFESPAN, 'seconds');
  const typ = 'Bearer';
  const sub = subject;

  const access_token = jwt.sign(
    {
      sub,
      typ,
      iat: iat.unix(),
      exp: exp.unix(),
    },
    process.env.SECRET_TOKEN
  );

  return {
    access_token,
    token_type: typ,
    expires_at: exp.unix(),
  };
};