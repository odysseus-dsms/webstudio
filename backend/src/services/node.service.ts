import fs from 'fs-extra';
import pathUtils from 'path';
import util from 'util';
import rimraf from 'rimraf';
import archiver from 'archiver';
import { mapToNodeDto } from '../assemblers/node.assembler';

import { NodeDto } from '../dtos/node.dto';
import { Writable } from 'stream';

const mkdir = util.promisify(fs.mkdir);
const writeFile = util.promisify(fs.writeFile);
const readFile = util.promisify(fs.readFile);
const lstat = util.promisify(fs.lstat);
const readdir = util.promisify(fs.readdir);
const rename = util.promisify(fs.rename);
const copy = util.promisify(fs.copy);
const createWriteStream = util.promisify(fs.createWriteStream);

export const create = async (
  userId: string,
  serverId: string,
  path: string,
  name: string,
  isDir: boolean
): Promise<NodeDto> => {
  const hostFullPath = pathUtils.join(
    process.env.PROJECTS_FOLDER,
    userId,
    serverId,
    path,
    name
  );

  let successful = false;
  let isProject = path === '';

  if (isDir) {
    successful = await createFolder(hostFullPath);

    if (isProject) {
      // project

      let projectFilePath = pathUtils.join(hostFullPath, '.project');
      let templatePath = pathUtils.join(
        'resources',
        'templates',
        'project-template.xml'
      );

      let template = await findContent(templatePath);

      successful = await createFile(
        projectFilePath,
        template.replace('${name}', name)
      );
    }
  } else {
    successful = await createFile(hostFullPath);
  }

  if (!successful) {
    throw { status: 409 };
  }

  return Promise.resolve({
    name,
    isDir,
    children: isProject ? await findChildren(hostFullPath) : [],
  });
};

const createFolder = async (hostFullPath): Promise<boolean> => {
  const path = await mkdir(hostFullPath, {
    recursive: true,
  });

  // code EEXIST is natively supported and returns undefind
  return Promise.resolve(path != undefined);
};

const createFile = async (hostFullPath, data = ''): Promise<boolean> => {
  await writeFile(hostFullPath, data, { flag: 'wx' });
  return true;
};

export const find = async (
  userId: string,
  serverName: string,
  path: string,
  parents: boolean
): Promise<NodeDto> => {
  const hostFullPath = pathUtils.join(
    process.env.PROJECTS_FOLDER,
    userId,
    serverName,
    path
  );

  try {
    const fileStatus = await lstat(hostFullPath);

    let nodeData = new NodeDto(
      pathUtils.basename(path),
      fileStatus.isDirectory()
    );

    if (fileStatus.isDirectory()) {
      nodeData.children = await findChildren(hostFullPath);
    } else {
      nodeData.content = await findContent(hostFullPath);
    }

    let response;
    if (parents) {
      response = new NodeDto(serverName, true);
      response.children = await wrapParents(
        hostFullPath,
        path,
        nodeData.children,
        nodeData.content
      );
    } else {
      response = nodeData;
    }

    return Promise.resolve(response);
  } catch (error) {
    if (error.code === 'ENOENT') {
      if (path === '') {
        // workplace just not initiated, that's fine. Will happen when a project is created
        return Promise.resolve(new NodeDto('/', true, []));
      } else {
        throw { status: 404 };
      }
    }
  }
};

const findChildren = async (hostFullPath: string): Promise<NodeDto[]> => {
  const dirents = await readdir(hostFullPath, { withFileTypes: true });
  return Promise.resolve(
    sort(dirents.map((dirent) => mapToNodeDto(dirent, null, null)))
  );
};

const wrapParents = async (
  hostFullPath: string,
  path: string,
  children: NodeDto[],
  content: string
): Promise<NodeDto[]> => {
  const parentHostFullPath = pathUtils.join(hostFullPath, '../');
  const parentPath = pathUtils.join(path, '../');

  if (parentPath === '../') {
    // break when parent path is the workspace path
    return children;
  } else {
    const dirents = await readdir(parentHostFullPath, { withFileTypes: true });
    const nodeName = pathUtils.basename(path);

    // append parent nodes and go recursively up
    return wrapParents(
      parentHostFullPath,
      parentPath,
      sort(
        dirents.map((dirent) =>
          mapToNodeDto(
            dirent,
            dirent.name === nodeName ? children : null,
            dirent.name === nodeName ? content : null
          )
        )
      ),
      null
    );
  }
};

const sort = (dtos: NodeDto[]) => {
  return dtos
    .sort((a, b) => a.name.localeCompare(b.name))
    .sort((a, b) => (a.isDir === b.isDir ? 0 : a.isDir ? -1 : 1));
};

const findContent = async (hostFullPath): Promise<string> => {
  return await readFile(hostFullPath, 'utf8');
};

export const update = async (
  userId: string,
  serverName: string,
  path: string,
  data: { name?: string; path?: string; content?: string }
): Promise<NodeDto> => {
  const hostFullPath = pathUtils.join(
    process.env.PROJECTS_FOLDER,
    userId,
    serverName,
    path
  );

  const fileStatus = await lstat(hostFullPath);

  if ((!fileStatus.isDirectory() && data.content) || data.content === '') {
    await updateFile(hostFullPath, data.content);
  }

  // either rename or move
  if (data.name) {
    path = pathUtils.join(hostFullPath, '../', data.name);
    await rename(hostFullPath, path);
  } else if (data.path) {
    path = pathUtils.join(
      process.env.PROJECTS_FOLDER,
      userId,
      serverName,
      data.path
    );

    await rename(hostFullPath, path);
  }

  return Promise.resolve(
    new NodeDto(pathUtils.basename(path), fileStatus.isDirectory())
  );
};

export const copyNode = async (
  userId: string,
  serverName: string,
  path: string,
  name: string,
  copyOf: string
): Promise<NodeDto> => {
  const targetHostFullPath = pathUtils.join(
    process.env.PROJECTS_FOLDER,
    userId,
    serverName,
    path,
    name
  );

  const sourceHostFullPath = pathUtils.join(
    process.env.PROJECTS_FOLDER,
    userId,
    serverName,
    copyOf
  );

  try {
    const fileStatus = await lstat(sourceHostFullPath);

    await copy(sourceHostFullPath, targetHostFullPath);

    return Promise.resolve(
      new NodeDto(
        pathUtils.basename(targetHostFullPath),
        fileStatus.isDirectory()
      )
    );
  } catch (error) {
    if (error.code === 'ENOENT') {
      throw { status: 404 };
    }
  }
};

const updateFile = async (hostFullPath, data): Promise<boolean> => {
  await writeFile(hostFullPath, data, { flag: 'w' });
  return true;
};

export const removePath = async (hostFullPath: string): Promise<boolean> => {
  rimraf(hostFullPath, (err) => {
    if (err) {
      throw err;
    }
  });
  return Promise.resolve(true);
};

export const remove = async (
  userId: string,
  serverName: string,
  path: string
): Promise<boolean> => {
  const hostFullPath = pathUtils.join(
    process.env.PROJECTS_FOLDER,
    userId,
    serverName,
    path
  );

  return removePath(hostFullPath);
};

export const getDownloadStream = async (
  userId: string,
  serverName: string,
  path: string
): Promise<Writable> => {
  const hostFullPath = pathUtils.join(
    process.env.PROJECTS_FOLDER,
    userId,
    serverName,
    path
  );

  try {
    const fileStatus = await lstat(hostFullPath);
    if (fileStatus.isDirectory()) {
    }

    return createWriteStream(hostFullPath);
  } catch (error) {}
};

export const download = async (
  userId: string,
  serverName: string,
  path: string
): Promise<string> => {
  const hostFullPath = pathUtils.join(
    process.env.PROJECTS_FOLDER,
    userId,
    serverName,
    path
  );

  return new Promise(async (resolve, reject) => {
    try {
      const stats = await lstat(hostFullPath);

      if (stats.isDirectory()) {
        const zipPath = hostFullPath + '.zip';
        const output = fs.createWriteStream(zipPath);
        const archive = archiver('zip');

        output.on('close', () => {
          resolve(pathUtils.resolve(zipPath));
        });
        archive.on('error', (error) => {
          throw error;
        });
        archive.pipe(output);
        archive.directory(hostFullPath, false);
        archive.finalize();
      } else {
        resolve(pathUtils.resolve(hostFullPath));
      }
    } catch (error) {
      reject(error);
    }
  });
};
