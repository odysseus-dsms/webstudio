import { getModelForClass } from '@typegoose/typegoose';

import Server from '../models/server.model';

const ServerModel = getModelForClass(Server);

export const findAllByOwner = async (owner: string): Promise<Server[]> => {
  return ServerModel.find({ owner }).exec();
};

export const find = async (id: string): Promise<Server> => {
  return ServerModel.findOne({ _id: id }).exec();
};

export const insert = async (server: Server): Promise<Server> => {
  const obj = new ServerModel(server);
  return obj.save();
};

export const update = async (id: string, server: Server): Promise<Server> => {
  return ServerModel.findOneAndUpdate({ _id: id }, server).exec();
};

export const remove = async (id: string): Promise<Server> => {
  return ServerModel.findOneAndRemove({ _id: id }).exec();
};

export const findByName = async (
  name: string,
  owner: string
): Promise<Server> => {
  return ServerModel.findOne({ name, owner }).exec();
};
