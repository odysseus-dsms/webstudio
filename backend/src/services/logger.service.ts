import winston from 'winston';
import mkdirp from 'mkdirp';
import path from 'path';
import fs, { WriteStream } from 'fs';

const logger = winston.createLogger({
  exitOnError: false,
  level: 'info',
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.printf(info => `${info.timestamp} ${info.level.toUpperCase()}: ${info.message}`)
  ),
  transports: [
      new (winston.transports.File)({ filename: './logs/app.log'}),
      new (winston.transports.Console)()
  ]
});
export default logger;

export const getRequestLogStream = (): WriteStream => {
  let requestLogStream = fs.createWriteStream(path.normalize('./logs/request.log'), { flags: 'a' });
  requestLogStream.on('error', async () => {
    await mkdirp(path.normalize('./logs'));
    requestLogStream = fs.createWriteStream(path.normalize('./logs/request.log'), { flags: 'a' });
  });

  return requestLogStream;
}
