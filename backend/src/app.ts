import { getModelForClass } from '@typegoose/typegoose';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import morgan from 'morgan';
import helmet from 'helmet';
import bcrypt from 'bcrypt';

import logger, { getRequestLogStream } from './services/logger.service';
import User from './models/user.model';
import setResourceRoutes from './routes/resource.routes';
import setAuthRoutes from './routes/auth.routes';

const app = express();
dotenv.config();

app.use(cors())

app.use(helmet());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(morgan('combined', { stream: getRequestLogStream() }));

(async () => {
  try {
    await mongoose.connect(process.env.MONGODB_URI);
    logger.info('Connected to ' + process.env.MONGODB_URI);

    const UserModel = getModelForClass(User);
    UserModel.countDocuments({}, async (error, docCount) => {
      if (error) {
        logger.error(error);
      }
      if (docCount === 0) {
        const defaultUser = new UserModel({
          username: 'System',
          email: 'odysseus@uni-oldenburg.de',
          password: 'manager',
          role: 'admin'
        })
        defaultUser.password = await bcrypt.hash(defaultUser.password, 10);
        defaultUser.save();
      }
    })

    setAuthRoutes(app);
    setResourceRoutes(app);

    app.listen(3000, () => {
      logger.info('WebStudio server listening on port 3000');
    });
  } catch (error) {
    logger.error(error);
  }
})()

export { app };
