import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

import * as UserService from '../services/user.service';
import User from '../models/user.model';

export const verifyToken = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let token = req.headers.authorization;

  if (token && token.startsWith('Bearer ')) {
    token = token.split(' ')[1];

    jwt.verify(token, process.env.SECRET_TOKEN, async (error, claims) => {
      if (error) {
        return res.status(401).json({
          result: false,
          message: 'The token is invalid',
        });
      }

      res.locals.user = await UserService.findById(claims['sub'].toString());
      return next();
    });
  } else {
    return res.status(400).json({
      result: false,
      message: 'The token is missing.',
    });
  }
};
