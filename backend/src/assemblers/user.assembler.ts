import { UserDto } from '../dtos/user.dto';
import User from '../models/user.model';

export function mapToUserDto(entity: User): UserDto {
  return new UserDto(entity.username, entity.email, entity.role);
}
