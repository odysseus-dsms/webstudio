import { Dirent } from 'fs';
import { NodeDto } from '../dtos/node.dto';

export function mapToNodeDto(
  dirent: Dirent,
  children: NodeDto[],
  content: string
): NodeDto {
  return new NodeDto(dirent.name, dirent.isDirectory(), children, content);
}
