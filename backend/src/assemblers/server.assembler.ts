import { ServerDto } from '../dtos/server.dto';
import Server from '../models/server.model';

export function mapToServerDto(entity: Server): ServerDto {
  return new ServerDto(
    entity.name,
    entity.scheme,
    entity.host,
    entity.port,
    entity.description,
    entity.priority
  );
}
