export default interface IFileElement {
  id?: string;
  isDir: boolean;
  name: string;
  parent: string;
  type?: string;
}
