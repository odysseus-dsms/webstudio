import { Ref, prop } from '@typegoose/typegoose';
import mongoose from 'mongoose';

import User from './user.model';

export default class Server {
  _id?: mongoose.Types.ObjectId;

  @prop({ required: true })
  name!: string;

  @prop({ required: true })
  scheme!: string;

  @prop({ required: true })
  host!: string;

  @prop({ ref: () => User, required: true })
  owner!: Ref<User>;

  @prop()
  port?: number;

  @prop()
  description?: string;

  @prop()
  priority?: number;
}
