import { prop } from '@typegoose/typegoose';
import mongoose from 'mongoose';

export default class User {
  _id?: mongoose.Types.ObjectId;

  @prop({ required: true, unique: true })
  username!: string;

  @prop({ required: true })
  email!: string;

  @prop({ required: true })
  password!: string;

  @prop({ required: true })
  role!: string;
}
