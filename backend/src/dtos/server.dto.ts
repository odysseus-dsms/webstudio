export class ServerDto {
  constructor(
    public name: string,
    public scheme: string,
    public host: string,
    public port?: number,
    public description?: string,
    public priority?: number
  ) {}
}
