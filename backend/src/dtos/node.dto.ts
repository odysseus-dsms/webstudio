export class NodeDto {
  constructor(
    public name: string,
    public isDir: boolean,
    public children?: NodeDto[],
    public content?: string
  ) {}
}
