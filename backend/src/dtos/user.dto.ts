export class UserDto {
  constructor(
    public username: string,
    public email: string,
    public role: string
  ) {}
}
