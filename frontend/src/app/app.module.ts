import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsModule } from '@ngxs/store';
import { MonacoEditorModule } from 'ngx-monaco-editor-v2';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { SharedModule } from './shared/shared.module';
import { NavigationComponent } from './navigation/navigation.component';
import { ServerModule } from './+servers/server.module';
import { TokenInterceptor } from './util/interceptors/token-interceptor.service';
import { ErrorInterceptorService } from './util/interceptors/error-interceptor.service';
import { MessageService } from './shared/services/message.service';
import { DialogModule } from './shared/dialogs/dialog.module';
import { AuthModule } from './+auth/auth.module';
import { LoggingModule } from './+loggings/logging.module';
import { QueryModule } from './+queries/query.module';
import { StreamModule } from './+streams/stream.module';
import { SinkModule } from './+sinks/sink.module';
import { ListModule } from './+lists/list.module';
import { ErrorService } from './shared/services/error.service';
import { IdeModule } from './+ide/ide.module';
import { MonitoringModule } from './+monitoring/monitoring.module';
import { AngularSplitModule } from 'angular-split';

import { editorConfig } from './+ide/shared/configs/editor.config';
import { ServerState } from './shared/states/server.state';
import { NodeService } from './shared/services/api/webstudio/node.service';
import { ParserService } from './shared/services/api/odysseus-core/parser.service';
import { OperatorService } from './shared/services/api/odysseus-core/operator.service';
import { AuthService } from './shared/services/api/auth.service';
import { AuthGuard } from './util/guards/auth.guard';
import { LoggingService } from './shared/services/api/odysseus-core/logging.service';
import { QueryService } from './shared/services/api/odysseus-core/query.service';
import { WebSocketService } from './shared/services/api/odysseus-core/websocket.service';
import { ServerService } from './shared/services/api/webstudio/server.service';
import { ServicesService } from './shared/services/api/odysseus-core/services.service';
import { ServerGuard } from './util/guards/odysseus.guard';
import { SinkService } from './shared/services/api/odysseus-core/sink.service';
import { StreamService } from './shared/services/api/odysseus-core/stream.service';
import { UserService } from './shared/services/api/webstudio/user.service';
import { AdminGuard } from './util/guards/admin.guard';
import { DatatypeService } from './shared/services/api/odysseus-core/datatype.service';
import { FunctionService } from './shared/services/api/odysseus-core/functions.service';
import { NgChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [AppComponent, NavigationComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LayoutModule,
    ReactiveFormsModule,

    NgxsModule.forRoot([], { developmentMode: !environment.production }),
    NgxsStoragePluginModule.forRoot({
      key: [ServerState],
    }),
    !environment.production ? NgxsLoggerPluginModule.forRoot() : [],
    !environment.production ? NgxsReduxDevtoolsPluginModule.forRoot() : [],
    MonacoEditorModule.forRoot(editorConfig),
    AngularSplitModule,
    DragDropModule,

    SharedModule,
    AuthModule,
    DialogModule,
    ServerModule,
    IdeModule,
    MonitoringModule,
    LoggingModule,
    QueryModule,
    StreamModule,
    SinkModule,
    ListModule,
    NgChartsModule
  ],
  exports: [DialogModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptorService,
      multi: true,
    },

    DatatypeService,
    FunctionService,
    LoggingService,
    ServicesService,
    OperatorService,
    ParserService,
    QueryService,
    SinkService,
    StreamService,

    AuthService,
    NodeService,
    ServerService,
    UserService,

    ErrorService,
    MessageService,
    WebSocketService,

    AuthGuard,
    ServerGuard,
    AdminGuard,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
