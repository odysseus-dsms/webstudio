import { Component, OnInit } from '@angular/core';
import { Router, RouterOutlet } from '@angular/router';
import { Store } from '@ngxs/store';
import { routerAnimation } from './util/animations/router.animation';
import { AuthService } from './shared/services/api/auth.service';
import { AuthRememberMe } from './shared/states/auth.state';
import { Shape } from '@antv/x6';
import { edgeConfig, rectConfig } from './util/graph.utils';
import { Chart } from 'chart.js';
import 'chartjs-adapter-luxon';
import StreamingPlugin from 'chartjs-plugin-streaming';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  animations: [routerAnimation],
})
export class AppComponent implements OnInit {
  constructor(
    private _store: Store,
    private _router: Router,
    private _authService: AuthService
  ) {
    Shape.Rect.config(rectConfig);
    Shape.Edge.config(edgeConfig);
    Chart.register(StreamingPlugin);
  }

  ngOnInit() {
    const tokenResponse = this._authService.restoreTokenResponseIfValid();

    if (tokenResponse) {
      const username = this._authService.restoreUsername()
      this._store.dispatch(new AuthRememberMe({
        tokenResponse,
        username
      }));
    } else {
      this._router.navigate(['login']);
    }
  }

  prepareRoute(outlet: RouterOutlet) {
    return (
      outlet && outlet.activatedRouteData && outlet.activatedRouteData.animation
    );
  }
}
