import { Component, Inject } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { ServerDto } from 'app/shared/dto/server/server.dto';

import { ServerDialogValidator } from './server-dialog.validator';

@Component({
  selector: 'app-server-dialog',
  templateUrl: './server-dialog.component.html',
  providers: [ServerDialogValidator],
})
export class ServerDialogComponent {
  serverDto: ServerDto;
  mode: string;

  form: UntypedFormGroup;

  constructor(
    public dialogRef: MatDialogRef<ServerDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { serverDto: ServerDto; mode: string },
    private readonly _formBuilder: UntypedFormBuilder,
    private _serverValidator: ServerDialogValidator
  ) {
    this.serverDto = data.serverDto;
    this.mode = data.mode;

    this.form = this._formBuilder.group({
      scheme: [
        this.serverDto.scheme ? this.serverDto.scheme : 'http',
        [Validators.required],
      ],
      name: [
        this.serverDto.name,
        {
          validators: [Validators.required],
          asyncValidators: [
            this._serverValidator.nameExists(this.serverDto.name),
          ],
        },
      ],
      host: [this.serverDto.host, [Validators.required]],
      port: [this.serverDto.port],
      description: [this.serverDto.description],
      owner: [this.serverDto.owner],
      priority: [this.serverDto.priority]
    });
  }

  public cancel(): void {
    this.dialogRef.close();
  }

  public save(): void {
    if (this.form.dirty && this.form.valid) {
      this.dialogRef.close(this.form.getRawValue());
    }
  }
}
