import { Injectable } from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  ValidationErrors,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ServerService } from '../../shared/services/api/webstudio/server.service';

@Injectable()
export class ServerDialogValidator {
  constructor(private _serverService: ServerService) {}

  nameExists(originalName: string | null): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this._serverService.hasName(control.value).pipe(
        map((res) => {
          if (originalName === control.value) {
            return null;
          }
          return res ? { nameExists: true } : null;
        })
      );
    };
  }
}
