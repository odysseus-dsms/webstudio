import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { Observable } from 'rxjs';

import { MessageService } from 'app/shared/services/message.service';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { ServerLogin, ServerState } from 'app/shared/states/server.state';
import { Connection } from 'app/shared/domain/connection.domain';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent {
  form: UntypedFormGroup;
  serverDto: ServerDto;
  // TODO: What is this for?
  loginError$: Observable<string>;
  loginErrorMessage: string = undefined;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ServerDto,
    public dialogRef: MatDialogRef<LoginComponent>,
    private _formBuilder: UntypedFormBuilder,
    private _store: Store,
    private _messageService: MessageService
  ) {
    this.serverDto = data;
    this.form = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.loginError$ = _store.select(
      ServerState.loginError(
        this.serverDto.name,
        this._store.selectSnapshot(AuthState.username)
      )
    );
  }

  public login(): void {
    if (this.form.touched && this.form.valid) {
      this._store
        .dispatch(new ServerLogin(this.serverDto, { ...this.form.value }))
        .subscribe(
          {next: (res) => {
          const login = res.servers.entities.find(
            (x: Connection) => x.serverDto.name === this.serverDto.name
          );
          if (login.loginError === null) {
            this.dialogRef.close();
            this._messageService.showMessage('Your login was successful.');
          }
        },
      error:(err) => {
        if ((err as HttpErrorResponse).status === 400){
          this.loginErrorMessage = "Wrong username or password."
        }else{
          this.loginErrorMessage = "Login failed."
        }
      }});
    }
  }

  public cancel(): void {
    this.dialogRef.close();
  }
}
