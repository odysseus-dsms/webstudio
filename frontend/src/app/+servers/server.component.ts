import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component, OnDestroy, OnInit, ViewChild, Pipe } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatLegacyPaginator as MatPaginator } from '@angular/material/legacy-paginator';
import { MatLegacyTableDataSource as MatTableDataSource } from '@angular/material/legacy-table';
import { EMPTY, forkJoin, interval, Observable, ObservedValueOf, of, Subject, Subscription, timer } from 'rxjs';
import {
  catchError,
  filter,
  mergeMap,
  switchMap,
  take,
  takeUntil,
} from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { DialogService } from 'app/shared/dialogs/dialog.service';
import { MessageService } from 'app/shared/services/message.service';
import { ServerService } from '../shared/services/api/webstudio/server.service';
import { ServerDialogComponent } from './server-dialog/server-dialog.component';
import { AuthState } from 'app/shared/states/auth.state';
import { ServerDtoUpdate, ServerLogout, ServerState, ServerStatusUpdate, ServerListUpdate } from 'app/shared/states/server.state';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { LoginComponent } from './login-dialog/login.component';
import { ServerListItem } from 'app/shared/domain/server-list-item.domain';
import { ServicesService } from 'app/shared/services/api/odysseus-core/services.service';
import { AuthService } from 'app/shared/services/api/auth.service';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { first, map, tap } from 'rxjs/operators';


@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.scss'],
})
export class ServerComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = [
    'priority',
    'name',
    'status',
    'scheme',
    'host',
    'port',
    'description',
    'actions',
  ];
  dataSource = new MatTableDataSource<ServerListItem>();
  defaultServer: string;

  private stopPolling$ = new Subject<void>();

  private updateServerList : Subscription;

  constructor(
    public dialog: MatDialog,
    private _store: Store,
    private _dialogService: DialogService,
    private _messageService: MessageService,
    private _serverService: ServerService,
    private _servicesService: ServicesService,
    private _authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.updateServers();
    this.updateServerList = interval(10000).subscribe(
      () => this.updateServers()
    );


    this.checkOnlineState();

    const user = this._store.selectSnapshot(AuthState.username);
    this._store.select(ServerState.getActiveConnections(user)).subscribe(connections => {
      this.defaultServer = connections.length > 0 ? connections[0].serverDto.name : null;
    });


  }

  ngOnDestroy() {
    this.stopPolling$.next();
    this.updateServerList.unsubscribe();
  }

  private updateServers(): void {
    const user = this._store.selectSnapshot(AuthState.username);
    this._serverService.getAll().subscribe(
      (servers) => {

        this._store.dispatch(new ServerListUpdate(servers))

        this.stopPolling$.next();
        this.dataSource.paginator = this.paginator;

        this.dataSource.data = servers.map((x) => {
          return {
            serverDto: x,
            online: null,
          };
        });

        this.checkOnlineState();
      }
    );
  }

  private checkOnlineState(){
    this.dataSource.data.forEach((x) =>
          timer(1, 5000)
            .pipe(
              //tap(() => console.log("Updating "+x)),
              mergeMap(() => {
                return this._servicesService.options(x.serverDto).pipe(
                  mergeMap((_) => of(true)),
                  catchError((_) => of(false))
                );
              }),
              takeUntil(this.stopPolling$)
            )
            .subscribe({
              next: (online) => {
                if (x.online !== online) {
                  x.online = online;
                  this._store.dispatch(new ServerStatusUpdate(x.serverDto, online));

                  if (online) {
                    this._authService.renewInvalidLoginSessions();
                  }
                }
              },
              error: (error) => {
                console.log("Check online state ",error);
              }
            })
        );
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public createServer(): void {
    this.ensureDialog(
      {
        name: '',
        host: '',
        scheme: '',
        owner: this._store.selectSnapshot(AuthState.username),
        priority: this.dataSource.data.length + 1
      },
      'create'
    ).subscribe((serverDto: ServerDto) =>
      this._serverService.create(serverDto).subscribe(() => {
        this.updateServers();
        this._messageService.showMessage(
          'The server was created successfully.'
        );
      })
    );
  }

  public editServer(serverDto: ServerDto): void {
    const name = serverDto.name;
    this.ensureDialog(serverDto, 'edit').subscribe((serverDto: ServerDto) =>
      this._serverService.edit(name, serverDto).subscribe(() => {
        this.updateServers();
        this._store.dispatch(new ServerDtoUpdate(serverDto, name));

        let msg = 'The server was edited successfully.';
        if (
          (serverDto.host !== serverDto.host ||
            serverDto.port !== serverDto.port) &&
          this._store.selectSnapshot(ServerState.getTokenByName(
            serverDto.name,
            this._store.selectSnapshot(AuthState.username)
          ))
        ) {
          this._store.dispatch(new ServerLogout(serverDto));
          msg += ' You have been logged out of this server.';
        }
        this._messageService.showMessage(msg);
      })
    );
  }

  public deleteServer(serverDto: ServerDto): void {
    this._dialogService
      .confirm('Delete ' + serverDto.name, 'Do you want to continue?')
      .subscribe((res) => {
        if (res) {
          this._serverService.delete(serverDto.name).subscribe(() => {
            this.updateServers();
            this._store.dispatch(new ServerLogout(serverDto));
            this._messageService.showMessage(
              'The server was deleted successfully.'
            );
          });
        }
      });
  }

  public loginServer(serverDto: ServerDto): void {
    this.dialog.open<LoginComponent, ServerDto>(LoginComponent, {
      width: '410px',
      data: serverDto,
    });
  }

  public logoutServer(serverDto: ServerDto): void {
    this._dialogService
      .confirm('Logout from ' + serverDto.name, 'Do you want to continue?')
      .subscribe((res) => {
        if (res) {
          this._store.dispatch(new ServerLogout(serverDto));
          this._messageService.showMessage('Your logout was successful.');
        }
      });
  }

  public hasToken(name: string): Observable<string> {
    return this._store.select(ServerState.getTokenByName(
      name,
      this._store.selectSnapshot(AuthState.username)
    ));
  }

  public serverClicked(serverDto: ServerDto): void {
    console.log("Clicked on server " + serverDto.name + " " + serverDto.host + " " + this.hasToken(serverDto.name));
    this.hasToken(serverDto.name)
      .pipe(take(1))
      .subscribe(t => {
        if (!t) {
          this.loginServer(serverDto);
        } else {
          this.router.navigate(['/servers', serverDto.name]);
        }
      })

  }

  onListDrop(event: CdkDragDrop<ServerListItem[]>) {
    const previousIndex = this.dataSource.data.findIndex(row => row === event.item.data);
    if (previousIndex === event.currentIndex) {
      return;
    }

    moveItemInArray(this.dataSource.data, previousIndex, event.currentIndex);
    const updateRequests = [];
    this.dataSource.data.forEach((element, index) => {
      const serverDto = { ...element.serverDto };

      if (serverDto.priority === index + 1) {
        return;
      }

      serverDto.priority = index + 1;
      element.serverDto = serverDto;
      updateRequests.push(
        this._serverService.edit(serverDto.name, serverDto).subscribe()
      );
      this._store.dispatch(new ServerDtoUpdate(serverDto, serverDto.name));
    });

    forkJoin([updateRequests]).subscribe(() => {
      this._messageService.showMessage('The priority was updated successfully');
      this.dataSource.data = this.dataSource.data
    });
  }

  private ensureDialog(
    serverDto: ServerDto,
    mode: string
  ): Observable<ServerDto> {
    return this.dialog
      .open<ServerDialogComponent, any>(ServerDialogComponent, {
        width: '590px',
        data: { serverDto, mode },
      })
      .afterClosed()
      .pipe(filter((serverDto: ServerDto): boolean => !!serverDto));
  }
}
