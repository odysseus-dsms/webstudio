import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ServerComponent } from './server.component';
import { ServerDialogComponent } from './server-dialog/server-dialog.component';
import { LoginComponent } from './login-dialog/login.component';
import { NgxsModule } from '@ngxs/store';
import { ServerState } from 'app/shared/states/server.state';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [ServerComponent, ServerDialogComponent, LoginComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: ServerComponent }]),
    SharedModule,
    NgxsModule.forFeature([ServerState]),
  ],
})
export class ServerModule {}
