import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

import { LoggingService } from '../shared/services/api/odysseus-core/logging.service';
import { Logging } from '../shared/domain/logging.domain';
import { ServerState } from 'app/shared/states/server.state';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-log',
  templateUrl: './logging.component.html',
  styleUrls: ['./logging.component.scss'],
})
export class LoggingComponent implements OnInit {
  serverLogs: Logging[];

  constructor(private _store: Store, private _logService: LoggingService) {}

  ngOnInit(): void {
    this.serverLogs = [];

    const connections = this._store.selectSnapshot(ServerState.getActiveConnections(
      this._store.selectSnapshot(AuthState.username)
    ));
    connections.forEach((x) => {
      this._logService.getAll(x.serverDto).subscribe((response) => {
        this.serverLogs.push({
          serverDto: x.serverDto,
          logContent: response.slice(response.length - 100000, response.length)
        });
        this.serverLogs.sort((a, b) => a.serverDto.priority - b.serverDto.priority);
      });
    });
  }
}
