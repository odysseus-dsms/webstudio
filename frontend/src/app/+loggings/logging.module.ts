import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import { LoggingComponent } from './logging.component';

@NgModule({
  declarations: [LoggingComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: LoggingComponent }]),
    SharedModule,
  ],
})
export class LoggingModule {}
