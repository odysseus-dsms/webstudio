import { Component, OnInit } from '@angular/core';

import { DialogService } from 'app/shared/dialogs/dialog.service';
import { MessageService } from 'app/shared/services/message.service';
import { StreamService } from '../shared/services/api/odysseus-core/stream.service';
import { Resource } from '../shared/models/resource.model';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { Store } from '@ngxs/store';
import { ServerState } from 'app/shared/states/server.state';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-stream',
  templateUrl: './stream.component.html',
  styleUrls: ['./stream.component.scss'],
})
export class StreamComponent implements OnInit {
  serverStreams: { serverDto: ServerDto; streams: Resource[] }[];

  constructor(
    private _store: Store,
    private _dialogService: DialogService,
    private _messageService: MessageService,
    private _streamService: StreamService
  ) {}

  ngOnInit(): void {
    this.updateStreams();
  }

  protected updateStreams(): void {
    this.serverStreams = [];

    let connections = this._store.selectSnapshot(ServerState.getActiveConnections(
      this._store.selectSnapshot(AuthState.username)
    ));
    connections.forEach((x) => {
      this._streamService
        .getAll(x.serverDto)
        .subscribe((response) => {
          this.serverStreams.push({ serverDto: x.serverDto, streams: response });
          this.serverStreams.sort((a, b) => a.serverDto.priority - b.serverDto.priority);
        });
    });
  }

  delete(event: { serverDto: ServerDto; stream: Resource }): void {
    this._dialogService
      .confirm('Delete ' + event.stream.name, 'Do you want to continue?')
      .subscribe((res) => {
        if (res) {
          this._streamService
            .delete(event.serverDto, event.stream)
            .subscribe(() => {
              this.updateStreams();
              this._messageService.showMessage(
                'The stream was deleted successfully.'
              );
            });
        }
      });
  }
}
