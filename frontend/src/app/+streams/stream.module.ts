import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import { StreamComponent } from './stream.component';
import { StreamTableComponent } from './table/stream-table.component';

@NgModule({
  declarations: [StreamComponent, StreamTableComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: StreamComponent }]),
    SharedModule,
  ],
})
export class StreamModule {}
