import {
  Component,
  OnInit,
  AfterViewInit,
  Input,
  Output,
  ViewChild,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatLegacyPaginator as MatPaginator } from '@angular/material/legacy-paginator';
import { MatSort } from '@angular/material/sort';
import { MatLegacyTableDataSource as MatTableDataSource } from '@angular/material/legacy-table';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Resource } from '../../shared/models/resource.model';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { WebSocketService } from 'app/shared/services/api/odysseus-core/websocket.service';
import { ServicesService } from 'app/shared/services/api/odysseus-core/services.service';

@Component({
  selector: 'app-stream-table',
  templateUrl: 'stream-table.component.html',
  styleUrls: ['stream-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*', marginBottom: '5px' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class StreamTableComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Output() streamDeleted = new EventEmitter<{
    serverDto: ServerDto;
    stream: Resource;
  }>();
  @Output() dataDictionaryEventReceived = new EventEmitter();

  @Input() serverDto: ServerDto;
  @Input() data: Resource[];

  displayedColumns: string[] = ['name', 'owner', 'actions'];
  expandedElement: Resource | null;
  dataSource = new MatTableDataSource<Resource>();
  destroyed$ = new Subject<void>();

  constructor(
    private _servicesService: ServicesService,
    private _webSocketService: WebSocketService
  ) {}

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.data);
    this.createEventConnection();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  createEventConnection(): void {
    this._servicesService
      .events(this.serverDto)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((event) => {
        this._webSocketService
          .connectToDataDictionaryEvent(this.serverDto, event)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            this.dataDictionaryEventReceived.emit();
          });
      });
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  delete(stream: Resource): void {
    this.streamDeleted.emit({ serverDto: this.serverDto, stream });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
