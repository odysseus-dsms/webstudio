import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import { SinkComponent } from './sink.component';
import { SinkTableComponent } from './table/sink-table.component';

@NgModule({
  declarations: [SinkComponent, SinkTableComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: SinkComponent }]),
    SharedModule,
  ],
})
export class SinkModule {}
