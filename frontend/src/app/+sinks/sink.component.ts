import { Component, OnInit } from '@angular/core';

import { DialogService } from 'app/shared/dialogs/dialog.service';
import { MessageService } from 'app/shared/services/message.service';
import { SinkService } from '../shared/services/api/odysseus-core/sink.service';
import { Resource } from '../shared/models/resource.model';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { Store } from '@ngxs/store';
import { ServerState } from 'app/shared/states/server.state';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-sink',
  templateUrl: './sink.component.html',
  styleUrls: ['./sink.component.scss'],
})
export class SinkComponent implements OnInit {
  serverSinks: { serverDto: ServerDto; sinks: Resource[] }[];

  constructor(
    private _store: Store,
    private _dialogService: DialogService,
    private _messageService: MessageService,
    private _sinkService: SinkService
  ) {}

  ngOnInit(): void {
    this.updateSinks();
  }

  protected updateSinks(): void {
    this.serverSinks = [];

    const connections = this._store.selectSnapshot(ServerState.getActiveConnections(
      this._store.selectSnapshot(AuthState.username)
    ));

    connections.forEach((x) => {
      this._sinkService
        .getAll(x.serverDto)
        .subscribe((response) => {
          this.serverSinks.push({ serverDto: x.serverDto, sinks: response });
          this.serverSinks.sort((a, b) => a.serverDto.priority - b.serverDto.priority);
      });
    });
  }

  delete(event: { serverDto: ServerDto; sink: Resource }): void {
    this._dialogService
      .confirm('Delete ' + event.sink.name, 'Do you want to continue?')
      .subscribe((res) => {
        if (res) {
          this._sinkService
            .delete(event.serverDto, event.sink)
            .subscribe(() => {
              this.updateSinks();
              this._messageService.showMessage(
                'The sink was deleted successfully.'
              );
            });
        }
      });
  }
}
