import { AfterViewInit, Component, Inject, Input, ViewChild, ElementRef } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { stringValidator } from 'app/shared/validators/user-dialog.validator';

import { NodeData } from '../../../shared/domain/node-data';

@Component({
  selector: 'create-node-explorer-dialog',
  templateUrl: './create-node-dialog.component.html',
  styleUrls: ['./create-node-dialog.component.scss'],
})
export class CreateNodeDialogComponent implements AfterViewInit{
  form: UntypedFormGroup;

  @ViewChild('fileInputField') fileInputField:ElementRef;

  constructor(
    private dialogRef: MatDialogRef<CreateNodeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private formBuilder: UntypedFormBuilder
  ) {
    let nodeData: NodeData = this.data.nodeData;
    let existingNodes: NodeData[] = this.data.existingNodes
      ? this.data.existingNodes
      : [];

    this.form = this.formBuilder.group({
      name: [
        nodeData.name,
        [
          Validators.required,
          stringValidator(existingNodes.map((x) => x.name)),
        ],
      ],
      path: [nodeData.path],
      isDir: [nodeData.isDir],
    });
  }

  ngAfterViewInit(): void {
    console.log(this.fileInputField.nativeElement.value);
    this.fileInputField.nativeElement.setSelectionRange(0, 0);
  }

  public onCancelClicked(): void {
    this.dialogRef.close();
  }

  public onSaveClicked(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    } else {
      this.form.controls.name.markAsTouched();
    }
  }

  isProject() {
    return (
      this.form.controls.isDir.value && this.form.controls.path.value === '/'
    );
  }

  isFolder() {
    return (
      this.form.controls.isDir.value && this.form.controls.path.value !== '/'
    );
  }

  isFile() {
    return !this.form.controls.isDir.value;
  }

}
