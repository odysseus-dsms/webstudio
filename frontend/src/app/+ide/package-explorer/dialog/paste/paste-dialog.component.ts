import { Component, Inject } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { stringValidator } from 'app/shared/validators/user-dialog.validator';

import { NodeData } from '../../../shared/domain/node-data';

@Component({
  selector: 'paste-explorer-dialog',
  templateUrl: './paste-dialog.component.html',
  styleUrls: ['./paste-dialog.component.scss'],
})
export class PasteNodeDialogComponent {
  form: UntypedFormGroup;

  constructor(
    private dialogRef: MatDialogRef<PasteNodeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private readonly formBuilder: UntypedFormBuilder
  ) {
    let nodeData: NodeData = this.data.nodeData;
    let existingNodes: NodeData[] = this.data.existingNodes
      ? this.data.existingNodes
      : [];

    this.form = this.formBuilder.group({
      name: [
        nodeData.name,
        [
          Validators.required,
          stringValidator(existingNodes.map((x) => x.name)),
        ],
      ],
      path: [nodeData.path],
      isDir: [nodeData.isDir],
    });
  }

  public onCancelClicked(): void {
    this.dialogRef.close();
  }

  public onSaveClicked(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.value);
    } else {
      this.form.controls.name.markAsTouched();
    }
  }
}
