import { Component, Inject } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { NodeData } from 'app/+ide/shared/domain/node-data';

@Component({
  selector: 'app-ide-editor-save-open-files-dialog',
  templateUrl: 'save-open-files-dialog.component.html',
  styleUrls: ['./save-open-files-dialog.component.scss'],
})
export class SaveOpenFilesDialogComponent {
  nodeDataList: NodeData[];

  constructor(
    public dialogRef: MatDialogRef<SaveOpenFilesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: NodeData[]
  ) {
    this.nodeDataList = data;
  }

  public onYesClicked(): void {
    this.dialogRef.close('yes');
  }

  public onNoClicked(): void {
    this.dialogRef.close('no');
  }

  public onCancelClicked(): void {
    this.dialogRef.close();
  }
}
