import { Component, Inject } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { NodeData } from 'app/+ide/shared/domain/node-data';

@Component({
  selector: 'app-ide-editor-delete-dialog',
  templateUrl: 'delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
})
export class DeleteNodeDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<DeleteNodeDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: NodeData
  ) {}

  public onYesClicked(): void {
    this.dialogRef.close('yes');
  }

  public onCancelClicked(): void {
    this.dialogRef.close();
  }
}
