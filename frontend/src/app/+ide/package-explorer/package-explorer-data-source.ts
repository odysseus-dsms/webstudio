import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { NodeData } from '../shared/domain/node-data';

export class PackageExplorerDataSource implements DataSource<NodeData> {
  dataChange = new BehaviorSubject<NodeData[]>([]);

  get data(): NodeData[] {
    return this.dataChange.value;
  }
  set data(value: NodeData[]) {
    this._treeControl.dataNodes = value;
    this.dataChange.next(value);
  }

  constructor(private _treeControl: FlatTreeControl<NodeData>) {}

  connect(collectionViewer: CollectionViewer): Observable<NodeData[]> {
    return merge(collectionViewer.viewChange, this.dataChange).pipe(
      map(() => this.data)
    );
  }

  disconnect(collectionViewer: CollectionViewer): void {}

  refresh(): void {
    let data = this.data;
    this.data = null;
    this.data = data;
  }

  initData(projects: NodeData[]) {
    this.data = projects;
    this.expandAllFetched();
  }

  expandAllFetched() {
    this.expandFetchedRecursivley(this.data);
  }

  expandFetchedRecursivley(nodes: NodeData[]) {
    nodes
      .filter((node) => !!node.children)
      .forEach((node) => {
        this._treeControl.expand(node);
        this.expandFetchedRecursivley(node.children);
      });
  }

  sortData() {
    this.sort(this.data);
  }

  sortChildren(node: NodeData) {
    if (node.children) {
      this.sort(node.children);
    }
  }

  sort(nodeDataList: NodeData[]) {
    nodeDataList.sort((a, b) => a.name.localeCompare(b.name));
    nodeDataList.sort((a, b) => (a.isDir === b.isDir ? 0 : a.isDir ? -1 : 1));
  }
}
