import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatLegacyMenuTrigger as MatMenuTrigger } from '@angular/material/legacy-menu';

import { NodeService } from '../../shared/services/api/webstudio/node.service';
import { PackageExplorerDataSource } from './package-explorer-data-source';
import { NodeData } from '../shared/domain/node-data';
import { pathToUrlArray } from '../shared/util/path.utils';
import { RenameNodeDialogComponent } from './dialog/rename/rename-node-dialog.component';
import { CreateNodeDialogComponent } from './dialog/create/create-node-dialog.component';
import { NodeDetails } from '../../shared/dto/node/node-details';
import { mapToNodeData } from '../shared/util/node.assembler';
import { ActivatedRoute, Router } from '@angular/router';
import { ServerState } from 'app/shared/states/server.state';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { Store } from '@ngxs/store';
import { DeleteNodeDialogComponent } from './dialog/delete/delete-dialog.component';
import { MessageService } from 'app/shared/services/message.service';
import { SaveOpenFilesDialogComponent } from './dialog/save-changes/save-open-files-dialog.component';
import { AuthState } from 'app/shared/states/auth.state';
import { PasteNodeDialogComponent } from './dialog/paste/paste-dialog.component';
import { OperatorService } from 'app/shared/services/api/odysseus-core/operator.service';
import { OperatorDto } from 'app/shared/dto/operator/operator.dto';
import { defineNodeWidth } from 'app/util/graph.utils';

import { Graph } from '@antv/x6';
import { Dnd } from '@antv/x6-plugin-dnd';

@Component({
  selector: 'app-ide-package-explorer',
  templateUrl: './package-explorer.component.html',
  styleUrls: ['./package-explorer.component.scss'],
})
export class PackageExplorerComponent implements OnInit, AfterViewInit {
  @Input() graph: Graph|null;

  @Output() onNodeDeleted = new EventEmitter<NodeData>();
  @Output() onNodeRefreshed = new EventEmitter<NodeData>();
  @Output() onSaveNodes = new EventEmitter<{
    nodeDataList: NodeData[];
    fn: () => void;
  }>();

  @ViewChild('dndContainer') dndContainer: ElementRef;

  dnd: Dnd;
  serverDto: ServerDto;
  subQueryOperator: OperatorDto;

  treeControl: NestedTreeControl<NodeData>;
  dataSource: PackageExplorerDataSource;

  currentNode: NodeData;

  pasteCacheNode: NodeData;
  pasteCacheType: 'move' | 'copy';

  contextMenuPosition = { x: '0px', y: '0px' };

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _store: Store,
    private _dialog: MatDialog,
    private _nodeService: NodeService,
    private _operatorService: OperatorService,
    private _messageService: MessageService
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );

      this._operatorService
        .findAllOperators(this.serverDto)
        .subscribe((operators: OperatorDto[]) => {
          this.subQueryOperator = operators.find(o => o.operatorName.toLowerCase() === 'subquery');
        });

      this.treeControl = new NestedTreeControl<NodeData>((node) => node.children);
      this.dataSource = new PackageExplorerDataSource(this.treeControl);
    });
  }

  ngAfterViewInit(): void {
    this.dnd = new Dnd({
      target: null,
      dndContainer: this.dndContainer.nativeElement,
      getDropNode(node) {
        const subQryNode = node.clone();
        subQryNode.data.operator.parameters
          .find(p => p.parameterName === 'QUERYFILE')
          .value = subQryNode.data.filePath;
        return subQryNode;
      }
    });
  }

  onWorkspaceLoaded(projects: NodeData[]) {
    this.dataSource.initData(projects);
  }

  onNodeRestored(nodeData: NodeData) {
    this.currentNode = nodeData;
  }

  onNodeCreated(nodeData: NodeData) {
    this.currentNode = nodeData;
    this.dataSource.refresh();
    this.treeControl.expand(nodeData);
  }

  isDir = (_: number, node: NodeData) => node.isDir;

  pathToUrl(path: string) {
    return pathToUrlArray(this.serverDto.name, path);
  }

  onCreateProjectClicked() {
    this.create(
      new NodeData('', null, true, -1),
      true,
      "",
      (response: NodeDetails) => {
        let nodeData = mapToNodeData(response, null, 0);
        this.dataSource.data.push(nodeData);
        this.dataSource.sortData();
        this.dataSource.refresh();

        this._router.navigate(this.pathToUrl(nodeData.path));
        this.treeControl.expand(nodeData);
        this._messageService.showMessage(`Project '${nodeData.name}' created.`);
      }
    );
  }

  onCreateFolderClicked(nodeData: NodeData = this.currentNode) {
    let workingDirectory = nodeData.isDir ? nodeData : nodeData.parent;
    this._router.navigate(this.pathToUrl(workingDirectory.path));

    this.create(workingDirectory, true, "", (response: NodeDetails) => {
      let nodeData = mapToNodeData(
        response,
        workingDirectory,
        workingDirectory.level + 1
      );

      workingDirectory.children.push(nodeData);

      this.dataSource.sortChildren(workingDirectory);
      this.dataSource.refresh();

      this._router.navigate(this.pathToUrl(nodeData.path));
      this.treeControl.expand(workingDirectory);
      this._messageService.showMessage(`Folder '${nodeData.name}' created.`);
    });
  }

  onCreateFileClicked2(nodeData: NodeData = this.currentNode) {
    this.onCreateFileClicked_internal(nodeData, ".vqry");
  }

  onCreateFileClicked(nodeData: NodeData = this.currentNode) {
    this.onCreateFileClicked_internal(nodeData, "");
  }

  onCreateFileClicked_internal(nodeData: NodeData = this.currentNode, ending:string) {
    let workingDirectory = nodeData.isDir ? nodeData : nodeData.parent;
    this._router.navigate(this.pathToUrl(workingDirectory.path));

    this.create(workingDirectory, false, ending, (response: NodeDetails) => {
      let nodeData = mapToNodeData(
        response,
        workingDirectory,
        workingDirectory.level + 1
      );

      workingDirectory.children.push(nodeData);

      this.dataSource.sortChildren(workingDirectory);
      this.dataSource.refresh();

      this._router.navigate(this.pathToUrl(nodeData.path));
      this.treeControl.expand(workingDirectory);
      this._messageService.showMessage(`File '${nodeData.name}' created.`);
    });
  }

  create(
    parent: NodeData,
    isDir: boolean,
    prefilled:string="",
    action: (response: NodeDetails) => void,
  ): void {
    this._dialog
      .open(CreateNodeDialogComponent, {
        width: '280px',
        data: {
          nodeData: new NodeData(prefilled, parent, isDir, parent.level),
          existingNodes: parent.children,
        },
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this._nodeService
            .createNode(this.serverDto, parent.path, {
              name: result.name,
              isDir: result.isDir,
            })
            .subscribe((response: NodeDetails) => {
              action(response);
            });
        }
      });
  }

  onRenameClicked(nodeData: NodeData = this.currentNode): void {
    this._dialog
      .open(RenameNodeDialogComponent, {
        width: '250px',
        data: {
          nodeData: nodeData,
          existingNodes: nodeData.parent
            ? nodeData.parent.children
            : this.dataSource.data,
        },
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this._nodeService
            .updateNode(this.serverDto, nodeData.path, {
              name: result.name,
            })
            .subscribe((response) => {
              nodeData.name = response.name;
              this._router.navigate(this.pathToUrl(nodeData.path));
              this._messageService.showMessage(
                `${
                  nodeData.parent
                    ? nodeData.isDir
                      ? 'Folder'
                      : 'File'
                    : 'Project'
                } '${nodeData.name}' renamed.`
              );
            });
        }
      });
  }

  onDeleteClicked(nodeData: NodeData = this.currentNode) {
    this._dialog
      .open<DeleteNodeDialogComponent, NodeData>(DeleteNodeDialogComponent, {
        width: '410px',
        data: nodeData,
      })
      .afterClosed()
      .subscribe((result) => {
        if (result === 'yes') {
          this._nodeService
            .deleteNode(this.serverDto, nodeData.path)
            .subscribe(() => {
              if (nodeData.parent) {
                let index = nodeData.parent.children.indexOf(nodeData);
                nodeData.parent.children.splice(index, 1);
                this.dataSource.refresh();

                if (nodeData === this.currentNode) {
                  this._router.navigate(this.pathToUrl(nodeData.parent.path));
                }
              } else {
                let index = this.dataSource.data.indexOf(nodeData);
                this.dataSource.data.splice(index, 1);
                this.dataSource.refresh();

                this._router.navigate(['servers', this.serverDto.name]);
              }
              this.onNodeDeleted.emit(nodeData);
            });
          this._messageService.showMessage(
            `${
              nodeData.parent ? (nodeData.isDir ? 'Folder' : 'File') : 'Project'
            } '${nodeData.name}' deleted.`
          );
        }
      });
  }

  onCutClicked(nodeData: NodeData = this.currentNode) {
    if (nodeData.parent) {
      this.pasteCacheNode = nodeData;
      this.pasteCacheType = 'move';
      this._messageService.showMessage(
        `${nodeData.isDir ? 'Folder' : 'File'} '${nodeData.name}' cut out.`
      );
    }
  }

  onCopyClicked(nodeData: NodeData = this.currentNode) {
    if (nodeData.parent) {
      this.pasteCacheNode = nodeData;
      this.pasteCacheType = 'copy';
      this._messageService.showMessage(
        `${nodeData.isDir ? 'Folder' : 'File'} '${nodeData.name}' copied.`
      );
    }
  }

  onPasteClicked(nodeData: NodeData = this.currentNode) {
    let pasteCacheNode = this.pasteCacheNode;
    this.pasteCacheNode = null;

    let workingDirectory = nodeData.isDir ? nodeData : nodeData.parent;
    this._router.navigate(this.pathToUrl(workingDirectory.path));

    if (this.pasteCacheType === 'move') {
      this._nodeService
        .updateNode(this.serverDto, pasteCacheNode.path, {
          path: `${workingDirectory.path}/${pasteCacheNode.name}`,
        })
        .subscribe((response) => {
          // remove here
          let index = pasteCacheNode.parent.children.indexOf(pasteCacheNode);
          pasteCacheNode.parent.children.splice(index, 1);

          // paste here
          workingDirectory.children.push(pasteCacheNode);
          pasteCacheNode.parent = workingDirectory;
          pasteCacheNode.level = workingDirectory.level + 1;
          this.updateLevelRecursively(pasteCacheNode);

          this.dataSource.sortChildren(workingDirectory);
          this.dataSource.refresh();

          this._router.navigate(this.pathToUrl(nodeData.path));
          this.treeControl.expand(workingDirectory);
          this._messageService.showMessage(
            `${nodeData.isDir ? 'Folder' : 'File'} '${nodeData.name}' pasted.`
          );
        });
    } else {
      this._dialog
        .open<PasteNodeDialogComponent, any>(PasteNodeDialogComponent, {
          width: '410px',
          data: {
            nodeData: pasteCacheNode,
            existingNodes: workingDirectory.children,
          },
        })
        .afterClosed()
        .subscribe((result) => {
          if (result) {
            this._nodeService
              .createNode(this.serverDto, workingDirectory.path, {
                name: `${result.name}`,
                copyOf: result.path,
              })
              .subscribe((response) => {
                let newNodeData = mapToNodeData(
                  response,
                  workingDirectory,
                  workingDirectory.level + 1
                );

                workingDirectory.children.push(newNodeData);
                this.dataSource.sortChildren(workingDirectory);
                this.dataSource.refresh();

                this._router.navigate(this.pathToUrl(newNodeData.path));
                this.treeControl.expand(workingDirectory);
                this._messageService.showMessage(
                  `${newNodeData.isDir ? 'Folder' : 'File'} '${
                    newNodeData.name
                  }' pasted.`
                );
              });
          }
        });
    }
  }

  updateLevelRecursively(nodeData: NodeData) {
    nodeData.children?.forEach((x) => {
      x.level = nodeData.level + 1;
      this.updateLevelRecursively(x);
    });
  }

  onRefreshClicked(nodeData: NodeData = this.currentNode) {
    if (this.hasAnyChangesRecursively(nodeData)) {
      let files = this.getAllFiles(nodeData).filter((x) => x.hasChanges());

      this._dialog
        .open<SaveOpenFilesDialogComponent, NodeData[]>(
          SaveOpenFilesDialogComponent,
          {
            width: '410px',
            data: files,
          }
        )
        .afterClosed()
        .subscribe((result) => {
          if (result === 'yes') {
            this.onSaveNodes.emit({
              nodeDataList: files,
              fn: () => this.refresh(nodeData),
            });
          } else if (result === 'no') {
            this.refresh(nodeData);
          }
        });
    } else {
      this.refresh(nodeData);
    }
  }

  refresh(nodeData: NodeData) {
    this.onNodeRefreshed.emit(nodeData);

    nodeData.children = null;
    nodeData.content = null;
    this._router.navigate(this.pathToUrl(nodeData.path));
    this._messageService.showMessage(
      `${nodeData.isDir ? 'Folder' : 'File'} '${nodeData.name}' refreshed.`
    );
  }

  onDownloadClicked(nodeData: NodeData = this.currentNode) {
    this._nodeService
      .downloadNode(this.serverDto, nodeData.path)
      .subscribe((x) => {
        var link = window.document.createElement('a');
        link.href = window.URL.createObjectURL(x);
        link.download = nodeData.name;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      });
  }

  hasAnyChangesRecursively(nodeData: NodeData) {
    if (nodeData.isDir) {
      return (
        nodeData.children &&
        nodeData.children.some((x) => this.hasAnyChangesRecursively(x))
      );
    }

    return nodeData.hasChanges();
  }

  getAllFiles(nodeData: NodeData): NodeData[] {
    let nodes = [];

    if (nodeData.isDir) {
      nodeData.children?.forEach((x) => nodes.push(...this.getAllFiles(x)));
    } else {
      nodes.push(nodeData);
    }

    return nodes;
  }

  openMenu(event: MouseEvent, contextMenu: MatMenuTrigger): void {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    contextMenu.menu.focusFirstItem('mouse');
    contextMenu.openMenu();
  }

  onKeyDown(event): void {
    let macOS = navigator.platform.match('Mac');

    if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.key === 'x'
    ) {
      event.preventDefault();
      event.stopPropagation();
      this.onCutClicked();
    } else if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.key === 'c'
    ) {
      event.preventDefault();
      event.stopPropagation();
      this.onCopyClicked();
    } else if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.key === 'v'
    ) {
      event.preventDefault();
      event.stopPropagation();
      this.onPasteClicked();
    }
  }

  startDrag(event: MouseEvent, nodeData: NodeData) {
    if (!this.graph
      || event.button !== 0
      || (!nodeData.name.endsWith('.qry') && !nodeData.name.endsWith('.vqry'))) {
      return;
    }

    const graphNode = this.graph.createNode({
      width: defineNodeWidth(this.subQueryOperator.operatorName.length),
      height: 40,
      label: this.subQueryOperator.operatorName,
      data: {
        operator: this.subQueryOperator,
        filePath: nodeData.path
      },
      ports: {
        items: [
          {
            group: 'in'
          },
          {
            group: 'out'
          }
        ]
      }
    });

    this.dnd.options.target = this.graph;
    this.dnd.start(graphNode, event);
  }
}
