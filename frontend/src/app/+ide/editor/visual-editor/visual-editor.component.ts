import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { OperatorDto } from 'app/shared/dto/operator/operator.dto';
import { firstValueFrom, Subject } from 'rxjs';
import { MatLegacyMenuTrigger as MatMenuTrigger } from '@angular/material/legacy-menu';
import { NodeData } from 'app/+ide/shared/domain/node-data';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { ParameterDialogComponent } from 'app/+ide/parameter-dialog/parameter-dialog.component';
import { MessageService } from 'app/shared/services/message.service';
import { detectCycle } from 'app/+ide/shared/util/graph.utils';
import { ServicesService } from 'app/shared/services/api/odysseus-core/services.service';
import { OdysseusNode } from 'app/shared/models/odysseus-node.model';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { ServerState } from 'app/shared/states/server.state';
import { AuthState } from 'app/shared/states/auth.state';
import { NodeService } from 'app/shared/services/api/webstudio/node.service';
import { NodeDetails } from 'app/shared/dto/node/node-details';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { QueryService } from 'app/shared/services/api/odysseus-core/query.service';

import { Edge, Graph, Node } from '@antv/x6';
import { Selection } from '@antv/x6-plugin-selection';
import { Clipboard } from '@antv/x6-plugin-clipboard';
import { History } from '@antv/x6-plugin-history';
import { Snapline } from '@antv/x6-plugin-snapline';
import { Keyboard } from '@antv/x6-plugin-keyboard';
import { ShortcutsDialogComponent } from 'app/+ide/shortcuts-dialog/shortcuts-dialog.component';

@Component({
  selector: 'app-ide-visual-editor',
  templateUrl: './visual-editor.component.html',
  styleUrls: ['./visual-editor.component.scss']
})
export class VisualEditorComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() serverDto: ServerDto;
  @Input() operators: OperatorDto[];
  @Input() currentNode: NodeData;

  @Output() onGraphNodeSelected = new EventEmitter<Node|null>();

  @ViewChild('graphContainer') graphContainer: ElementRef;
  @ViewChild('subGraphContainer') subGraphContainer: ElementRef;
  @ViewChild(MatMenuTrigger, { static: true }) matMenuTrigger: MatMenuTrigger;

  graph: Graph;
  history: History;

  menuTopLeftPosition = { x: '0', y: '0' };
  odysseusNodes: OdysseusNode[];
  selectedNodes: Node[] = [];

  destroyed$ = new Subject<void>();

  constructor(
    private _route: ActivatedRoute,
    private _dialog: MatDialog,
    private _store: Store,
    private _messageService: MessageService,
    private _serviceService: ServicesService,
    private _nodeService: NodeService,
    private _queryService: QueryService
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );
      this.updateOdysseusNodes();

      if (this.graph) {
        this.graph.dispose();
        this.initGraph();
      }
    });
  }

  ngAfterViewInit(): void {
    this.initGraph();
  }

  updateOdysseusNodes(): void {
    this._serviceService.nodeManager(this.serverDto).subscribe(nodeMap => {
      this.odysseusNodes = nodeMap.nodemap.sort((a, b) => a.name.localeCompare(b.name));
    });
  }

  initGraph(): void {
    this.graph = new Graph({
      container: this.graphContainer.nativeElement,
      panning: true,
      highlighting: {
        magnetAvailable: {
          name: 'stroke',
          args: {
            padding: 2,
            attrs: {
              strokeWidth: 3,
              stroke: '#52c41a',
            },
          }
        }
      },
      connecting: {
        snap: {
          radius: 25
        },
        allowBlank: false,
        allowMulti: 'withPort',
        allowLoop: false,
        allowNode: false,
        allowEdge: false,
        allowPort: true,
        highlight: true,
        router: {
          // alternative manhattan --> doesnt work?
          name: 'metro',
        },
        connector: {
          // alternative jumpover --> doesnt work properly and has significant impact on performance
          name: 'rounded',
          args: {
            size: 5
          }
        },
        validateMagnet({ magnet }) {
          return magnet.getAttribute('port-group') !== 'in';
        },
        validateConnection({
          sourceMagnet,
          targetMagnet,
          sourceCell,
          targetCell,
          targetPort
        }) {
          if ((!sourceMagnet || sourceMagnet.getAttribute('port-group') === 'in')
            || (!targetMagnet || targetMagnet.getAttribute('port-group') !== 'in')
            || (targetCell.data.operator.maxPorts <= 2 && this.getIncomingEdges(targetCell)?.find((edge: Edge) => edge.getTargetPortId() === targetPort))
            || detectCycle(this, sourceCell as Node, targetCell as Node)
            || (this.getNeighbors(targetCell, { incoming: true }).length >= targetCell.data.operator.maxPorts)) {
            return false;
          }
          return true;
        }
      },
      grid: {
        visible: false,
        size: 1,
        type: 'mesh'
      },
      mousewheel: {
        enabled: true
      }
    });
    this.graph.center();

    this.initPlugins();
    this.initKeyBindings();
    this.initEventListeners();
  }

  initPlugins(): void {
    this.graph.use(
      new Selection({
        enabled: true,
        multiple: true,
        rubberband: true,
        modifiers: 'shift',
        showNodeSelectionBox: true,
        showEdgeSelectionBox: true
      })
    );

    this.graph.use(
      new Clipboard({
        enabled: true,
        useLocalStorage: true
      })
    );

    this.graph.use(
      new Snapline({
        enabled: true,
        sharp: true
      })
    );

    this.graph.use(
      new Keyboard({
        enabled: true
      })
    );

    this.history = new History({
      enabled: true,
      beforeAddCommand(event, args) {
        return args['key'] !== 'tools';
      }
    });

    this.graph.use(
      this.history
    );
  }

  initKeyBindings(): void {
    const primaryKey = navigator.platform.match('Mac') ? 'command' : 'ctrl';

    // Select all
    this.graph.bindKey(`${primaryKey}+a`, (e) => {
      e.preventDefault();
      this.graph.select(this.graph.getCells());
    });

    // Copy
    this.graph.bindKey(`${primaryKey}+c`, () => {
      const cells = this.graph.getSelectedCells();
      if (cells.length) {
        this.graph.copy(cells);
      }
      return false;
    });

    // Cut
    this.graph.bindKey(`${primaryKey}+x`, () => {
      const cells = this.graph.getSelectedCells();
      if (cells.length) {
        this.graph.cut(cells);
      }
      return false;
    });

    // Paste
    this.graph.bindKey(`${primaryKey}+v`, () => {
      if (!this.graph.isClipboardEmpty()) {
        const cells = this.graph.paste({ offset: 32 });
        this.graph.cleanSelection();
        this.graph.select(cells);
      }
      return false;
    });

    // Remove
    this.graph.bindKey('backspace', () => {
      this.removeOperators();
      return false;
    });

    // Remove
    this.graph.bindKey('del', () => {
      this.removeOperators();
      return false;
    });

    // Undo
    this.graph.bindKey(`${primaryKey}+left`, (e) => {
      e.preventDefault();
      this.history.undo();

      if (!this.history.canUndo()) {
        this.currentNode.graphHasChanged = false;
      }
    });

    // Redo
    this.graph.bindKey(`${primaryKey}+right`, (e) => {
      e.preventDefault();
      this.history.redo();
    });
  }

  initEventListeners(): void {
    this.graph.on('node:contextmenu', ({ e, node }) => {
      e.preventDefault();

      if (this.selectedNodes.length === 0) {
        this.selectedNodes.push(node);
      } else if (this.selectedNodes.length > 1 && !this.selectedNodes.includes(node)) {
        return;
      }

      if (node.data?.operator) {
        this.menuTopLeftPosition.x = e.clientX + 'px';
        this.menuTopLeftPosition.y = e.clientY + 'px';
        this.matMenuTrigger.menuData = { item: node };

        this.matMenuTrigger.openMenu();
      }
    });

    this.graph.on('node:selected', ({ node }) => {
      this.selectedNodes.length = 0;

      if (this.graph.getSelectedCellCount() === 1) {
        this.onGraphNodeSelected.emit(node);
        this.selectedNodes.push(node);
      } else {
        this.onGraphNodeSelected.emit(null);
        this.graph.getSelectedCells().forEach(cell => {
          if (cell.isNode()) {
            this.selectedNodes.push(cell);
          }
        })
      }
    });

    this.graph.on('node:unselected', ({ node }) => {
      this.selectedNodes.length = 0;
      this.toggleEdgeHighlighting(node, false);
    });

    this.graph.on('node:dblclick', ({ node }) => {
      if (!node.data?.operator) {
        return;
      }
      this.openParameterDialog(node);
    });

    this.graph.on('node:added', () => {
      this.graph.cleanSelection();

      if (!this.currentNode.graphHasChanged) {
        this.currentNode.graphHasChanged = true;
      }
    });

    this.graph.on('node:change:*', ({ node }) => {
      if (!this.currentNode.graphHasChanged) {
        this.currentNode.graphHasChanged = true;
      }
      this.toggleEdgeHighlighting(node, false);
    });

    this.graph.on('node:mouseenter', ({ node }) => {
      this.toggleEdgeHighlighting(node, true)
    });

    this.graph.on('node:mouseleave', ({ node }) => {
      this.toggleEdgeHighlighting(node, false)
    });

    this.graph.on('cell:removed', () => {
      if (!this.currentNode.graphHasChanged) {
        this.currentNode.graphHasChanged = true;
      }
    });

    this.graph.on('edge:mouseenter', ({ cell }) => {
      cell.addTools([
        {
          name: 'source-arrowhead'
        },
        {
          name: 'target-arrowhead'
        }
      ]);
    });

    this.graph.on('edge:mouseleave', ({ cell }) => {
      cell.removeTools();
    });

    this.graph.on('edge:change:source', () => {
      if (!this.currentNode.graphHasChanged) {
        this.currentNode.graphHasChanged = true;
      }
    });

    this.graph.on('edge:change:target', () => {
      if (!this.currentNode.graphHasChanged) {
        this.currentNode.graphHasChanged = true;
      }
    });

    this.graph.on('blank:click', () => {
      this.onGraphNodeSelected.emit(null);
    });
  }

  onDestinationSelected(destNode: string) {
    this.selectedNodes.forEach(node => {
      if (!node.data?.operator) {
        return;
      }

      node.data.operator.parameters.find(p => p.parameterName === 'DESTINATION').value = destNode;
    });

    if (!this.currentNode.graphHasChanged) {
      this.currentNode.graphHasChanged = true;
    }
    this._messageService.showMessage('Destination edited.');
  }

  async transformGraphToPql(querySubmitted?: boolean, startQuery?: boolean, subVisualQuery?: NodeDetails): Promise<string> {
    const graph = subVisualQuery
      ? new Graph({ container: this.subGraphContainer.nativeElement }).fromJSON(JSON.parse(subVisualQuery.content))
      : this.graph;
    const pqlOperators = [];
    const subQueries = [];
    const nodes = [];
    let isDistributed = false;

    graph.getLeafNodes().forEach((n) => {
      graph.model.breadthFirstSearch(n, (cell, distance) => {
        if (!nodes.find(c => c.cell === cell)) {
          nodes.push({ cell, distance });
        }
      }, { incoming: true });
    });

    // ensure correct order of operators
    nodes.forEach((node) => {
      const updateIncomingDistances = (currentNode) => {
        graph.getNeighbors(currentNode.cell, { incoming: true }).forEach((incomingCell) => {
          const incomingSeenNode = nodes.find(c => c.cell === incomingCell);
          if (incomingSeenNode.distance <= currentNode.distance) {
            incomingSeenNode.distance = currentNode.distance + 1;
            // recursively update distances for incoming nodes
            updateIncomingDistances(incomingSeenNode);
          }
        });
      }
      updateIncomingDistances(node);
    });
    nodes.sort((a, b) => b.distance - a.distance);

    nodes.forEach((node) => {
      const pqlParams = [];
      const operator = node.cell.data?.operator ? node.cell.data?.operator : null;

      if (operator !== null) {
        operator.parameters.forEach(param => {
          if (param.value) {
            if (param.parameterName === 'QUERYFILE' && querySubmitted && node.cell.data.filePath) {
              subQueries.push({
                id: node.cell.id,
                path: node.cell.data.filePath
              });
              pqlParams.push(`QUERYID=`);
            } else {
              pqlParams.push(
                param.list
                || param.parameterType.endsWith('.IntegerParameter')
                || param.parameterType.endsWith('.LongParameter')
                || param.parameterType.endsWith('.DoubleParameter')
                  ? `${param.parameterName}=${param.value}`
                  : `${param.parameterName}='${param.value}'`
              );
            }

            if (!isDistributed) {
              isDistributed = param.parameterName === 'DESTINATION';
            }
          }
        });
      }

      const incomingNeighbors = [];
      graph.getNeighbors(node.cell, { incoming: true }).forEach(inputCell => {
        incomingNeighbors.push(inputCell);
      });

      const paramText = pqlParams.length > 0 ? `({\n\t${pqlParams.join(`,\n\t`)}\n})` : '';
      const currentPqlOperator = {
        id: node.cell.id,
        var: `op${pqlOperators.length}`,
        pqlParams,
        incomingNeighbors,
        hasInputOperator: false,
        text: operator === null
          ? `op${pqlOperators.length} = ${node.cell.label}`
          : `op${pqlOperators.length} = ${operator.operatorName}${paramText}`
      }

      pqlOperators.push(currentPqlOperator);
    });

    if (subQueries.length > 0) {
      await subQueries.reduce(async (promise, q) => {
        const nodeDetails = await firstValueFrom(this._nodeService.findNode(this.serverDto, q.path));
        const query: QueryPreview = {
          parser: 'OdysseusScript',
          queryText: nodeDetails?.name.endsWith('.vqry')
            ? await this.transformGraphToPql(true, startQuery, nodeDetails)
            : nodeDetails.content
        };
        const subQuery = await firstValueFrom(this._queryService.create(this.serverDto, query));

        const pqlOperator = pqlOperators.find((o) => o.id === q.id);
        const queryParamIndex = await pqlOperator.pqlParams.indexOf('QUERYID=');
        if (queryParamIndex !== -1) {
          pqlOperator.pqlParams[queryParamIndex] = `QUERYID=${subQuery[0].id}`;
          pqlOperator.text = pqlOperator.text.replace('QUERYID=', pqlOperator.pqlParams[queryParamIndex]);
        }
      }, Promise.resolve());

      return this.generateQueryText(
        pqlOperators,
        isDistributed,
        subVisualQuery ? subVisualQuery.name : this.currentNode.name,
        startQuery
      );
    } else {
      return this.generateQueryText(
        pqlOperators,
        isDistributed,
        subVisualQuery ? subVisualQuery.name : this.currentNode.name,
        startQuery
      );
    }
  }

  generateQueryText(pqlOperators, isDistributed: boolean, qryName: string, startQuery?: boolean): string {
    let queryText = `#PARSER PQL\n#QNAME ${qryName}\n`;
    queryText += startQuery ? '#RUNQUERY\n' : '#ADDQUERY\n'

    pqlOperators.forEach(pqlOperator => {
      pqlOperator.incomingNeighbors.forEach(cell => {
        const inputPqlOperator = pqlOperators.find(o => o.id === cell.id);

        if (pqlOperator.pqlParams.length > 0 || pqlOperator.hasInputOperator) {
          pqlOperator.text = pqlOperator.text.slice(0, -1) + `, ${inputPqlOperator.var})`;
        } else {
          pqlOperator.text += `(${inputPqlOperator.var})`;
        }
        pqlOperator.hasInputOperator = true;
      });

      if (pqlOperator.text !== '') {
        queryText += pqlOperator.text + '\n\n';
      }
    });

    if (isDistributed) {
      queryText = '#CONFIG DISTRIBUTE true\n#NODE_ALLOCATION USER\n#NODE_PARTITION OPERATORCLOUD\n\n' + queryText;
    }
    return queryText;
  }

  openParameterDialog(node: Node): void {
    this._dialog
      .open<ParameterDialogComponent, any>(
        ParameterDialogComponent,
        {
          data: {
            operatorDto: node.data.operator,
            serverDto: this.serverDto
          },
          autoFocus: false
        }
      )
      .afterClosed()
      .subscribe((result) => {
        if (result === 'saved') {
          this._messageService.showMessage('Parameters edited.');
          this.onGraphNodeSelected.emit(node);

          if (!this.currentNode.graphHasChanged) {
            this.currentNode.graphHasChanged = true;
          }
        }
      });
  }

  openShortcutDialog(): void {
    this._dialog
      .open<ShortcutsDialogComponent, any>(
        ShortcutsDialogComponent,
        {
          width: '400px',
          autoFocus: false
        }
      );
  }

  toggleEdgeHighlighting(node: Node, highlight: boolean): void {
    this.graph.getIncomingEdges(node)?.forEach(e => {
      const cellView = this.graph.findViewByCell(e);
      if (cellView) {
        highlight ? cellView.highlight() : cellView.unhighlight();
      }
    });
    this.graph.getOutgoingEdges(node)?.forEach(e => {
      const cellView = this.graph.findViewByCell(e);
      if (cellView) {
        highlight ? cellView.highlight() : cellView.unhighlight();
      }
    });
  }

  removeOperators(): void {
    const cells = this.graph.getSelectedCells();
    if (cells.length) {
      this.graph.removeCells(cells);
      this.onGraphNodeSelected.emit(null);
    } else if (this.selectedNodes.length) {
      this.graph.removeCells(this.selectedNodes);
      this.onGraphNodeSelected.emit(null);
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
