import { Component, OnDestroy, OnInit, NgZone, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';

import { NodeData } from '../shared/domain/node-data';
import { OperatorDto } from '../../shared/dto/operator/operator.dto';
import { ParserTokenDto } from '../../shared/dto/parser/token.dto';
import { NodeService } from '../../shared/services/api/webstudio/node.service';
import { ParserService } from '../../shared/services/api/odysseus-core/parser.service';
import { pathToUrlArray } from '../shared/util/path.utils';
import {
  getKeywordsCompletionItems,
  getOperatorsCompletionItems,
  getQueryLanguage,
  queryLanguageConfiguration,
} from '../shared/configs/language/odysseus-script.language';
import { MessageService } from 'app/shared/services/message.service';
import { QueryService } from 'app/shared/services/api/odysseus-core/query.service';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { ServerState } from 'app/shared/states/server.state';
import { Store } from '@ngxs/store';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatLegacyMenuTrigger as MatMenuTrigger } from '@angular/material/legacy-menu';
import { SaveChangesDialogComponent } from './save-changes/save-changes-dialog.component';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { takeUntil } from 'rxjs/operators';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AuthState } from 'app/shared/states/auth.state';
import { VisualEditorComponent } from './visual-editor/visual-editor.component';
import { Node } from '@antv/x6';
import { OperatorService } from 'app/shared/services/api/odysseus-core/operator.service';

@Component({
  selector: 'app-ide-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss'],
})
export class EditorComponent implements OnInit, OnDestroy {
  @Output() onEditorChanged = new EventEmitter<boolean>();
  @Output() onGraphNodeSelection = new EventEmitter<Node|null>();

  @ViewChild(VisualEditorComponent) visualEditorComponent: VisualEditorComponent;

  serverDto: ServerDto;
  operators: OperatorDto[];
  parserToken: ParserTokenDto;

  isVisualEditor: boolean;
  initialized: boolean;

  openNodes: NodeData[];
  currentNode: NodeData;

  contextMenuPosition = { x: '0px', y: '0px' };

  editor: monaco.editor.IStandaloneCodeEditor;
  destroyed$ = new Subject<void>();

  public editorOptions = {
    theme: 'OdysseusTheme',
  };

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _ngZone: NgZone,
    private _store: Store,
    private _dialog: MatDialog,
    private _nodeService: NodeService,
    private _parserService: ParserService,
    private _queryService: QueryService,
    private _operatorService: OperatorService,
    private _messageService: MessageService
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.ngOnDestroy();
      this.openNodes = [];
      this.initialized = false;
      this.currentNode = null;
      this.isVisualEditor = false;

      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );

      this.loadOperatorsAndParserToken();
    });
  }

  loadOperatorsAndParserToken(): void {
    this._operatorService
      .findAllOperators(this.serverDto)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((operators: OperatorDto[]) => {
        this.operators = operators;

        if (this.parserToken) {
          this.initialized = true;
        }
      });

    this._parserService
      .getAllTokens(this.serverDto, 'OdysseusScript')
      .pipe(takeUntil(this.destroyed$))
      .subscribe((x) => {
        this.parserToken = x;
        this.parserToken.DEPRECATED = this.parserToken.DEPRECATED.map(
          (x) => `#${x}`
        );
        this.parserToken.KEYWORDS = this.parserToken.KEYWORDS.map(
          (x) => `#${x}`
        );
        this.parserToken.STATIC = this.parserToken.STATIC.map((x) => `#${x}`);

        if (this.operators) {
          this.initialized = true;
        }
      });
  }

  ngOnDestroy() {
    if (this.openNodes) {
      this.openNodes.forEach((x) => {
        x.model.dispose();
        x.model = null;
        x.state = null;
      });
    }
    this.destroyed$.next();
  }

  async onChangeEditorClicked() {
    this.isVisualEditor = !this.isVisualEditor;
    this.onEditorChanged.emit(this.isVisualEditor);

    if (this.currentNode.isVisual && !this.isVisualEditor) {
      this.currentNode.model.setValue(
        await this.visualEditorComponent.transformGraphToPql()
      );
      this.dispatchResizeEvent();
    }
  }

  onGraphNodeSelected(graphNode: Node|null) {
    this.onGraphNodeSelection.emit(graphNode);
  }

  onNodeCreated(nodeData: NodeData) {
    if (this.editor) {
      this.createTab(nodeData);
    } else {
      this.currentNode = nodeData;
    }
  }

  onNodeRestored(nodeData: NodeData) {
    if (this.editor) {
      this.changeTab(nodeData);
    } else {
      this.currentNode = nodeData;
    }
  }

  async onEditorInitialized(editor: monaco.editor.IStandaloneCodeEditor) {
    this._ngZone.run(() => {
      this.editor = editor;

      if (
        !monaco.languages.getLanguages().some((x) => x.id === 'OdysseusScript')
      ) {
        monaco.languages.register({ id: 'OdysseusScript' });

        monaco.languages.setLanguageConfiguration(
          'OdysseusScript',
          queryLanguageConfiguration
        );

        monaco.languages.setMonarchTokensProvider(
          'OdysseusScript',
          getQueryLanguage(this.parserToken, this.operators)
        );

        monaco.languages.registerCompletionItemProvider(
          'OdysseusScript',
          getKeywordsCompletionItems(this.parserToken)
        );

        monaco.languages.registerCompletionItemProvider(
          'OdysseusScript',
          getOperatorsCompletionItems(this.operators)
        );
      }

      if (this.currentNode) {
        setTimeout(() => {
          this.createTab(this.currentNode);
        });
      }
    });
  }

  createTab(nodeData: NodeData) {
    let isQuery = nodeData.name.endsWith('.qry') || nodeData.name.endsWith('.vqry');

    nodeData.model = window.monaco.editor.createModel(
      nodeData.content,
      isQuery ? 'OdysseusScript' : undefined,
      isQuery ? undefined : monaco.Uri.file(nodeData.path)
    );

    nodeData.lastSavedVersionId = nodeData.model.getAlternativeVersionId();
    this.editor.setModel(nodeData.model);
    this.editor.updateOptions({ readOnly: nodeData.isVisual });

    if (this.currentNode?.isVisual) {
      this.currentNode.graphJson = this.visualEditorComponent.graph.toJSON();
    }

    if (nodeData.isVisual) {
      const content = nodeData.content ? JSON.parse(nodeData.content) : {};
      this.visualEditorComponent.graph.fromJSON(nodeData.graphJson?.length > 0 ? nodeData.graphJson : content);
      this.visualEditorComponent.history.clean();
      this.visualEditorComponent.graph.center();
    }

    if (this.isVisualEditor !== nodeData.isVisual) {
      this.isVisualEditor = nodeData.isVisual;
      this.onEditorChanged.emit(this.isVisualEditor);
    }

    this.openNodes.push(nodeData);
    this.currentNode = nodeData;
    this.dispatchResizeEvent();
  }

  changeTab(nodeData: NodeData) {
    this.currentNode.model = this.editor.getModel();
    this.currentNode.state = this.editor.saveViewState();

    this.editor.setModel(nodeData.model);
    this.editor.restoreViewState(nodeData.state);
    this.editor.updateOptions({ readOnly: nodeData.isVisual });

    if (this.currentNode.isVisual) {
      this.currentNode.graphJson = this.visualEditorComponent.graph.toJSON();
    }

    if (nodeData.isVisual) {
      this.visualEditorComponent.graph.fromJSON(nodeData.graphJson);
      this.visualEditorComponent.history.clean();
      this.visualEditorComponent.graph.center();
    }

    if (this.isVisualEditor !== nodeData.isVisual) {
      this.isVisualEditor = nodeData.isVisual;
      this.onEditorChanged.emit(this.isVisualEditor);
    }

    this.currentNode = nodeData;
    this.dispatchResizeEvent();
  }

  hasChanges(): boolean {
    return this.currentNode && this.currentNode.hasChanges();
  }

  onSaveNodeClicked() {
    if (this.hasChanges()) {
      this.saveNode(this.currentNode);
    }
  }

  saveNode(nodeData: NodeData, action?: () => void) {
    const versionId = nodeData.model.getAlternativeVersionId();

    if (nodeData.isVisual && nodeData === this.currentNode) {
      nodeData.graphJson = this.visualEditorComponent.graph.toJSON();
    }

    this._nodeService
      .updateNode(this.serverDto, nodeData.path, {
        content: nodeData.isVisual ? JSON.stringify(nodeData.graphJson) : nodeData.model.getValue(),
      })
      .subscribe((response) => {
        nodeData.lastSavedVersionId = versionId;
        nodeData.graphHasChanged = false;
        action?.();
        this._messageService.showMessage('The file was saved successfully.');
      });
  }

  onSaveAllNodesClicked() {
    if (this.anyChanges()) {
      this.openNodes
        .filter((x) => x.model)
        .filter(
          (x) => x.hasChanges()
        )
        .forEach((x) => this.saveNode(x));
    }
  }

  anyChanges() {
    return this.openNodes
      .filter((x) => x.model)
      .some((x) => x.hasChanges());
  }

  async onQuerySubmitClicked(startQuery: boolean) {
    const query: QueryPreview = {
      parser: 'OdysseusScript',
      queryText: this.currentNode.isVisual
        ? await this.visualEditorComponent.transformGraphToPql(true, startQuery)
        : this.currentNode.model.getValue()
    };

    this._queryService.create(this.serverDto, query).subscribe(() => {
      this._messageService.showMessage('Query submitted.');
      this.visualEditorComponent.updateOdysseusNodes();
    });
  }

  onCloseTabClicked(nodeData: NodeData, event?: MouseEvent) {
    event?.preventDefault();

    if (nodeData.hasChanges()) {
      this._dialog
        .open<SaveChangesDialogComponent, NodeData>(
          SaveChangesDialogComponent,
          {
            width: '410px',
            data: nodeData,
          }
        )
        .afterClosed()
        .subscribe((result) => {
          if (result === 'yes') {
            this.saveNode(nodeData, () => this.closeTab(nodeData));
          } else if (result === 'no') {
            this.closeTab(nodeData);
          }
        });
    } else {
      this.closeTab(nodeData);
    }
  }

  closeTab(nodeData: NodeData) {
    nodeData.content = null;
    nodeData.model?.dispose();
    nodeData.graphHasChanged = false;

    let index = this.openNodes.indexOf(nodeData);
    if (index !== -1) {
      this.openNodes.splice(index, 1);

      if (this.currentNode === nodeData) {
        if (this.openNodes.length > 0) {
          if (index > 0) {
            this._router.navigate(
              this.pathToUrl(this.openNodes[index - 1].path)
            );
          } else {
            this._router.navigate(this.pathToUrl(this.openNodes[index].path));
          }
        } else {
          this.editor.setModel(null);
          this.currentNode = null;
          this.isVisualEditor = false;
          this.editor.updateOptions({ readOnly: false });
          this.onEditorChanged.emit(false);

          this._router.navigate(this.pathToUrl(nodeData.parent.path));
        }
      }
    }
  }

  navigateToNode(nodeData: NodeData) {
    this._router.navigate(this.pathToUrl(nodeData.path));
  }

  pathToUrl(path: string) {
    return pathToUrlArray(this.serverDto.name, path);
  }

  getAffectedNodes(nodeData: NodeData): NodeData[] {
    let nodes = [];
    nodes.push(nodeData);
    if (nodeData.children) {
      nodeData.children.forEach((x) => nodes.push(...this.getAffectedNodes(x)));
    }
    return nodes;
  }

  dispatchResizeEvent() {
    // sometimes monaco editor is not fully shown, if no resize event is triggered
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 50);
  }

  onKeyDown(event): void {
    let macOS = navigator.platform.match('Mac');

    if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.shiftKey &&
      event.key === 's'
    ) {
      event.preventDefault();
      this.onSaveAllNodesClicked();
    } else if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.key === 's'
    ) {
      event.preventDefault();
      this.onSaveNodeClicked();
    } else if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.key === 'r'
    ) {
      event.preventDefault();
      this.onQuerySubmitClicked(true);
    } else if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.key === 'i'
    ) {
      event.preventDefault();
      event.stopPropagation();
      this.onQuerySubmitClicked(false);
    } else if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.key === 'q'
    ) {
      event.preventDefault();
      this.onCloseTabClicked(this.currentNode);
    }
  }

  openMenu(
    event: MouseEvent,
    contextMenu: MatMenuTrigger,
    nodeData: NodeData
  ): void {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    contextMenu.menuData = { node: nodeData };
    contextMenu.menu.focusFirstItem('mouse');
    contextMenu.openMenu();
  }

  onTabClicked(index: number) {
    if (index < this.openNodes.length) {
      this._router.navigate(this.pathToUrl(this.openNodes[index].path));
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }
}
