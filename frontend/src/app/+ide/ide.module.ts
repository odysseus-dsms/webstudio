import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MonacoEditorModule } from 'ngx-monaco-editor-v2';

import { IdeComponent } from './ide.component';
import { SharedModule } from '../shared/shared.module';
import { AngularSplitModule } from 'angular-split';
import { PackageExplorerComponent } from './package-explorer/package-explorer.component';
import { EditorComponent } from './editor/editor.component';
import { CreateNodeDialogComponent } from './package-explorer/dialog/create/create-node-dialog.component';
import { RenameNodeDialogComponent } from './package-explorer/dialog/rename/rename-node-dialog.component';
import { IdeSinksComponent } from './sinks/sinks.component';
import { IdeQueriesComponent } from './queries/queries.component';
import { IdeStreamsComponent } from './streams/streams.component';
import { IdeDetailComponent } from './detail/details.component';
import { IdeDetailTableComponent } from './detail/table/table.component';
import { IdeDetailGraphComponent } from './detail/graph/graph.component';
import { CommandPipe } from 'app/shared/pipe/command.pipe';
import { SaveChangesDialogComponent } from './editor/save-changes/save-changes-dialog.component';
import { DeleteNodeDialogComponent } from './package-explorer/dialog/delete/delete-dialog.component';
import { IdeDetailPlanComponent } from './detail/plan/plan.component';
import { SaveOpenFilesDialogComponent } from './package-explorer/dialog/save-changes/save-open-files-dialog.component';
import { PasteNodeDialogComponent } from './package-explorer/dialog/paste/paste-dialog.component';
import { VisualEditorComponent } from './editor/visual-editor/visual-editor.component';
import { IdeOperatorsComponent } from './operators/operators.component';
import { ParameterDialogComponent } from './parameter-dialog/parameter-dialog.component';
import { CreateSDFAttributeComponent } from './parameter-dialog/editors/create-sdf-attribute/create-sdf-attribute.component';
import { ShortcutsDialogComponent } from './shortcuts-dialog/shortcuts-dialog.component';

@NgModule({
  declarations: [
    IdeComponent,
    PackageExplorerComponent,
    CreateNodeDialogComponent,
    RenameNodeDialogComponent,
    DeleteNodeDialogComponent,
    SaveOpenFilesDialogComponent,
    PasteNodeDialogComponent,
    EditorComponent,
    SaveChangesDialogComponent,
    IdeStreamsComponent,
    IdeQueriesComponent,
    IdeSinksComponent,
    IdeDetailComponent,
    IdeDetailTableComponent,
    IdeDetailGraphComponent,
    IdeDetailPlanComponent,
    IdeOperatorsComponent,
    CommandPipe,
    VisualEditorComponent,
    ParameterDialogComponent,
    CreateSDFAttributeComponent,
    ShortcutsDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,

    MonacoEditorModule,
    AngularSplitModule,

    RouterModule.forChild([{ path: '', component: IdeComponent }]),
  ],
})
export class IdeModule {}
