import { Component } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';

@Component({
  selector: 'app-ide-shortcuts-dialog',
  templateUrl: './shortcuts-dialog.component.html',
  styleUrls: ['./shortcuts-dialog.component.scss']
})
export class ShortcutsDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<ShortcutsDialogComponent>
  ) {}

  public close(): void {
    this.dialogRef.close();
  }
}
