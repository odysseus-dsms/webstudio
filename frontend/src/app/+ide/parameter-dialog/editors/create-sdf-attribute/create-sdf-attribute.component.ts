import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormGroup, UntypedFormArray, UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { Datatype } from 'app/shared/models/datatype.model';
import { OperatorParameter } from 'app/shared/models/operator-parameter.model';

@Component({
  selector: 'app-parameter-create-sdf-attribute',
  templateUrl: './create-sdf-attribute.component.html',
  styleUrls: ['./create-sdf-attribute.component.scss']
})
export class CreateSDFAttributeComponent implements OnInit {
  @Input() serverDto: ServerDto;
  @Input() param: OperatorParameter;
  @Input() parentForm: UntypedFormGroup;
  @Input() datatypes: Datatype[];

  form: UntypedFormArray;

  constructor(
    private _formBuilder: UntypedFormBuilder
  ) { }

  ngOnInit(): void {
    this.form = new FormArray([]);
    this.parentForm.addControl(`${this.param.parameterName}WSBUILDER`, this.form);

    if (this.param.value) {
      this.addGroupFromString(this.param.value);
    }
  }

  addGroup(): void {
    this.form.push(
      this._formBuilder.group({
        name: ['', [Validators.required]],
        datatype: ['', [Validators.required]],
        constraints: ['']
      })
    );
  }

  removeGroup(index: number): void {
    this.form.removeAt(index);
  }

  getStringValue(): string {
    if (this.form.controls.length < 1) {
      return;
    }

    let value = '[';
    this.form.controls.forEach((group: FormGroup, key, arr) => {
      value = `${value}['${group.controls['name'].value}', '${group.controls['datatype'].value}'`;

      value = group.controls['constraints'].value
        ? `${value}, '${group.controls['constraints'].value}']`
        : `${value}]`

      if (!Object.is(arr.length - 1, key)) {
        value = value + ',';
      }
    });
    value = value + ']';

    return value;
  }

  addGroupFromString(value: string) {
    value = value.replace(new RegExp("'", 'g'), '');
    const attributes = value.match(/(?<=\[)[^\][]*(?=])/g);

    attributes.forEach(a => {
      const config = a.split(',');

      this.form.push(
        this._formBuilder.group({
          name: [config[0].trim(), [Validators.required]],
          datatype: [config[1].trim(), [Validators.required]],
          constraints: [config.length > 2 ? config[2].trim() : '']
        })
      );
    });
  }
}
