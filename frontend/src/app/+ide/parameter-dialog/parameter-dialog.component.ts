import { Component, Inject, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormGroup, UntypedFormGroup, Validators } from '@angular/forms';
import { MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { OperatorDto } from 'app/shared/dto/operator/operator.dto';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { Datatype } from 'app/shared/models/datatype.model';
import { DatatypeService } from 'app/shared/services/api/odysseus-core/datatype.service';
import { typeMap } from '../shared/domain/parameter-type.map';
import { CreateSDFAttributeComponent } from './editors/create-sdf-attribute/create-sdf-attribute.component';

@Component({
  selector: 'app-ide-parameter-dialog',
  templateUrl: './parameter-dialog.component.html',
  styleUrls: ['./parameter-dialog.component.scss']
})
export class ParameterDialogComponent implements OnInit {
  @ViewChildren(CreateSDFAttributeComponent)
  createSDFAttributeComponents?: QueryList<CreateSDFAttributeComponent>;

  serverDto: ServerDto;
  operatorDto: OperatorDto;
  requiredParams: any;
  optionalParams: any;
  step: number;
  typeMap = typeMap;

  requiredFormGroup: UntypedFormGroup;
  optionalFormGroup: UntypedFormGroup;

  datatypes: Datatype[];

  constructor(
    public dialogRef: MatDialogRef<ParameterDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: { operatorDto: OperatorDto, serverDto: ServerDto },
    private _datatypeService: DatatypeService
  ) {
    this._datatypeService.getAll(data.serverDto).subscribe(response => {
      this.datatypes = response;
    });

    this.operatorDto = data.operatorDto;
    this.serverDto = data.serverDto;
  }

  ngOnInit(): void {
    this.requiredParams = this.operatorDto.parameters.filter((p) => p.mandatory && !p.deprecated);
    this.optionalParams = this.operatorDto.parameters.filter((p) => !p.mandatory && !p.deprecated);
    this.step = this.requiredParams.length > 0 ? 0 : 1;

    let group: any = {};
    this.requiredParams.forEach(param => {
      group[param.parameterName] = new FormControl(param.value || '', Validators.required);
    });
    this.requiredFormGroup = new FormGroup(group);

    group = {}
    this.optionalParams.forEach(param => {
      group[param.parameterName] = new FormControl(param.value || '');
    });
    this.optionalFormGroup = new FormGroup(group);
  }

  setStep(index: number): void {
    this.step = index;
  }

  nextStep(): void {
    this.step++;
  }

  prevStep(): void {
    this.step--;
  }

  public cancel(): void {
    this.dialogRef.close();
  }

  public save(): void {
    if (this.requiredFormGroup.valid && this.optionalFormGroup.valid) {
      this.createSDFAttributeComponents?.forEach(component => {
        const control = this.requiredFormGroup.get(component.param.parameterName)
          ? this.requiredFormGroup.get(component.param.parameterName)
          : this.optionalFormGroup.get(component.param.parameterName);

        control.setValue(component.getStringValue());
      });

      this.requiredParams.forEach(
        param => param.value = this.requiredFormGroup.get(param.parameterName).getRawValue()
      );
      this.optionalParams.forEach(
        param => param.value = this.optionalFormGroup.get(param.parameterName).getRawValue()
      );
      this.dialogRef.close('saved');
    }
  }
}
