export const typeMap = new Map<string, string>([
  ['de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.IntegerParameter', 'number'],
  ['de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.LongParameter', 'number'],
  ['de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.DoubleParameter', 'double'],
  ['de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.BooleanParameter', 'radio'],
  ['de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.TimeParameter', 'time'],
  ['de.uniol.inf.is.odysseus.core.server.logicaloperator.builder.CreateSDFAttributeParameter', 'create-sdf']
]);
