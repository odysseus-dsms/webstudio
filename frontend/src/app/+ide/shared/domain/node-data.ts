import { JSONObject } from "@antv/x6";

export class NodeData {
  public model: monaco.editor.ITextModel | null;
  public state: monaco.editor.ICodeEditorViewState | null;
  public lastSavedVersionId: number;
  public isVisual: boolean;
  public graphJson: JSONObject;
  public graphHasChanged: boolean;

  constructor(
    public name: string,
    public parent: NodeData,
    public isDir: boolean,
    public level: number,
    public children?: NodeData[],
    public content?: string
  ) {
    this.isVisual = this.name.endsWith('.vqry');
  }

  hasChanges(): boolean {
    if (!this.isVisual) {
      return (
        this.model &&
        !this.model.isDisposed() &&
        this.lastSavedVersionId !== this.model.getAlternativeVersionId()
      );
    } else {
      return this.graphHasChanged;
    }
  }

  public get path(): string {
    return this.parent ? `${this.parent.path}/${this.name}` : this.name;
  }
}
