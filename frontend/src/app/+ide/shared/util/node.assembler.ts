import { NodeData } from '../domain/node-data';
import { NodeDetails } from '../../../shared/dto/node/node-details';

/**
 * Converts the provided NodeDetails dto to the NodeData domain model with its children recursivelty
 */
export function mapToProjectDomainModels(dto: NodeDetails): NodeData[] {
  return mapToTreeNodesRecursively(null, dto.children, 0);
}

export function mapToTreeNodesRecursively(
  parent: NodeData,
  dtos: NodeDetails[],
  level: number
): NodeData[] {
  return dtos.map((dto) => {
    let node = mapToNodeData(dto, parent, level);
    if (dto.children) {
      node.children = mapToTreeNodesRecursively(node, dto.children, level + 1);
    } else {
      node.content = dto.content;
    }
    return node;
  });
}

/**
 * Converts multiple provided NodeDetails dtos to multiple NodeData domain models
 */
export function mapToNodaDataList(
  parent: NodeData,
  dtos: NodeDetails[],
  level: number
): NodeData[] {
  return dtos.map((dto) => mapToNodeData(dto, parent, level));
}

/**
 * Converts the provided NodeDetails dto to the NodeData domain model
 */
export function mapToNodeData(
  dto: NodeDetails,
  parent?: NodeData,
  level?: number
): NodeData {
  return new NodeData(dto.name, parent, dto.isDir, level);
}
