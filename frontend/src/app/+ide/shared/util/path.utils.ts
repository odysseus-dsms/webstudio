import { NodeData } from '../domain/node-data';

/**
 * Formats the given path to a url array in the angular router syntax. Assumes the ide is behind the /ide path.
 * e.g. /FirstSteps/queries/query1.qry -> ['/servers', '127.0.0.1', 'FirstSteps', 'queries', 'query1.qry']
 */
export function pathToUrlArray(workspace: string, path: string): string[] {
  if (path.startsWith('/')) {
    path = path.substring(1);
  }

  return ['/servers', workspace, ...path.split('/')];
}

/**
 * Formats the given url to the path of the node. Assumes the ide is behind one path segment.
 * e.g. /servers/127.0.0.1/FirstSteps/queries/query1.qry -> 127.0.0.1/FirstSteps/queries/query1.qry
 */
export function urlToPath(url: string): string {
  if (url.startsWith('/')) {
    url = url.substring(1);
  }

  let urlTree = url.split('/');
  urlTree.splice(0, 2);
  return `${urlTree.join('/')}`;
}

/**
 * Returns the NodeData domain model relative to the given nodeDataList at the given path
 */
export function findNodeByPath(
  nodeDataList: NodeData[],
  path: string
): NodeData {
  if (path.startsWith('/')) {
    path = path.substring(1);
  }

  let pathTree = path.split('/');
  let current = pathTree.splice(0, 1);

  let node = nodeDataList.find((x) => x.name === current[0]);
  if (node && pathTree.length > 0) {
    return findNodeByPath(node.children, `/${pathTree.join('/')}`);
  } else {
    return node;
  }
}
