export const odysseusTheme: monaco.editor.IStandaloneThemeData = {
  base: 'vs',
  inherit: true,
  rules: [
    { token: 'keyword', foreground: '#ae414b', fontStyle: 'bold' },
    { token: 'keyword.static', foreground: '#808080' },
    {
      token: 'keyword.deprecated',
      fontStyle: 'italic',
    },
    { token: 'string', foreground: '#406798' },
    { token: 'number', foreground: '#669240' },
    {
      token: 'odysseus-operators',
      foreground: '#1c2a4f',
      fontStyle: 'bold',
    },
    {
      token: 'odysseus-operators.deprecated',
      fontStyle: 'italic',
    },
  ],
  colors: {},
};
