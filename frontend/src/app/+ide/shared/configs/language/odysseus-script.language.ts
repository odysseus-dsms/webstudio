import { OperatorDto } from 'app/shared/dto/operator/operator.dto';
import { ParserTokenDto } from 'app/shared/dto/parser/token.dto';

export const queryLanguageConfiguration: monaco.languages.LanguageConfiguration =
  {
    // the default separators except `@$`
    comments: {
      lineComment: '///',
    },
    brackets: [
      ['{', '}'],
      ['[', ']'],
      ['(', ')'],
    ],
    autoClosingPairs: [
      { open: '{', close: '}' },
      { open: '[', close: ']' },
      { open: '(', close: ')' },
      { open: '"', close: '"' },
      { open: "'", close: "'" },
    ],
    surroundingPairs: [
      { open: '{', close: '}' },
      { open: '[', close: ']' },
      { open: '(', close: ')' },
      { open: '"', close: '"' },
      { open: "'", close: "'" },
    ],
  };

export function getQueryLanguage(
  parserTokenDto: ParserTokenDto,
  operatorDtoList: OperatorDto[]
): monaco.languages.IMonarchLanguage {
  return <monaco.languages.IMonarchLanguage>{
    defaultToken: '',
    tokenPostfix: '.qry',

    deprecatedKeywords: parserTokenDto.DEPRECATED,
    staticKeywords: parserTokenDto.STATIC,
    keywords: parserTokenDto.KEYWORDS,

    deprecatedOdysseusOperators: operatorDtoList
      .filter((x) => x.deprecated === true)
      .map((x) => x.operatorName),
    odysseusOperators: operatorDtoList.map((x) => x.operatorName),

    operators: [
      '=',
      '>',
      '<',
      '!',
      '~',
      '?',
      ':',
      '==',
      '<=',
      '>=',
      '!=',
      '&&',
      '||',
      '++',
      '--',
      '+',
      '-',
      '*',
      '/',
      '&',
      '|',
      '^',
      '%',
      '<<',
      '>>',
      '>>>',
      '+=',
      '-=',
      '*=',
      '/=',
      '&=',
      '|=',
      '^=',
      '%=',
      '<<=',
      '>>=',
      '>>>=',
    ],

    // we include these common regular expressions
    symbols: /[=><!~?:&|+\-*\/\^%]+/,
    escapes:
      /\\(?:[abfnrtv\\"']|x[0-9A-Fa-f]{1,4}|u[0-9A-Fa-f]{4}|U[0-9A-Fa-f]{8})/,
    digits: /\d+(_+\d+)*/,
    octaldigits: /[0-7]+(_+[0-7]+)*/,
    binarydigits: /[0-1]+(_+[0-1]+)*/,
    hexdigits: /[[0-9a-fA-F]+(_+[0-9a-fA-F]+)*/,

    // The main tokenizer for our languages
    tokenizer: {
      root: [
        // identifiers and keywords
        [
          /[a-zA-Z_\#][\w$]*/,
          {
            cases: {
              '@deprecatedKeywords': { token: 'keyword.deprecated' },
              '@staticKeywords': { token: 'keyword.static' },
              '@keywords': { token: 'keyword.$0' },
              '@deprecatedOdysseusOperators': {
                token: 'odysseus-operators.deprecated',
              },
              '@odysseusOperators': { token: 'odysseus-operators' },
              '@default': 'identifier',
            },
          },
        ],

        // whitespace
        { include: '@whitespace' },

        // delimiters and operators
        [/[{}()\[\]]/, '@brackets'],
        [/[<>](?!@symbols)/, '@brackets'],
        [
          /@symbols/,
          {
            cases: {
              '@operators': 'delimiter',
              '@default': '',
            },
          },
        ],

        // numbers
        [/(@digits)[eE]([\-+]?(@digits))?[fFdD]?/, 'number.float'],
        [/(@digits)\.(@digits)([eE][\-+]?(@digits))?[fFdD]?/, 'number.float'],
        [/0[xX](@hexdigits)[Ll]?/, 'number.hex'],
        [/0(@octaldigits)[Ll]?/, 'number.octal'],
        [/0[bB](@binarydigits)[Ll]?/, 'number.binary'],
        [/(@digits)[fFdD]/, 'number.float'],
        [/(@digits)[lL]?/, 'number'],

        // delimiter: after number because of .\d floats
        [/[;,.]/, 'delimiter'],

        // strings
        [/'([^'\\]|\\.)*$/, 'string.invalid'], // non-teminated string
        [/'/, 'string', '@string'],
      ],

      whitespace: [
        [/[ \t\r\n]+/, ''],
        [/\/\/\/.*$/, 'comment'],
      ],

      string: [
        [/[^\\']+/, 'string'],
        [/@escapes/, 'string.escape'],
        [/\\./, 'string.escape.invalid'],
        [/'/, 'string', '@pop'],
      ],
    },
  };
}

export function getKeywordsCompletionItems(
  parserTokenDto: ParserTokenDto
): monaco.languages.CompletionItemProvider {
  return <monaco.languages.CompletionItemProvider>{
    triggerCharacters: ['#'],
    provideCompletionItems: (model, position, context, token) => {
      return <monaco.languages.CompletionList>{
        suggestions: parserTokenDto.KEYWORDS.map((token) => ({
          label: token,
          kind: monaco.languages.CompletionItemKind.Keyword,
          insertText: token,
          range: {
            startLineNumber: position.lineNumber,
            endLineNumber: position.lineNumber,
            startColumn: model.getWordUntilPosition(position).startColumn - 1, // offset the # since it doesn't count as Word
            endColumn: model.getWordUntilPosition(position).endColumn,
          },
        })),
      };
    },
  };
}

export function getOperatorsCompletionItems(
  operatorDtoList: OperatorDto[]
): monaco.languages.CompletionItemProvider {
  return <monaco.languages.CompletionItemProvider>{
    provideCompletionItems: (model, position, context, token) => {
      return <monaco.languages.CompletionList>{
        suggestions: operatorDtoList.map((operator) => ({
          label: operator.operatorName,
          detail: operator.doc,
          kind: monaco.languages.CompletionItemKind.Struct,
          insertText: operator.operatorName,
          range: {
            startLineNumber: position.lineNumber,
            endLineNumber: position.lineNumber,
            startColumn: model.getWordUntilPosition(position).startColumn,
            endColumn: model.getWordUntilPosition(position).endColumn,
          },
        })),
      };
    },
  };
}
