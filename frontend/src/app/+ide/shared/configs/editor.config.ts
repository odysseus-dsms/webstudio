import { NgxMonacoEditorConfig } from 'ngx-monaco-editor-v2';

import { odysseusTheme } from './themes/odysseus.theme';

export const editorConfig: NgxMonacoEditorConfig = {
  defaultOptions: {
    scrollBeyondLastLine: false,
    minimap: { enabled: false },
    theme: 'OdysseusTheme',
  },
  onMonacoLoad() {
    monaco.editor.defineTheme('OdysseusTheme', odysseusTheme);
  },
};
