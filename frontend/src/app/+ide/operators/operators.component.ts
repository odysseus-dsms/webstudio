import { Component, Input, ViewChild, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';

import { OperatorDto } from 'app/shared/dto/operator/operator.dto';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { OperatorService } from 'app/shared/services/api/odysseus-core/operator.service';
import { AuthState } from 'app/shared/states/auth.state';
import { ServerState } from 'app/shared/states/server.state';
import { defineNodeWidth } from 'app/util/graph.utils';

import { Graph, Shape } from '@antv/x6';
import { Stencil } from '@antv/x6-plugin-stencil';

@Component({
  selector: 'app-ide-operators',
  templateUrl: './operators.component.html',
  styleUrls: ['./operators.component.scss']
})
export class IdeOperatorsComponent implements OnInit {
  @Input() graph: Graph;

  @ViewChild('stencilContainer') stencilContainer: ElementRef;

  serverDto: ServerDto;
  operators: OperatorDto[];

  stencil: Stencil;
  groups: Array<any> = [];
  operatorNodes: Array<any> = [];

  constructor(
    private _store: Store,
    private _route: ActivatedRoute,
    private _operatorService: OperatorService
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );

      this.loadOperatorsAndGroups();
    });
  }

  loadOperatorsAndGroups(): void {
    this._operatorService
      .findAllOperators(this.serverDto)
      .subscribe((operators: OperatorDto[]) => {
        this.operators = operators.filter(o => !o.deprecated && !o.hidden);

        this.operators.map(o => o.categories.map(c => {
          if (!this.groups.includes(c)) {
            this.groups.push(c);
          }
        }));
        this.groups.forEach((g, key) => {
          this.groups[key] = {
            name: g,
            title: g
          };
        });

        this.createOperatorNodes();
      });
  }

  createOperatorNodes(): void {
    this.operators.forEach(operator => {
      const portItems: Array<{
        group: 'in' | 'out';
      }> = [];

      for (let i = 0; i < operator.minPorts; i++) {
        portItems.push({ group: 'in' });
      }
      if (operator.maxPorts !== 0 && portItems.length === 0) {
        portItems.push({ group: 'in' });
      }
      if (!operator.categories.includes('SINK')) {
        portItems.push({ group: 'out' });
      }

      operator.categories.map(c => {
        let group = this.operatorNodes.find(o => o.category === c);
        if (!group) {
          this.operatorNodes.push({
            category: c,
            nodes: []
          });
          group = this.operatorNodes.find(o => o.category === c);
        }

        group.nodes.push(new Shape.Rect({
          shape: 'rect',
          size: {
            width: 100,
            height: 40
          },
          label: operator.operatorName,
          data: {
            operator
          },
          ports: {
            items: portItems
          }
        }));
      });
    });

    this.initStencil();
  }

  initStencil(): void {
    const groups = this.groups;
    this.stencil = new Stencil({
      title: 'Operators',
      target: this.graph,
      search(cell, keyword) {
        return cell.data.operator.operatorName.toLowerCase().indexOf(keyword.toLowerCase()) !== -1
      },
      placeholder: 'Search by name',
      collapsable: true,
      stencilGraphWidth: 250,
      stencilGraphHeight: 160,
      groups,
      layoutOptions: {
        rowHeight: 60,
      },
      getDragNode(node) {
        if (defineNodeWidth(node.data.operator.operatorName.length) > node.size().width) {
          return node.clone().size(defineNodeWidth(node.data.operator.operatorName.length), 40);
        }
        return node.clone().size(node.size().width, 40);
      }
    });
    this.stencilContainer.nativeElement.appendChild(this.stencil.container);

    this.operatorNodes.forEach((o) => {
      o.nodes.sort((a, b) => a.data.operator.operatorName.localeCompare(b.data.operator.operatorName));
      this.stencil.load([...o.nodes], o.category);
      this.stencil.resizeGroup(o.category, {
        width: 250,
        height: Math.ceil(o.nodes.length / 2) * 60 + 20
      });
    });
  }
}
