import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { ServerState } from 'app/shared/states/server.state';
import { Store } from '@ngxs/store';
import { Subject } from 'rxjs';
import { MatLegacyTableDataSource as MatTableDataSource } from '@angular/material/legacy-table';
import { takeUntil } from 'rxjs/operators';
import { WebSocketService } from 'app/shared/services/api/odysseus-core/websocket.service';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-ide-detail-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class IdeDetailTableComponent implements OnInit, OnDestroy {
  @Input() query: QueryPreview;

  serverDto: ServerDto;
  destroyed$ = new Subject<void>();
  dataSource = new MatTableDataSource<any>([]);
  displayedColumns: string[];
  paused = false;
  maxResults = '10';
  results = [];

  constructor(
    private _route: ActivatedRoute,
    private _store: Store,
    private _webSocketService: WebSocketService
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );

      this.destroyed$.next();
    });

    this.displayedColumns =
      this.query.rootOperators[0].ports[0].schema.attributes.map(
        (x) => x.attributename
      );
    this.displayedColumns.push('metadata');

    this.createResultConnection();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  createResultConnection(): void {
    this._webSocketService
      .connectToQueryResult(this.serverDto, this.query)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((res) => {
        const lastIndex = res.lastIndexOf(';');
        const data = res.substr(0, lastIndex);
        const metaData = res.substr(lastIndex + 1, res.length);

        const jsonData = JSON.parse(data);
        jsonData.metadata = metaData;

        if (this.results.length > +this.maxResults - 1) {
          this.results.length = +this.maxResults - 1;
        }

        this.results.unshift(jsonData);
        this.dataSource.data = this.results;
      });
  }

  onPauseConnectionClicked(): void {
    this.pauseConnection();
  }

  pauseConnection() {
    this.paused ? this.createResultConnection() : this.destroyed$.next();
    this.paused = !this.paused;
  }
}
