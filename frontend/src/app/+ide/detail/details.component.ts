import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { ServerState } from 'app/shared/states/server.state';
import { Store } from '@ngxs/store';
import { UntypedFormControl } from '@angular/forms';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-ide-detail',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class IdeDetailComponent implements OnInit {
  serverDto: ServerDto;
  tabs: Tab[];
  selected = new UntypedFormControl(0);

  contextMenuPosition = { x: '0px', y: '0px' };

  constructor(
    private _route: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef,
    private _store: Store
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );

      this.tabs = [];
    });
  }

  showQueryGraph(query: QueryPreview) {
    let index = this.tabs.findIndex(
      (x) => x.data === query && x.type === 'graph'
    );

    if (index === -1) {
      this.tabs.push({ type: 'graph', data: query });
      this.selected.setValue(this.tabs.length - 1);
      this.changeDetectorRef.detectChanges();
    } else {
      this.selected.setValue(index);
    }
  }

  showPlan(queries: QueryPreview[]) {
    let index = this.tabs.findIndex((x) => x.type === 'plan');

    if (index === -1) {
      this.tabs.push({ type: 'plan', data: queries });
      this.selected.setValue(this.tabs.length - 1);
      this.changeDetectorRef.detectChanges();
    } else {
      this.selected.setValue(index);
    }
  }

  showQueryTable(query: QueryPreview) {
    let index = this.tabs.findIndex(
      (x) => x.data === query && x.type === 'table'
    );

    if (index === -1) {
      this.tabs.push({ type: 'table', data: query });
      this.selected.setValue(this.tabs.length - 1);
      this.changeDetectorRef.detectChanges();
    } else {
      this.selected.setValue(index);
    }
  }

  onCloseTabClicked(index: number) {
    this.tabs.splice(index, 1);
  }
}

class Tab {
  type: string;
  data: QueryPreview | QueryPreview[];
}
