import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';

import { ServerState } from 'app/shared/states/server.state';
import { ServerDto } from 'app/shared/dto/server/server.dto';

import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-ide-detail-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.scss'],
})
export class IdeDetailPlanComponent implements OnInit {
  serverDto: ServerDto;

  @Input() queries: QueryPreview;

  constructor(private _route: ActivatedRoute, private _store: Store) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );
    });
  }
}
