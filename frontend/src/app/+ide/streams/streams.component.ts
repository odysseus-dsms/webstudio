import { Component, OnInit, Output, EventEmitter, AfterViewInit, ViewChild, ElementRef, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';

import { ServerDto } from 'app/shared/dto/server/server.dto';
import { ServerState } from 'app/shared/states/server.state';
import { Resource } from 'app/shared/models/resource.model';
import { Attribute } from 'app/shared/models/attribute.model';
import { StreamService } from 'app/shared/services/api/odysseus-core/stream.service';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { MatLegacyMenuTrigger as MatMenuTrigger } from '@angular/material/legacy-menu';
import { QueryService } from 'app/shared/services/api/odysseus-core/query.service';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { MessageService } from 'app/shared/services/message.service';
import { AuthState } from 'app/shared/states/auth.state';
import { defineNodeWidth } from 'app/util/graph.utils';

import { Graph } from '@antv/x6';
import { Dnd } from '@antv/x6-plugin-dnd';

@Component({
  selector: 'app-ide-streams',
  templateUrl: './streams.component.html',
  styleUrls: ['./streams.component.scss'],
})
export class IdeStreamsComponent implements OnInit, AfterViewInit {
  @Input() graph: Graph|null;

  @Output() onStreamQueried = new EventEmitter<Resource>();

  @ViewChild('dndContainer') dndContainer: ElementRef;

  dnd: Dnd;
  serverDto: ServerDto;
  streams: Resource[];

  treeControl: NestedTreeControl<Resource | Attribute | any>;
  dataSource: MatTreeNestedDataSource<Resource | Attribute | any>;

  contextMenuPosition = { x: '0px', y: '0px' };

  constructor(
    private _route: ActivatedRoute,
    private _store: Store,
    private _streamService: StreamService,
    private _queryService: QueryService,
    private _messageService: MessageService
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );

      this.treeControl = new NestedTreeControl<Resource | Attribute | any>(
        (node) => {
          if (this.isResource(node)) {
            return (node as Resource).schema?.attributes;
          } else if (this.isAttribute(node)) {
            return (node as Attribute).subschema?.attributes;
          } else {
            return null;
          }
        }
      );
      this.dataSource = new MatTreeNestedDataSource<
        Resource | Attribute | any
      >();

      this.loadAllStreams();
    });
  }

  ngAfterViewInit(): void {
    this.dnd = new Dnd({
      target: null,
      dndContainer: this.dndContainer.nativeElement
    });
  }

  loadAllStreams() {
    this._streamService.getAll(this.serverDto).subscribe((response) => {
      this.streams = response;
      this.dataSource.data = this.streams;
      this.treeControl.dataNodes = this.streams;
    });
  }

  hasChild = (_: number, node: Resource | Attribute) => {
    if (this.isResource(node)) {
      return (
        (node as Resource).schema?.attributes &&
        (node as Resource).schema.attributes.length > 0
      );
    } else if (this.isAttribute(node)) {
      return (
        (node as Attribute).subschema?.attributes &&
        (node as Attribute).subschema.attributes.length > 0
      );
    } else {
      return false;
    }
  };

  isResource(node: any): boolean {
    return !!node.name;
  }

  isAttribute(node: any): boolean {
    return !!node.attributename;
  }

  onQueryClicked(stream: Resource) {
    const query: QueryPreview = {
      parser: 'OdysseusScript',
      queryText: `#PARSER PQL\n#RUNQUERY\nout = ${stream.name}`,
    };

    this._queryService.create(this.serverDto, query).subscribe((res) => {
      if (res) {
        this.onStreamQueried.emit(stream);
        this._messageService.showMessage('Source queried.');
      }
    });
  }

  onDeleteClicked(stream: Resource) {
    this._streamService.delete(this.serverDto, stream).subscribe(() => {
      this._messageService.showMessage('Source deleted.');
    });
  }

  onExpandAllClicked() {
    this.treeControl.expandAll();
  }

  onCollapseAllClicked() {
    this.treeControl.collapseAll();
  }

  onDeleteAllClicked() {
    this.streams.forEach((x) => {
      this._streamService.delete(this.serverDto, x).subscribe(() => {
        this._messageService.showMessage('All sources deleted.');
      });
    });
  }

  openMenu(event: MouseEvent, contextMenu: MatMenuTrigger): void {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    contextMenu.menu.focusFirstItem('mouse');
    contextMenu.openMenu();
  }

  startDrag(event: MouseEvent, node: Resource) {
    if (!this.graph || event.button !== 0) {
      return;
    }

    const graphNode = this.graph.createNode({
      width: defineNodeWidth(node.name.length),
      height: 40,
      label: node.name,
      ports: {
        items: [{
          group: 'out'
        }]
      }
    });

    this.dnd.options.target = this.graph;
    this.dnd.start(graphNode, event);
  }
}
