import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';

import { ServerDto } from 'app/shared/dto/server/server.dto';
import { ServerState } from 'app/shared/states/server.state';
import { SinkService } from 'app/shared/services/api/odysseus-core/sink.service';
import { Resource } from 'app/shared/models/resource.model';
import { Attribute } from 'app/shared/models/attribute.model';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { MessageService } from 'app/shared/services/message.service';
import { MatLegacyMenuTrigger as MatMenuTrigger } from '@angular/material/legacy-menu';
import { AuthState } from 'app/shared/states/auth.state';
import { defineNodeWidth } from 'app/util/graph.utils';
import { OperatorDto } from 'app/shared/dto/operator/operator.dto';
import { OperatorService } from 'app/shared/services/api/odysseus-core/operator.service';

import { Graph } from '@antv/x6';
import { Dnd } from '@antv/x6-plugin-dnd';

@Component({
  selector: 'app-ide-sinks',
  templateUrl: './sinks.component.html',
  styleUrls: ['./sinks.component.scss'],
})
export class IdeSinksComponent implements OnInit, AfterViewInit {
  @Input() graph: Graph|null;

  @ViewChild('dndContainer') dndContainer: ElementRef;

  dnd: Dnd;

  serverDto: ServerDto;
  sinks: Resource[];
  sinkOperator: OperatorDto;

  treeControl: NestedTreeControl<Resource | Attribute | any>;
  dataSource: MatTreeNestedDataSource<Resource | Attribute | any>;

  contextMenuPosition = { x: '0px', y: '0px' };

  constructor(
    private _route: ActivatedRoute,
    private _store: Store,
    private _sinkService: SinkService,
    private _operatorService: OperatorService,
    private _messageService: MessageService,
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );

      this._operatorService
        .findAllOperators(this.serverDto)
        .subscribe((operators: OperatorDto[]) => {
          this.sinkOperator = operators.find(o => o.operatorName.toLowerCase() === 'sink');
        });

      this.treeControl = new NestedTreeControl<Resource | Attribute | any>(
        (node) => {
          if (this.isResource(node)) {
            return (node as Resource).schema?.attributes;
          } else if (this.isAttribute(node)) {
            return (node as Attribute).subschema?.attributes;
          } else {
            return null;
          }
        }
      );
      this.dataSource = new MatTreeNestedDataSource<
        Resource | Attribute | any
      >();

      this.loadAllSinks();
    });
  }

  ngAfterViewInit(): void {
    this.dnd = new Dnd({
      target: null,
      dndContainer: this.dndContainer.nativeElement,
      getDropNode(node) {
        const sinkNode = node.clone();
        sinkNode.data.operator.parameters
          .find(p => p.parameterName === 'SINK')
          .value = sinkNode.attr('text/text');
        return sinkNode;
      }
    });
  }

  loadAllSinks() {
    this._sinkService.getAll(this.serverDto).subscribe((response) => {
      this.sinks = response;
      this.dataSource.data = this.sinks;
      this.treeControl.dataNodes = this.sinks;
    });
  }

  hasChild = (_: number, node: Resource | Attribute) => {
    if (this.isResource(node)) {
      return (
        (node as Resource).schema?.attributes &&
        (node as Resource).schema.attributes.length > 0
      );
    } else if (this.isAttribute(node)) {
      return (
        (node as Attribute).subschema?.attributes &&
        (node as Attribute).subschema.attributes.length > 0
      );
    } else {
      return false;
    }
  };

  isResource(node: any): boolean {
    return !!node.name;
  }

  isAttribute(node: any): boolean {
    return !!node.attributename;
  }

  onDeleteClicked(sink: Resource) {
    this._sinkService.delete(this.serverDto, sink).subscribe(() => {
      this._messageService.showMessage('Sink deleted.');
    });
  }

  onExpandAllClicked() {
    this.treeControl.expandAll();
  }

  onCollapseAllClicked() {
    this.treeControl.collapseAll();
  }

  onDeleteAllClicked() {
    this.sinks.forEach((x) => {
      this._sinkService.delete(this.serverDto, x).subscribe(() => {
        this._messageService.showMessage('All sinks deleted.');
      });
    });
  }

  openMenu(event: MouseEvent, contextMenu: MatMenuTrigger): void {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    contextMenu.menu.focusFirstItem('mouse');
    contextMenu.openMenu();
  }

  startDrag(event: MouseEvent, node: Resource) {
    if (!this.graph || event.button !== 0) {
      return;
    }

    const graphNode = this.graph.createNode({
      width: defineNodeWidth(node.name.length),
      height: 40,
      label: node.name,
      data: {
        operator: this.sinkOperator
      },
      ports: {
        items: [{
          group: 'in'
        }]
      }
    });

    this.dnd.options.target = this.graph;
    this.dnd.start(graphNode, event);
  }
}
