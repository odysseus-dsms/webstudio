import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';

import { ServerDto } from 'app/shared/dto/server/server.dto';
import { ServerState } from 'app/shared/states/server.state';
import { QueryService } from 'app/shared/services/api/odysseus-core/query.service';
import { MatLegacyMenuTrigger as MatMenuTrigger } from '@angular/material/legacy-menu';
import { MessageService } from 'app/shared/services/message.service';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-ide-queries',
  templateUrl: './queries.component.html',
  styleUrls: ['./queries.component.scss']
})
export class IdeQueriesComponent implements OnInit {
  @Output() onQueryGraph = new EventEmitter<QueryPreview>();
  @Output() onQueryTabled = new EventEmitter<QueryPreview>();
  @Output() onShowPlan = new EventEmitter<QueryPreview[]>();

  serverDto: ServerDto;
  queries: QueryPreview[];

  displayedColumns: string[] = ['id', 'name', 'state', 'parser', 'user', 'queryText'];

  contextMenuPosition = { x: '0px', y: '0px' };

  constructor(
    private _route: ActivatedRoute,
    private _store: Store,
    private _queryService: QueryService,
    private _messageService: MessageService
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );

      this.loadAllQueries();
    });
  }

  loadAllQueries() {
    this._queryService.getAll(this.serverDto).subscribe((response) => {
      this.queries = response;
    });
  }

  onStartClicked(query: QueryPreview) {
    this._queryService
      .changeState(this.serverDto, query, 'RUNNING')
      .subscribe(() => {
        this._messageService.showMessage('Query started.');
      });
  }

  onStopClicked(query: QueryPreview) {
    this._queryService
      .changeState(this.serverDto, query, 'INACTIVE')
      .subscribe(() => {
        this._messageService.showMessage('Query stopped.');
      });
  }

  onGraphClicked(query: QueryPreview) {
    this.onQueryGraph.emit(query);
  }

  onTabledClicked(query: QueryPreview) {
    this.onQueryTabled.emit(query);
  }

  onShowPlanClicked() {
    this.onShowPlan.emit(this.queries);
  }

  onDeleteClicked(query: QueryPreview) {
    this._queryService.delete(this.serverDto, query).subscribe(() => {
      this._messageService.showMessage('Query deleted.');
    });
  }

  onDeleteAllClicked() {
    this.queries.forEach((x) => {
      this._queryService.delete(this.serverDto, x).subscribe(() => {
        this._messageService.showMessage('All queries deleted.');
      });
    });
  }

  isRunning(query: QueryPreview) {
    return query.state === 'RUNNING';
  }

  isInactive(query: QueryPreview) {
    return query.state === 'INACTIVE';
  }

  openMenu(
    event: MouseEvent,
    contextMenu: MatMenuTrigger,
    query: QueryPreview
  ): void {
    event.preventDefault();
    this.contextMenuPosition.x = event.clientX + 'px';
    this.contextMenuPosition.y = event.clientY + 'px';
    contextMenu.menuData = { query: query };
    contextMenu.menu.focusFirstItem('mouse');
    contextMenu.openMenu();
  }
}
