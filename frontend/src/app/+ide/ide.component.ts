import {
  AfterViewInit,
  Component,
  HostListener,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { SplitComponent } from 'angular-split';

import { NodeService } from '../shared/services/api/webstudio/node.service';
import { NodeDetails } from '../shared/dto/node/node-details';
import { EditorComponent } from './editor/editor.component';
import { PackageExplorerComponent } from './package-explorer/package-explorer.component';
import { findNodeByPath, urlToPath } from './shared/util/path.utils';
import { NodeData } from './shared/domain/node-data';
import {
  mapToNodaDataList,
  mapToProjectDomainModels,
} from './shared/util/node.assembler';
import { ServerState } from 'app/shared/states/server.state';
import { Store } from '@ngxs/store';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { IdeQueriesComponent } from './queries/queries.component';
import { IdeStreamsComponent } from './streams/streams.component';
import { IdeSinksComponent } from './sinks/sinks.component';
import { IdeDetailComponent } from './detail/details.component';
import { Resource } from 'app/shared/models/resource.model';
import { ServicesService } from 'app/shared/services/api/odysseus-core/services.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { WebSocketService } from 'app/shared/services/api/odysseus-core/websocket.service';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { AuthState } from 'app/shared/states/auth.state';
import { OperatorDto } from 'app/shared/dto/operator/operator.dto';
import { Node } from '@antv/x6';
import { MatLegacyPaginator as MatPaginator } from '@angular/material/legacy-paginator';
import { MatSort } from '@angular/material/sort';
import { MatLegacyTableDataSource as MatTableDataSource } from '@angular/material/legacy-table';

@Component({
  selector: 'app-ide',
  templateUrl: './ide.component.html',
  styleUrls: ['./ide.component.scss'],
})
export class IdeComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('mainSplit') mainSplit: SplitComponent;
  @ViewChild('centerSplit') centerSplit: SplitComponent;
  @ViewChild('rightSplit') rightSplit: SplitComponent;

  @ViewChild(PackageExplorerComponent)
  packageExplorerComponent: PackageExplorerComponent;
  @ViewChild(EditorComponent) editorComponent: EditorComponent;
  @ViewChild(IdeStreamsComponent) ideStreamsComponent: IdeStreamsComponent;
  @ViewChild(IdeQueriesComponent) ideQueriesComponent: IdeQueriesComponent;
  @ViewChild(IdeSinksComponent) ideSinksComponent: IdeSinksComponent;
  @ViewChild(IdeDetailComponent) ideDetailsComponent: IdeDetailComponent;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  serverDto: ServerDto;
  isVisualEditor: boolean;

  projects: NodeData[];
  selectedGraphOp: OperatorDto|null;
  paramDataSource = new MatTableDataSource<any>();

  destroyed$ = new Subject<void>();

  constructor(
    private _router: Router,
    private _store: Store,
    private _route: ActivatedRoute,
    private _nodeService: NodeService,
    private _servicesService: ServicesService,
    private _webSocketService: WebSocketService
  ) {}

  ngOnInit(): void {
    this._route.params.subscribe((params) => {
      this.destroyed$.next();
      this.projects = null;
      this.isVisualEditor = false;

      this.serverDto = this._store.selectSnapshot(
        ServerState.getDtoByName(
          params.server,
          this._store.selectSnapshot(AuthState.username)
        )
      );

      this.initInitialUrl(decodeURIComponent(this._router.url));
      this.initEvents();
    });

    this._router.events.subscribe((x) => {
      if (x instanceof NavigationEnd) {
        if (this.projects) {
          this.onUrlChanged(decodeURIComponent(x.url));
        } else {
          // handle invalid url
          this.initInitialUrl(decodeURIComponent(x.url));
        }
      }
    });
  }

  ngOnDestroy() {
    this.destroyed$.next();
  }

  ngAfterViewInit() {
    this.mainSplit.dragProgress$.subscribe(() => this.onSplitPaneResized());
    this.rightSplit.dragProgress$.subscribe(() => this.onSplitPaneResized());
    this.centerSplit.dragProgress$.subscribe(() => this.onSplitPaneResized());
  }

  initInitialUrl(url: string) {
    let workingDirectory = urlToPath(url);
    let loadParents = true;

    this.loadNode(
      workingDirectory,
      (response: NodeDetails) => {
        this.projects = mapToProjectDomainModels(response);
        this.packageExplorerComponent.onWorkspaceLoaded(this.projects);

        let node = findNodeByPath(this.projects, workingDirectory);
        if (node) {
          this.packageExplorerComponent.onNodeCreated(node);

          if (!node.isDir) {
            this.editorComponent.onNodeCreated(node);
          }
        } else {
          // error
        }
      },
      loadParents
    );
  }

  onUrlChanged(url: string) {
    const workingDirectory = urlToPath(url);
    const node = findNodeByPath(this.projects, workingDirectory);
    this.selectedGraphOp = null;

    if (node) {
      this.loadOrRestoreNode(node);
    } else {
      // file don't exist
    }
  }

  loadOrRestoreNode(node: NodeData) {
    if (node.isDir) {
      if (node.children) {
        // restore cached folder
        this.packageExplorerComponent.onNodeRestored(node);
      } else {
        // load folder
        this.loadNode(node.path, (response: NodeData) => {
          node.children = mapToNodaDataList(
            node,
            response.children,
            node.level + 1
          );
          this.packageExplorerComponent.onNodeCreated(node);
        });
      }
    } else {
      if (node.content || node.content === '') {
        // restore cached file
        this.packageExplorerComponent.onNodeRestored(node);
        this.editorComponent.onNodeRestored(node);
      } else {
        // load file
        this.loadNode(node.path, (response: NodeData) => {
          node.content = response.content;
          this.editorComponent.onNodeCreated(node);
        });
      }
    }
  }

  loadNode(
    path: string,
    action: (response: NodeDetails) => void,
    loadParents: boolean = false
  ) {
    this._nodeService.findNode(this.serverDto, path, loadParents).subscribe(
      (response: NodeDetails) => {
        action(response);
      },
      (error) => {
        this._router.navigate(['/servers', this.serverDto.name]);
      }
    );
  }

  onSplitPaneResized() {
    window.dispatchEvent(new Event('resize'));
  }

  onNodeDeleted(nodeData: NodeData) {
    this.editorComponent.closeTab(nodeData);

    if (nodeData.children) {
      nodeData.children.forEach((x) => this.onNodeRefreshed(x));
    }
  }

  onNodeRefreshed(nodeData: NodeData) {
    this.editorComponent.closeTab(nodeData);

    if (nodeData.children) {
      nodeData.children.forEach((x) => this.onNodeRefreshed(x));
    }
  }

  onSaveNodes(event: { nodeDataList: NodeData[]; fn: () => void }) {
    event.nodeDataList.forEach((x) => this.editorComponent.saveNode(x));
  }

  onStreamQueried(stream: Resource) {
    this.ideQueriesComponent.loadAllQueries();
  }

  onQueryGraph(query: QueryPreview) {
    this.ideDetailsComponent.showQueryGraph(query);
  }

  onShowPlan(queries: QueryPreview[]) {
    this.ideDetailsComponent.showPlan(queries);
  }

  onQueryTabled(query: QueryPreview) {
    this.ideDetailsComponent.showQueryTable(query);
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  getStreams(): number {
    return this.ideStreamsComponent?.streams?.length;
  }

  getSinks(): number {
    return this.ideSinksComponent?.sinks?.length;
  }

  getQueries(): number {
    return this.ideQueriesComponent?.queries?.length;
  }

  initEvents() {
    this._servicesService
      .events(this.serverDto)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((event) => {
        this._webSocketService
          .connectToDataDictionaryEvent(this.serverDto, event)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            this.ideStreamsComponent.loadAllStreams();
            this.ideSinksComponent.loadAllSinks();
          });

        this._webSocketService
          .connectToQueryEvent(this.serverDto, event)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            this.ideQueriesComponent.loadAllQueries();
          });

        this._webSocketService
          .connectToErrorEvent(this.serverDto, event)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            this.ideQueriesComponent.loadAllQueries();
          });
      });
  }

  onEditorChanged(isVisual: boolean) {
    this.isVisualEditor = isVisual;
  }

  onGraphNodeSelectionChanged(graphNode: Node|null) {
    if (graphNode && graphNode.data?.operator) {
      this.paramDataSource.paginator = this.paginator;
      this.paramDataSource.sort = this.sort;

      this.selectedGraphOp = graphNode.data.operator;
      this.paramDataSource.data = this.selectedGraphOp.parameters.filter(p => p.value);
    } else {
      this.selectedGraphOp = null;
    }
  }

  @HostListener('window:keydown', ['$event'])
  onKeyDown(event): void {
    let macOS = navigator.platform.match('Mac');

    if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.key === 'z'
    ) {
      event.preventDefault();
      event.stopPropagation();
      this.packageExplorerComponent.onCreateProjectClicked();
    } else if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.key === 'i'
    ) {
      event.preventDefault();
      event.stopPropagation();
      this.packageExplorerComponent.onCreateFolderClicked();
    } else if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.key === ''
    ) {
      event.preventDefault();
      event.stopPropagation();
      this.packageExplorerComponent.onCreateFileClicked();
    } else if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.shiftKey &&
      event.key === 'r'
    ) {
      event.preventDefault();
      event.stopPropagation();
      this.packageExplorerComponent.onRefreshClicked();
    } else if (
      ((macOS && event.metaKey) || (!macOS && event.ctrlKey)) &&
      event.shiftKey &&
      event.key === 'ü'
    ) {
      event.preventDefault();
      event.stopPropagation();
      this.packageExplorerComponent.onDownloadClicked();
    } else if (event.key === 'F2') {
      event.preventDefault();
      event.stopPropagation();
      this.packageExplorerComponent.onRenameClicked();
    } else if (event.key === 'Delete') {
      event.preventDefault();
      event.stopPropagation();
      this.packageExplorerComponent.onDeleteClicked();
    }
  }
}
