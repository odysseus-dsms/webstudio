import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MonitoringComponent } from './monitoring.component';
import { SharedModule } from 'app/shared/shared.module';
import { AngularSplitModule } from 'angular-split';
import { GaugeChartComponent } from './gauge-chart/gauge-chart.component';
import { NgChartsModule } from 'ng2-charts';
import { NetworkChartComponent } from './network-chart/network-chart.component';
import { ResourceDialogComponent } from './resource-dialog/resource-dialog.component';
import { LogDialogComponent } from './log-dialog/log-dialog.component';

@NgModule({
  declarations: [
    MonitoringComponent,
    GaugeChartComponent,
    NetworkChartComponent,
    ResourceDialogComponent,
    LogDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AngularSplitModule,
    NgChartsModule
  ]
})
export class MonitoringModule { }
