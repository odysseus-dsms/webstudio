import { Component, Inject, OnInit } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { LoggingService } from 'app/shared/services/api/odysseus-core/logging.service';
import { Subject, takeUntil, timer } from 'rxjs';

@Component({
  selector: 'app-monitoring-log-dialog',
  templateUrl: './log-dialog.component.html'
})
export class LogDialogComponent implements OnInit {
  serverDto: ServerDto;
  logContent: string;

  destroyed$ = new Subject<void>();

  constructor(
    public dialogRef: MatDialogRef<LogDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ServerDto,
    private _logService: LoggingService
  ) {
    this.serverDto = data;
  }

  ngOnInit(): void {
    timer(1, 5000).subscribe(() => {
      this._logService
        .getAll(this.serverDto)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(response => this.logContent = response.slice(response.length - 100000, response.length));
    });
  }

  public close(): void {
    this.dialogRef.close();
    this.destroyed$.next();
  }
}
