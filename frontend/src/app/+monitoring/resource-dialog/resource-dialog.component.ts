import { Component, Inject } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { ServerSystemLoads } from 'app/shared/domain/server-system-loads.domain';

@Component({
  selector: 'app-monitoring-resource-dialog',
  templateUrl: './resource-dialog.component.html',
  styleUrls: ['./resource-dialog.component.scss']
})
export class ResourceDialogComponent {
  serverSystemLoads: ServerSystemLoads;

  constructor(
    public dialogRef: MatDialogRef<ResourceDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: ServerSystemLoads
  ) {
    this.serverSystemLoads = data;
  }

  public close(): void {
    this.dialogRef.close();
  }
}
