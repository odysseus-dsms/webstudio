import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatLegacyPaginator as MatPaginator } from '@angular/material/legacy-paginator';
import { MatSort } from '@angular/material/sort';
import { MatLegacyTableDataSource as MatTableDataSource } from '@angular/material/legacy-table';
import { MatLegacyTabGroup as MatTabGroup } from '@angular/material/legacy-tabs';
import { Node } from '@antv/x6';
import { Store } from '@ngxs/store';

import { QueryGraphComponent } from 'app/shared/components/query-graph/query-graph.component';
import { Connection } from 'app/shared/domain/connection.domain';
import { ServerQueries } from 'app/shared/domain/server-queries.domain';
import { ServerSystemLoads } from 'app/shared/domain/server-system-loads.domain';
import { QueryDetailOperator } from 'app/shared/dto/query/query-detail-operator.model';
import { QueryDetails } from 'app/shared/dto/query/query-details.model';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { QueryService } from 'app/shared/services/api/odysseus-core/query.service';
import { ServicesService } from 'app/shared/services/api/odysseus-core/services.service';
import { AuthState } from 'app/shared/states/auth.state';
import { ServerState } from 'app/shared/states/server.state';
import { forkJoin, Subject, switchMap, takeUntil, timer } from 'rxjs';
import { LogDialogComponent } from './log-dialog/log-dialog.component';
import { ResourceDialogComponent } from './resource-dialog/resource-dialog.component';
import { WebSocketService } from 'app/shared/services/api/odysseus-core/websocket.service';

@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.scss']
})
export class MonitoringComponent implements OnInit, OnDestroy {
  @ViewChild(QueryGraphComponent) queryGraphComponent?: QueryGraphComponent;
  @ViewChild('infoTabGroup', { static: false }) infoTabGroup: MatTabGroup;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  connections: Connection[] = [];
  serverQueries: ServerQueries[] = [];
  serverSystemLoads: ServerSystemLoads[] = [];

  selectedGraphOp: QueryDetailOperator|null;
  selectedGraphServer: ServerDto|null;
  paramDataSource = new MatTableDataSource<any>();
  required: number = 0;
  loaded: number = -1;

  selectedGraphOpChanged$ = new Subject<void>();
  destroyed$ = new Subject<void>();

  resultDisplayedColumns: string[];
  resultDataSource = new MatTableDataSource<any>([]);
  maxResults = '5';
  results = [];
  paused = false;

  constructor(
    private _dialog: MatDialog,
    private _store: Store,
    private _queryService: QueryService,
    private _servicesService: ServicesService,
    private _webSocketService: WebSocketService
  ) {}

  ngOnInit(): void {
    this.connections = this._store.selectSnapshot(ServerState.getActiveConnections(
      this._store.selectSnapshot(AuthState.username)
    ));

    this.updateServerQueries();
    this.initSystemLoadFetching();
  }

  protected updateServerQueries(): void {
    this.connections.forEach((connection) => {
      this._queryService
        .getAll(connection.serverDto)
        .subscribe((response) => {
          this.required += response.length;
          const server: ServerQueries = {
            serverDto: connection.serverDto,
            allChecked: false,
            odysseusId: null,
            odysseusNodeName: null,
            queries: []
          }
          this.serverQueries.push(server);
          this.serverQueries.sort((a, b) => a.serverDto.priority - b.serverDto.priority);

          this._servicesService.odysseusId(connection.serverDto).subscribe(res => {
            server.odysseusId = res.id

            this._servicesService.nodeManager(connection.serverDto).subscribe((res) => {
              res.nodemap.forEach(node => {
                const serverNode = this.serverQueries.find(s => s.odysseusId === node.id);
                if (serverNode && !serverNode.odysseusNodeName) {
                  serverNode.odysseusNodeName = node.name;
                }
              })
            });
          });

          response.forEach(query => {
            server.allChecked = true;
            this._queryService.get(connection.serverDto, query.id).subscribe((res) => {
              server.queries?.push({ query: res, checked: true });
              server.queries.sort((a, b) => a.query.id - b.query.id);
              this.loaded += this.loaded === -1 ? 2 : 1;
            });
          });
        });
    });
  }

  protected initSystemLoadFetching(): void {
    this.connections.forEach((connection) => {
      timer(1, 1000).pipe(
        switchMap(() => {
          return forkJoin([
            this._servicesService.systemloadCpu(connection.serverDto),
            this._servicesService.systemloadMem(connection.serverDto),
            this._servicesService.systemloadNet(connection.serverDto)
          ])
        }),
        takeUntil(this.destroyed$)
      ).subscribe(([cpu, mem, net]) => {
        const entry = this.serverSystemLoads.find((entry => entry.serverDto === connection.serverDto));

        if (entry) {
          entry.cpu = cpu;
          entry.cpuCurrentFreqSum = cpu.currentFreq.reduce((pv, cv) => pv + cv, 0) / cpu.currentFreq.length;

          entry.mem = mem;
          entry.net = net;
        } else {
          const cpuCurrentFreqSum = cpu.currentFreq.reduce((pv, cv) => pv + cv, 0) / cpu.currentFreq.length;
          this.serverSystemLoads.push({
            serverDto: connection.serverDto,
            cpu,
            mem,
            net,
            netArray: Array(net.length).fill(0).map((x, i) => i),
            cpuCurrentFreqSum
          });
          this.serverSystemLoads.sort((a, b) => a.serverDto.priority - b.serverDto.priority);
        }
      });
    });
  }

  updateAllChecked(serverQueries: ServerQueries, query: any, checked: boolean): void {
    query.checked = checked;
    serverQueries.allChecked = serverQueries.queries.every(q => q.checked);

    this.queryGraphComponent?.toggleNodeVisibility(query.query, checked, this.serverQueries);
    this.updateDistributedQueries(query.query, checked, 'visibility', serverQueries);
    checked ? this.onQueryMouseEnter(query.query) : this.onQueryMouseLeave(query.query);
  }

  someChecked(serverQueries: ServerQueries): boolean {
    if (serverQueries.queries?.length < 1) {
      return false;
    }
    return serverQueries.queries?.filter(q => q.checked).length > 0 && !serverQueries.allChecked;
  }

  setAll(checked: boolean, serverQueries: ServerQueries): void {
    serverQueries.allChecked = checked;
    if (serverQueries.queries?.length < 1) {
      return;
    }

    serverQueries.queries.forEach(qry => {
      qry.checked = checked;
      this.queryGraphComponent?.toggleNodeVisibility(qry.query, checked, this.serverQueries);
      this.updateDistributedQueries(qry.query, checked, 'visibility', serverQueries);
    });
    checked ? this.onServerMouseEnter(serverQueries) : this.onServerMouseLeave(serverQueries);
  }

  updateDistributedQueries(
    query: QueryDetails,
    bool: boolean,
    fnType: 'visibility'|'highlighting',
    serverQueries?: ServerQueries,
  ): void {
    if (query.queryParameters.hasOwnProperty('globalQueryID')
      && !query.queryOperators.find(o => o.operatorDisplayName.startsWith('RCV_'))) {

      this.serverQueries.forEach(sq => {
        if (sq === serverQueries) {
          return;
        }
        sq.queries.forEach(q => {
          if (q.query.queryParameters['globalQueryID'] === query.queryParameters['globalQueryID']) {
            if (fnType === 'visibility') {
              q.checked = bool
              this.queryGraphComponent?.toggleNodeVisibility(q.query, bool, this.serverQueries);
              sq.allChecked = sq.queries?.filter(q => q.checked).length === sq.queries.length;
            } else if (fnType === 'highlighting') {
              this.queryGraphComponent?.toggleNodeHighlighting(q.query, bool);
            }
          }
        });
      });
    }
  }

  onGraphNodeSelected(graphNode: Node|null): void {
    this.selectedGraphOp = graphNode?.data?.operator;
    this.selectedGraphServer = graphNode?.data?.serverDto;
    this.selectedGraphOpChanged$.next();
    this.results = [];
    this.resultDataSource.data = [];
    this.resultDisplayedColumns = [];
    this.paused = false;

    if (this.selectedGraphOp) {
      if (this.selectedGraphOp.ports) {
        this.resultDisplayedColumns = this.selectedGraphOp.ports[0].schema.attributes.map(
          (x) => x.attributename
        );
        this.resultDisplayedColumns.push('metadata');

        this.createResultConnection();
      }

      this.paramDataSource.paginator = this.paginator;
      this.paramDataSource.sort = this.sort;
      this.paramDataSource.data = Object.entries(this.selectedGraphOp.parameterInfos);
    } else {
      this.infoTabGroup.selectedIndex = 0;
    }
  }

  createResultConnection(): void {
    this._webSocketService
      .connectToOperatorResult(this.selectedGraphServer, this.selectedGraphOp)
      .pipe(takeUntil(this.selectedGraphOpChanged$))
      .subscribe(res => {
        const lastIndex = res.lastIndexOf(';');
        const data = res.substr(0, lastIndex);
        const metaData = res.substr(lastIndex + 1, res.length);

        const jsonData = JSON.parse(data);
        jsonData.metadata = metaData;

        if (this.results.length > +this.maxResults - 1) {
          this.results.length = +this.maxResults - 1;
        }

        this.results.unshift(jsonData);
        this.resultDataSource.data = this.results;
      });
  }

  onPauseConnectionClicked(): void {
    this.pauseConnection();
  }

  pauseConnection(): void {
    this.paused ? this.createResultConnection() : this.selectedGraphOpChanged$.next();
    this.paused = !this.paused;
  }

  onGraphNodeHover(event: { node: Node, hover: boolean }) {
    const hoveredGraphOp = event.node.data.operator;

    // if subquery hovered highlight query in plan
    if (hoveredGraphOp.operatorType === 'SubQuery'
      && hoveredGraphOp.parameterInfos.hasOwnProperty('QUERYID')) {
      const query = event.node.data.query;

      this.serverQueries.forEach(sq => {
        if (sq.queries.find(q => q.query.id === query.id && q.query.queryText === query.queryText)) {
          const subQuery = sq.queries.find(q => q.query.id == hoveredGraphOp.parameterInfos['QUERYID']);
          if (subQuery) {
            event.hover ? this.onQueryMouseEnter(subQuery.query) : this.onQueryMouseLeave(subQuery.query);
          }
        }
      });
    }
  }

  onQueryMouseEnter(query: QueryDetails): void {
    this.queryGraphComponent?.toggleNodeHighlighting(query, true);
    this.updateDistributedQueries(query, true, 'highlighting');
  }

  onQueryMouseLeave(query: QueryDetails): void {
    this.queryGraphComponent?.toggleNodeHighlighting(query, false);
    this.updateDistributedQueries(query, false, 'highlighting');
  }

  onServerMouseEnter(server: ServerQueries): void {
    server.queries.forEach(q => {
      if (q.checked) {
        this.queryGraphComponent?.toggleNodeHighlighting(q.query, true);
      }
    });
  }

  onServerMouseLeave(server: ServerQueries): void {
    server.queries.forEach(q => {
      if (q.checked) {
        this.queryGraphComponent?.toggleNodeHighlighting(q.query, false);
      }
    });
  }

  openResourceDialog(serverSystemLoads: ServerSystemLoads): void {
    this._dialog
      .open<ResourceDialogComponent, any>(
        ResourceDialogComponent,
        {
          data: serverSystemLoads,
          autoFocus: false
        }
      );
  }

  openLogDialog(serverDto: ServerDto): void {
    this._dialog
      .open<LogDialogComponent, any>(
        LogDialogComponent,
        {
          data: serverDto,
          autoFocus: false
        }
      );
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.selectedGraphOpChanged$.next();
  }
}
