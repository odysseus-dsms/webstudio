import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Chart, ChartData, ChartDataset, ChartOptions, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-monitoring-network-chart',
  templateUrl: './network-chart.component.html',
  styleUrls: ['./network-chart.component.scss']
})
export class NetworkChartComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() bytesSent: number;
  @Input() bytesRecv: number;
  @Input() chartTitle: string;

  @ViewChild(BaseChartDirective) chart?: BaseChartDirective;

  lastBytesSent: number;
  lastBytesRecv: number;
  initialized: boolean = false;

  chartType: ChartType = 'line';
  chartData: ChartData<'line'> = {
    datasets: [
      {
        label: 'Sent',
        data: [],
        backgroundColor: '#002060',
        borderColor: '#002060',
        pointBackgroundColor: '#002060',
        pointBorderColor: '#002060'
      },
      {
        label: 'Received',
        data: [],
        backgroundColor: '#7da9d0',
        borderColor: '#7da9d0',
        pointBackgroundColor: '#7da9d0',
        pointBorderColor: '#7da9d0'
      },
    ]
  };
  chartOptions: ChartOptions;

  constructor() {}

  ngOnInit(): void {
    this.chartOptions = {
      maintainAspectRatio: false,
      plugins: {
        title: {
          display: true,
          position: 'top',
          text: this.chartTitle,
          padding: 5,
        },
        legend: {
          position: 'bottom',
          maxWidth: 10
        }
      },
      scales: {
        x: {
          type: 'realtime',
          reverse: true,
          realtime: {
            delay: 1500,
            duration: 15000
          }
        },
        y: {
          title: {
            display: true,
            text: 'KiB/s'
          }
        },
      },
      interaction: {
        intersect: false
      },
    };
  }

  ngAfterViewInit(): void {
    this.initialized = true;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.initialized) {
      this.updateChart();
      this.lastBytesSent = this.bytesSent;
      this.lastBytesRecv = this.bytesRecv;
    }
  }

  updateChart() {
    this.chart.data.datasets.forEach((dataset: ChartDataset) => {
      if (dataset.label === 'Received') {
        dataset.data.push({
          x: Date.now(),
          y: (this.bytesRecv - this.lastBytesRecv) / 1024
        });
      } else {
        dataset.data.push({
          x: Date.now(),
          y: (this.bytesSent - this.lastBytesSent) / 1024
        });
      }
    });
    //this.chart.update();
  }
}
