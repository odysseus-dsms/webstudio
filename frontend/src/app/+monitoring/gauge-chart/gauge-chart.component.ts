import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { ChartData, ChartOptions, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-monitoring-gauge-chart',
  templateUrl: './gauge-chart.component.html',
  styleUrls: ['./gauge-chart.component.scss']
})
export class GaugeChartComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() currentValue: number;
  @Input() maxValue: number;
  @Input() type: string;

  @ViewChild(BaseChartDirective) chart?: BaseChartDirective;

  initialized: boolean = false;
  percent: number = 0;

  chartType: ChartType = 'doughnut';
  chartData: ChartData<'doughnut'> = {
    datasets: [
      {
        backgroundColor: [
          '#7da9d0',
          '#ccc',
        ],
        data: [
          0,
          100,
        ],
        rotation: 270,
        circumference: 180,
      },
    ]
  };
  chartOptions: ChartOptions<'doughnut'>;

  constructor() { }

  ngOnInit(): void {
    this.chartOptions = {
      cutout: 14,
      maintainAspectRatio: false,
      plugins: {
        subtitle: {
          display: true,
          position: 'bottom',
          text: this.type
        },
        tooltip: {
          enabled: false
        }
      }
    }
  }

  ngAfterViewInit(): void {
    this.initialized = true;
    this.updateChart();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.initialized) {
      this.updateChart();
    }
  }

  updateChart(): void {
    this.percent = Math.ceil(this.currentValue / this.maxValue * 100);
    this.chart.data.datasets.forEach((dataset) => {
      dataset.data = [
        this.percent,
        this.percent < 100 ? 100 - this.percent : 0,
      ];

      if (this.percent < 75) {
        dataset.backgroundColor = [
          '#7da9d0',
          '#ccc',
        ]
      } else {
        dataset.backgroundColor = [
          '#f44336',
          '#ccc',
        ]
      }
    });
    this.chart.update();
  }
}
