import { Component, ViewChild } from '@angular/core';
import { Observable, switchMap } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';

import { DialogService } from 'app/shared/dialogs/dialog.service';
import { MessageService } from 'app/shared/services/message.service';
import { AuthLogout, AuthState } from 'app/shared/states/auth.state';
import { ServerState } from 'app/shared/states/server.state';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent {
  @Select(AuthState.username) username$: Observable<string>;
  @Select(AuthState.isAuthenticated) isAuthenticated$: Observable<boolean>;
  @Select(AuthState.isAdmin) isAdmin$: Observable<boolean>;

  @ViewChild('sidenav') sideNav: MatSidenav;

  isLoggedIn$: Observable<boolean>;
  defaultServer: string;
  isExpanded = false;

  constructor(
    private dialogService: DialogService,
    private messageService: MessageService,
    private store: Store,
    private router: Router
  ) {
    this.store.select(AuthState.username).subscribe(owner => {
      this.isLoggedIn$ = this.store.select(ServerState.someActiveConnection(owner));

      this.store.select(ServerState.getActiveConnections(owner)).subscribe(connections => {
        this.defaultServer = connections.length > 0 ? connections[0].serverDto.name : null;
      });
    });
  }

  public logout(): void {
    this.dialogService
      .confirm('Logout', 'Do you want to continue?')
      .subscribe((res) => {
        if (res) {
          this.store.dispatch(new AuthLogout());
          this.messageService.showMessage('Your logout was successful.');
          this.sideNav.close();
          this.router.navigate(['login']);
        }
      });
  }
}
