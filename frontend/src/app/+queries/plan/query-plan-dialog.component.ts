import { Component, Inject } from '@angular/core';
import { MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { ServerDto } from 'app/shared/dto/server/server.dto';

@Component({
  selector: 'query-plan-dialog',
  templateUrl: './query-plan-dialog.component.html',
  styleUrls: ['./query-plan-dialog.component.scss']
})
export class QueryPlanDialogComponent {
  serverDto: ServerDto;
  query: QueryPreview;

  constructor(
    private dialogRef: MatDialogRef<QueryPlanDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) {
    this.serverDto = this.data.serverDto;
    this.query = this.data.query;
  }

  public onCloseClicked(): void {
    this.dialogRef.close();
  }
}
