import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatLegacyTableDataSource as MatTableDataSource } from '@angular/material/legacy-table';
import { MatLegacyPaginator as MatPaginator } from '@angular/material/legacy-paginator';
import { MatSort } from '@angular/material/sort';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { QueryService } from '../../shared/services/api/odysseus-core/query.service';
import { WebSocketService } from '../../shared/services/api/odysseus-core/websocket.service';
import { Store } from '@ngxs/store';
import { ServerState } from 'app/shared/states/server.state';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-query-dashboard',
  templateUrl: './query-dashboard.component.html',
  styleUrls: ['./query-dashboard.component.scss'],
})
export class QueryDashboardComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  destroyed$ = new Subject<void>();
  dataSource = new MatTableDataSource<any>([]);
  serverDto: ServerDto;
  queryId: number;
  query: QueryPreview;
  displayedColumns: string[];
  paused = false;
  maxResults = '50';
  results = [];

  constructor(
    private _route: ActivatedRoute,
    private _store: Store,
    private _queryService: QueryService,
    private _webSocketService: WebSocketService
  ) {}

  ngOnInit(): void {
    this.queryId = this._route.snapshot.params.id;
    this.serverDto = this._store.selectSnapshot(
      ServerState.getDtoByName(
        this._route.snapshot.params.server,
        this._store.selectSnapshot(AuthState.username)
      )
    );

    this.createResultConnection();
  }

  createResultConnection(): void {
    this._queryService
      .get(this.serverDto, this.queryId)
      .subscribe((query: QueryPreview) => {
        this.query = query;

        this._webSocketService
          .connectToQueryResult(this.serverDto, this.query)
          .pipe(takeUntil(this.destroyed$))
          .subscribe((res) => {
            const lastIndex = res.lastIndexOf(';');
            const data = res.substr(0, lastIndex);
            const metaData = res.substr(lastIndex + 1, res.length);

            const jsonData = JSON.parse(data);
            jsonData.metadata = metaData;

            if (this.results.length === 0) {
              this.displayedColumns = Object.keys(jsonData);
            }
            if (this.results.length > +this.maxResults - 1) {
              this.results.length = +this.maxResults - 1;
            }

            this.results.unshift(jsonData);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.dataSource.data = this.results;
          });
      });
  }

  updateQuery(): void {
    this._queryService
      .get(this.serverDto, this.queryId)
      .subscribe((query: QueryPreview) => {
        this.query = query;
      });
  }

  pauseConnection(): void {
    this.paused ? this.createResultConnection() : this.destroyed$.next();
    this.paused = !this.paused;
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
