import { Component, OnInit } from '@angular/core';

import { DialogService } from 'app/shared/dialogs/dialog.service';
import { MessageService } from 'app/shared/services/message.service';
import { QueryService } from '../shared/services/api/odysseus-core/query.service';
import { Store } from '@ngxs/store';
import { ServerState } from 'app/shared/states/server.state';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { QueryPlanDialogComponent } from './plan/query-plan-dialog.component';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.scss'],
})
export class QueryComponent implements OnInit {
  serverQueries: { serverDto: ServerDto; queries: QueryPreview[] }[];

  constructor(
    private _store: Store,
    private _dialog: MatDialog,
    private _dialogService: DialogService,
    private _messageService: MessageService,
    private _queryService: QueryService
  ) {}

  ngOnInit(): void {
    this.updateQueries();
  }

  protected updateQueries(): void {
    this.serverQueries = [];

    const connections = this._store.selectSnapshot(ServerState.getActiveConnections(
      this._store.selectSnapshot(AuthState.username)
    ));
    connections.forEach((x) => {
      this._queryService
        .getAll(x.serverDto)
        .subscribe((response) => {
          this.serverQueries.push({ serverDto: x.serverDto, queries: response });
          this.serverQueries.sort((a, b) => a.serverDto.priority - b.serverDto.priority);
        });
    });
  }

  changeState(event: { serverDto: ServerDto; query: QueryPreview }): void {
    this._queryService
      .changeState(
        event.serverDto,
        event.query,
        event.query.state !== 'RUNNING' ? 'RUNNING' : 'INACTIVE'
      )
      .subscribe((res) => {
        if (res) {
          this._messageService.showMessage(
            'The state was changed successfully.'
          );
        }
      });
  }

  delete(event: { serverDto: ServerDto; query: QueryPreview }): void {
    this._dialogService
      .confirm('Delete ' + event.query.name, 'Do you want to continue?')
      .subscribe((res) => {
        if (res) {
          this._queryService
            .delete(event.serverDto, event.query)
            .subscribe(() => {
              this._messageService.showMessage(
                'The query was deleted successfully.'
              );
            });
        }
      });
  }

  onShowPlanClicked(event: {
    serverDto: ServerDto;
    query: QueryPreview;
  }): void {
    this._dialog
      .open(QueryPlanDialogComponent, {
        data: event,
        height: '400px',
        width: '800px'
      })
      .afterClosed();
  }
}
