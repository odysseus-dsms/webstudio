import {
  Component,
  OnInit,
  AfterViewInit,
  Input,
  Output,
  ViewChild,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatLegacyPaginator as MatPaginator } from '@angular/material/legacy-paginator';
import { MatSort } from '@angular/material/sort';
import { MatLegacyTableDataSource as MatTableDataSource } from '@angular/material/legacy-table';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { WebSocketService } from '../../shared/services/api/odysseus-core/websocket.service';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { ServicesService } from 'app/shared/services/api/odysseus-core/services.service';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';

@Component({
  selector: 'app-query-table',
  templateUrl: 'query-table.component.html',
  styleUrls: ['query-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*', marginBottom: '5px' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class QueryTableComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Output() queryStateChanged = new EventEmitter<{
    serverDto: ServerDto;
    query: QueryPreview;
  }>();
  @Output() queryDeleted = new EventEmitter<{
    serverDto: ServerDto;
    query: QueryPreview;
  }>();
  @Output() showPlan = new EventEmitter<{
    serverDto: ServerDto;
    query: QueryPreview;
  }>();
  @Output() queryEventReceived = new EventEmitter();

  @Input() serverDto: ServerDto;
  @Input() data: QueryPreview[];

  displayedColumns: string[] = ['name', 'parser', 'state', 'id', 'actions'];
  expandedElement: QueryPreview | null;
  dataSource = new MatTableDataSource<QueryPreview>();
  destroyed$ = new Subject<void>();

  constructor(
    private _servicesService: ServicesService,
    private _webSocketService: WebSocketService
  ) {}

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.data);
    this.createEventConnection();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  createEventConnection(): void {
    this._servicesService
      .events(this.serverDto)
      .pipe(takeUntil(this.destroyed$))
      .subscribe((event) => {
        this._webSocketService
          .connectToQueryEvent(this.serverDto, event)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
            this.queryEventReceived.emit();
          });
      });
  }

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  changeState(query: QueryPreview): void {
    this.queryStateChanged.emit({ serverDto: this.serverDto, query });
  }

  delete(query: QueryPreview): void {
    this.queryDeleted.emit({ serverDto: this.serverDto, query });
  }

  plan(query: QueryPreview) {
    this.showPlan.emit({ serverDto: this.serverDto, query });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
