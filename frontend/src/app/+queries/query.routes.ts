import { AuthGuard } from 'app/util/guards/auth.guard';
import { ServerGuard } from 'app/util/guards/odysseus.guard';
import { QueryDashboardComponent } from './dashboard/query-dashboard.component';
import { QueryComponent } from './query.component';

const ROUTE_PREFIX = 'queries';
export const ROUTES = [
  {
    path: `${ROUTE_PREFIX}`,
    component: QueryComponent,
    canActivate: [AuthGuard, ServerGuard],
  },
  {
    path: `${ROUTE_PREFIX}/:server/:id`,
    component: QueryDashboardComponent,
    canActivate: [AuthGuard, ServerGuard],
  },
];
