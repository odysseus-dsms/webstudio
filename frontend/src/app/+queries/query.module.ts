import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import { QueryDashboardComponent } from './dashboard/query-dashboard.component';
import { QueryPlanDialogComponent } from './plan/query-plan-dialog.component';
import { QueryComponent } from './query.component';
import { ROUTES } from './query.routes';
import { QueryTableComponent } from './table/query-table.component';

@NgModule({
  declarations: [
    QueryComponent,
    QueryTableComponent,
    QueryDashboardComponent,
    QueryPlanDialogComponent,
  ],
  imports: [CommonModule, RouterModule.forChild(ROUTES), SharedModule],
})
export class QueryModule {}
