import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { MessageService } from 'app/shared/services/message.service';
import { AuthState, AuthLogin } from '../shared/states/auth.state';
import { AuthService } from '../shared/services/api/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {
  @Select(AuthState.loginError) loginError$: Observable<string>;
  form: UntypedFormGroup;

  constructor(
    private _router: Router,
    private _store: Store,
    private _formBuilder: UntypedFormBuilder,
    private _messageService: MessageService,
    private _authService: AuthService
  ) {}

  ngOnInit() {
    const tokenResponse = this._authService.restoreTokenResponseIfValid();
    if (tokenResponse) {
      this._router.navigate(['/']);
    } else {
      this.form = this._formBuilder.group({
        username: [this._authService.restoreUsername(), Validators.required],
        password: ['', Validators.required],
        rememberMe: [true],
      });
    }
  }

  public login(): void {
    if (this.form.valid) {
      this._store
        .dispatch(new AuthLogin({ ...this.form.value }))
        .subscribe((res) => {
          if (res.auth.loginError === null) {
            this._messageService.showMessage('Your login was successful.');
            this._router.navigate(['/servers']);
            this._authService.renewInvalidLoginSessions();
          }
        });
    }
  }

  getYear(): number {
    return new Date().getFullYear();
  }
}
