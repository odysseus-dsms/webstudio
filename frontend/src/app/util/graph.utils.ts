export const defineNodeWidth = (labelLength: number): number => {
  if (labelLength <= 8) {
    return 100;
  } else if (labelLength > 8 && labelLength < 14) {
    return 120;
  } else if (labelLength >= 14 && labelLength < 18) {
    return 150;
  } else if (labelLength >= 18 && labelLength < 22) {
    return 180;
  } else if (labelLength >= 22 && labelLength < 26) {
    return 210;
  } else if (labelLength >= 26 && labelLength < 30) {
    return 240;
  } else {
    return 250;
  }
}

export const rectConfig = {
  width: 80,
  height: 40,
  children: [
    {
      tagName: 'rect',
      selector: 'body',
    },
    {
      tagName: 'text',
      selector: 'label',
    },
  ],
  attrs: {
    body: {
      refWidth: '100%',
      refHeight: '100%',
      strokeWidth: 2,
      fill: '#EFF4FF',
      stroke: '#7da9d0',
    },
    label: {
      textWrap: {
        ellipsis: true,
        width: -10,
      },
      textAnchor: 'middle',
      textVerticalAnchor: 'middle',
      refX: '50%',
      refY: '50%',
      fontSize: 14,
    },
  },
  ports: {
    groups: {
      in: {
        position: {
          name: 'left',
        },
        attrs: {
          portBody: {
            r: 5,
            stroke: '#FE854F',
            strokeWidth: 1.5,
            fill: '#fff',
            magnet: 'passive'
          },
        },
      },
      out: {
        position: {
          name: 'right',
        },
        attrs: {
          portBody: {
            r: 5,
            stroke: '#FE854F',
            strokeWidth: 1.5,
            fill: '#fff',
            magnet: true
          },
        },
      },
    },
  },
  portMarkup: [
    {
      tagName: 'circle',
      selector: 'portBody',
    },
  ],
}

export const edgeConfig = {
  zIndex: 1,
  attrs: {
    line: {
      stroke: '#002060',
      strokeWidth: 1.5,
    }
  }
}
