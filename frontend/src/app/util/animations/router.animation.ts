import {
  trigger,
  transition,
  animate,
  style,
  query,
} from '@angular/animations';

export const routerAnimation = trigger('routerAnimation', [
  transition('AuthComponent => NavigationComponent', [
    style({ position: 'relative' }),
    query(
      ':leave',
      [
        style({
          position: 'absolute',
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
          zIndex: 1000,
        }),
        animate('200ms ease-in', style({ transform: 'translateY(-1000px)' })),
      ],
      { optional: true }
    ),
  ]),
  transition('NavigationComponent => AuthComponent', [
    query(
      ':enter',
      [
        style({
          position: 'absolute',
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
          zIndex: 1000,
          transform: 'translateY(-1000px)',
        }),
        animate('200ms ease-out', style({ transform: 'translateY(0px)' })),
      ],
      { optional: true }
    ),
  ]),
]);
