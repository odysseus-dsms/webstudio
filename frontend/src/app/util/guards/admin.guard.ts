import { Injectable } from '@angular/core';
import { CanLoad } from '@angular/router';
import { Store } from '@ngxs/store';

import { AuthState } from 'app/shared/states/auth.state';

@Injectable()
export class AdminGuard implements CanLoad {
  constructor(private store: Store) {}

  public canLoad(): boolean {
    return this.store.selectSnapshot(AuthState.isAdmin);
  }
}
