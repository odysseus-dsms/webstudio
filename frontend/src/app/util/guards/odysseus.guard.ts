import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngxs/store';
import { AuthState } from 'app/shared/states/auth.state';
import { ServerState } from 'app/shared/states/server.state';

@Injectable()
export class ServerGuard implements CanActivate {
  constructor(private store: Store) {}

  public canActivate(): boolean {
    return this.store.selectSnapshot(ServerState.someActiveConnection(
      this.store.selectSnapshot(AuthState.username)
    ));
  }
}
