import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { catchError } from 'rxjs/operators';

import { ErrorService } from '../../shared/services/error.service';
import { environment } from 'environments/environment';
import { AuthState } from 'app/shared/states/auth.state';
import { ServerState } from 'app/shared/states/server.state';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private _store: Store, private _errorService: ErrorService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (
      request.url.startsWith(environment.authIssuer) ||
      request.url.startsWith(environment.apiUrl)
    ) {
      request = request.clone({
        headers: request.headers.set(
          'Authorization',
          `Bearer ${this._store.selectSnapshot(AuthState.token)}`
        ),
      });
    } else {
      let token = this._store.selectSnapshot(
        ServerState.getTokenByUrl(
          request.url,
          this._store.selectSnapshot(AuthState.username)
        )
      );

      if (token) {
        request = request.clone({
          headers: request.headers.set('Authorization', `Bearer ${token}`),
        });
      } else {
        // not authenticated
      }
    }

    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        return this._errorService.handleError(error);
      })
    );
  }
}
