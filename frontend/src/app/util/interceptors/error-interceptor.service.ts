import {
  HttpErrorResponse,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpEvent,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { MessageService } from '../../shared/services/message.service';

@Injectable()
export class ErrorInterceptorService implements HttpInterceptor {
  constructor(private _messageService: MessageService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((err) => {
        if (!err) {
          return;
        }

        // Login errors are handled differently
        if (
          !request.url.includes('/auth') &&
          !request.url.includes('/login') &&
          !request.url.includes('/session') &&
          !request.url.includes('/odysseusnodemanager') &&
          !request.url.includes('/systemload')
        ) {
          switch ((err as HttpErrorResponse).status) {
            case 400:
              this._messageService.showMessage(
                'The request was invalid.',
                'warning'
              );
              break;
            case 401:
              this._messageService.showMessage(
                'The token is invalid or missing.',
                'warning'
              );
              break;
            case 403:
              this._messageService.showMessage(
                'You do not have the necessary rights.',
                'warning'
              );
              break;
            case 404:
              this._messageService.showMessage(
                'The resource was not found.',
                'warning'
              );
              break;
            case 409:
              this._messageService.showMessage(
                'The resource already exists.',
                'warning'
              );
              break;
            case 409:
              this._messageService.showMessage(
                'The resource is locked.',
                'warning'
              );
              break;
            case 500:
              this._messageService.showMessage(
                'An internal server error has occurred.',
                'warning'
              );
              break;
            default:
              this._messageService.showMessage(
                'An unknown error has occurred.',
                'warning'
              );
          }
        }

        return throwError(err);
      })
    );
  }
}
