import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthComponent } from 'app/+auth/auth.component';
import { ServerComponent } from './+servers/server.component';
import { StreamComponent } from './+streams/stream.component';
import { LoggingComponent } from './+loggings/logging.component';
import { QueryComponent } from './+queries/query.component';
import { SinkComponent } from './+sinks/sink.component';
import { ListComponent } from './+lists/list.component';
import { NavigationComponent } from './navigation/navigation.component';
import { IdeComponent } from './+ide/ide.component';
import { ServerGuard } from './util/guards/odysseus.guard';
import { AuthGuard } from './util/guards/auth.guard';
import { AdminGuard } from './util/guards/admin.guard';
import { MonitoringComponent } from './+monitoring/monitoring.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'servers',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: AuthComponent,
    data: { animation: 'AuthComponent' }
  },
  {
    path: '',
    component: NavigationComponent,
    data: { animation: 'NavigationComponent' },
    children: [
      {
        path: 'servers',
        children: [
          {
            path: '',
            component: ServerComponent,
            canActivate: [AuthGuard]
          },
          {
            path: ':server',
            children: [
              {
                path: '**',
                component: IdeComponent,
              }
            ],
            canActivate: [AuthGuard, ServerGuard]
          }
        ]
      },
      {
        path: 'monitoring',
        component: MonitoringComponent,
        canActivate: [AuthGuard, ServerGuard]
      },
      {
        path: 'streams',
        component: StreamComponent,
        canActivate: [AuthGuard, ServerGuard]
      },
      {
        path: 'sinks',
        component: SinkComponent,
        canActivate: [AuthGuard, ServerGuard]
      },
      {
        path: 'queries',
        component: QueryComponent,
        canActivate: [AuthGuard, ServerGuard]
      },
      {
        path: 'logs',
        component: LoggingComponent,
        canActivate: [AuthGuard, ServerGuard]
      },
      {
        path: 'lists',
        component: ListComponent,
        canActivate: [AuthGuard, ServerGuard]
      },
      {
        path: 'users',
        loadChildren: async () =>
          (await import('./+users/user.module')).UserModule,
        canLoad: [AdminGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
  providers: [AdminGuard]
})
export class AppRoutingModule {}
