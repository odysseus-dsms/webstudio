import { Component, OnInit } from '@angular/core';

import { Datatype } from 'app/shared/models/datatype.model';
import { Parser } from 'app/shared/models/parser.model';
import { IFunction } from 'app/shared/models/function.model';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { ParserService } from 'app/shared/services/api/odysseus-core/parser.service';
import { Store } from '@ngxs/store';
import { ServerState } from 'app/shared/states/server.state';
import { DatatypeService } from 'app/shared/services/api/odysseus-core/datatype.service';
import { FunctionService } from 'app/shared/services/api/odysseus-core/functions.service';
import { AuthState } from 'app/shared/states/auth.state';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  serverParsers: { serverDto: ServerDto; parsers: Parser[] }[];
  serverDatatypes: { serverDto: ServerDto; datatypes: Datatype[] }[];
  serverFunctions: { serverDto: ServerDto; functions: IFunction[] }[];

  constructor(
    private _store: Store,
    private _parserService: ParserService,
    private _datatypeService: DatatypeService,
    private _functionService: FunctionService
  ) {}

  ngOnInit(): void {
    this.updateLists();
  }

  private updateLists(): void {
    this.serverParsers = [];
    this.serverDatatypes = [];
    this.serverFunctions = [];

    const connections = this._store.selectSnapshot(ServerState.getActiveConnections(
      this._store.selectSnapshot(AuthState.username)
    ));
    connections.forEach((x) => {
      this._parserService
        .getAll(x.serverDto)
        .subscribe((response) => {
          this.serverParsers.push({ serverDto: x.serverDto, parsers: response })
          this.serverParsers.sort((a, b) => a.serverDto.priority - b.serverDto.priority);
        });

      this._datatypeService.getAll(x.serverDto).subscribe((response) => {
        this.serverDatatypes.push({
          serverDto: x.serverDto,
          datatypes: response,
        });
        this.serverDatatypes.sort((a, b) => a.serverDto.priority - b.serverDto.priority);
      });

      this._functionService.getAll(x.serverDto).subscribe((response) => {
        this.serverFunctions.push({
          serverDto: x.serverDto,
          functions: response,
        });
        this.serverFunctions.sort((a, b) => a.serverDto.priority - b.serverDto.priority);
      });
    });
  }
}
