import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ListComponent } from './list.component';
import { ParserTableComponent } from './table/parser-table/parser-table.component';
import { DatatypeTableComponent } from './table/datatype-table/datatype-table.component';
import { FunctionTableComponent } from './table/function-table/function-table.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [
    ListComponent,
    ParserTableComponent,
    DatatypeTableComponent,
    FunctionTableComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: ListComponent }]),
    SharedModule,
  ],
})
export class ListModule {}
