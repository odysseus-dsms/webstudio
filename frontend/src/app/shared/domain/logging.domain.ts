import { ServerDto } from "../dto/server/server.dto";

export interface Logging {
  serverDto: ServerDto;
  logContent: string;
}
