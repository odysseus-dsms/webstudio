import { ServerDto } from "../dto/server/server.dto";
import { SystemloadCpu } from "../models/systemload-cpu.model";
import { SystemloadMemory } from "../models/systemload-memory.model";
import { SystemloadNetwork } from "../models/systemload-network.model";

export interface ServerSystemLoads {
  serverDto: ServerDto;
  cpu: SystemloadCpu;
  mem: SystemloadMemory;
  net: SystemloadNetwork[];
  netArray: number[];
  cpuCurrentFreqSum?: number;
}
