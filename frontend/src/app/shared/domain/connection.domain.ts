import { ServerDto } from '../dto/server/server.dto';

export interface Connection {
  owner: string;
  serverDto: ServerDto;
  baseUrl: string;
  username: string;
  password: string;
  token: string;
  loginError: string;
  online: boolean;
}
