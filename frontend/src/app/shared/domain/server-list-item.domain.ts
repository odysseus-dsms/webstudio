import { ServerDto } from '../dto/server/server.dto';

export class ServerListItem {
  serverDto: ServerDto;
  online?: boolean;
}
