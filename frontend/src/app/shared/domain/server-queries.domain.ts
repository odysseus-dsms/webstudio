import { QueryDetails } from '../dto/query/query-details.model';
import { ServerDto } from '../dto/server/server.dto';

export interface ServerQueries {
  serverDto: ServerDto;
  odysseusId?: string;
  odysseusNodeName?: string;
  queries?: {
    query: QueryDetails;
    checked: boolean;
  }[];
  allChecked: boolean;
}
