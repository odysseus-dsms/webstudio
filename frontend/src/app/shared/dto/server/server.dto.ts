export class ServerDto {
  name: string;
  scheme: string;
  host: string;
  owner: string;
  port?: number;
  description?: string;
  priority?: number;
}
