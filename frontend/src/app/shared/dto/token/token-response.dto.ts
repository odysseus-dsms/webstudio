export interface TokenResponse {
  access_token: string;
  expires_at: number;
  token_type: string;
}
