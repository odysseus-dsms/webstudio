export class NodePreview {
  name: string;
  path: string;
  isDir: boolean;
}
