export class NodeCreateRequest {
  name: string;
  isDir?: boolean;
  copyOf?: string;
}
