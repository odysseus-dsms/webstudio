export class NodeUpdateRequest {
  name?: string;
  path?: string;
  content?: string;
}
