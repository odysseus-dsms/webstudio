import { NodePreview } from './node-preview';

export class NodeDetails extends NodePreview {
  children?: NodePreview[];
  content?: string;
}
