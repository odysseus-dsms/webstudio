export class ParserTokenDto {
  DEPRECATED: string[];
  STATIC: string[];
  KEYWORDS: string[];
}
