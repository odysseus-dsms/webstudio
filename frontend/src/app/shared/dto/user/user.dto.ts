export class UserDto {
  username: string;
  email: string;
  role: string;
}
