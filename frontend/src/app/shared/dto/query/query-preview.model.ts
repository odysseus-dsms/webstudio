import { Operator } from 'app/shared/models/operator.model';

export interface QueryPreview {
  id?: number;
  name?: string;
  parser?: string;
  queryText?: string;
  state?: string;
  user?: string;
  rootOperators?: Operator[];
}
