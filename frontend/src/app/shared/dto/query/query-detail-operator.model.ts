import { OperatorPort } from "app/shared/models/operator-port.model";

export interface QueryDetailOperator {
  operatorName?: string;
  operatorDisplayName?: string;
  operatorType?: string;
  operatorImplementation?: string;
  inputports?: string[];
  parameterInfos?: [];
  ports?: OperatorPort[];
}
