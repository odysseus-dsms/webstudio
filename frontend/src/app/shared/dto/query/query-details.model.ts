import { Operator } from 'app/shared/models/operator.model';
import { QueryDetailOperator } from './query-detail-operator.model';

export interface QueryDetails {
  id?: number;
  name?: string;
  parser?: string;
  queryText?: string;
  state?: string;
  user?: string;
  rootOperators?: Operator[];
  queryOperators?: QueryDetailOperator[];
  queryParameters: any[];
}
