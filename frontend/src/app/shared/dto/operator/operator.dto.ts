import { OperatorParameter } from "app/shared/models/operator-parameter.model";

export class OperatorDto {
  operatorName: string;
  doc: string;
  url: string;
  parameters: OperatorParameter[];
  maxPorts: number;
  minPorts: number;
  categories: string[];
  hidden: boolean;
  deprecated: boolean;
}
