import { Component } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './info-dialog.component.html'
})
export class InfoDialogComponent {
  title: string;
  message: string;

  constructor(public dialogRef: MatDialogRef<InfoDialogComponent>) {}
}
