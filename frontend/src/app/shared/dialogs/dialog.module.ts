import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InfoDialogComponent } from './info-dialog/info-dialog.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { DialogService } from './dialog.service';
import { SharedModule } from '../shared.module';

const components = [ConfirmDialogComponent, InfoDialogComponent];

@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, SharedModule],
  declarations: [components],
  exports: [components],
  providers: [DialogService],
})
export class DialogModule {}
