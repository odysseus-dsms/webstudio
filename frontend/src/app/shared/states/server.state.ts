import {
  State,
  Action,
  StateContext,
  Selector,
  createSelector,
  Store,
} from '@ngxs/store';
import { patch, append, removeItem, updateItem } from '@ngxs/store/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { ServicesService } from '../services/api/odysseus-core/services.service';
import { Token } from 'app/shared/models/token.model';
import { Connection } from '../domain/connection.domain';
import { ServerDto } from '../dto/server/server.dto';
import { buildServerBaseUrl } from '../services/api/odysseus-core/utils/server-base-url-builder';
import { AuthState } from './auth.state';

export interface ServerStateModel {
  entities: Connection[];
}

export class ServerLogin {
  static readonly type = '[Server] Login';
  constructor(
    public serverDto: ServerDto,
    public credentials: { username: string; password: string }
  ) { }
}

export class ServerError {
  static readonly type = '[Server] Error';
  constructor(
    public err: any,
    public serverDto: ServerDto,
    public username: string
  ) { }
}

export class ServerLogout {
  static readonly type = '[Server] Logout';
  constructor(public serverDto: ServerDto) { }
}

export class ServerStatusUpdate {
  static readonly type = '[Server] Status Update';
  constructor(
    public serverDto: ServerDto,
    public online: boolean
  ) { }
}

export class ServerListUpdate {
  static readonly type = '[Server] List Update';
  constructor(
    public serverDtos: ServerDto[]
  ) { }
}


export class ServerDtoUpdate {
  static readonly type = '[Server] Dto Update';
  constructor(
    public serverDto: ServerDto,
    public name: string
  ) { }
}

@State<ServerStateModel>({
  name: 'servers',
  defaults: {
    entities: [],
  },
})
@Injectable()
export class ServerState {

  static getActiveConnections(owner: string) {
    return createSelector(
      [ServerState],
      (state: ServerStateModel) => {
        return state.entities.filter((x) => x.token !== null
          && x.online
          && x.owner === owner
        ).sort((a, b) => a.serverDto.priority - b.serverDto.priority);
      });
  }

  static someActiveConnection(owner: string) {
    return createSelector(
      [ServerState],
      (state: ServerStateModel) => {
        return state.entities.some((x) => x.token !== null
          && x.online
          && x.owner === owner
        );
      });
  }

  static getTokenByName(serverName: string, owner: string): any {
    return createSelector(
      [ServerState],
      (state: ServerStateModel): string | null =>
        state.entities.find((x) => x.serverDto.name === serverName && x.owner === owner)?.token
    );
  }

  static getTokenByUrl(url: string, owner: string): any {
    return createSelector(
      [ServerState],
      (state: ServerStateModel): string | null =>
        state.entities.find((x) => url.startsWith(x.baseUrl) && x.owner === owner)?.token
    );
  }

  static getDtoByName(serverName: string, owner: string): any {
    return createSelector(
      [ServerState],
      (state: ServerStateModel): ServerDto | null =>
        state.entities.find((x) => x.serverDto.name === serverName && x.owner === owner)?.serverDto
    );
  }

  static getStatusByName(serverName: string, owner: string): any {
    return createSelector(
      [ServerState],
      (state: ServerStateModel): boolean | null =>
        state.entities.find((x) => x.serverDto.name === serverName && x.owner === owner)?.online
    );
  }

  static loginError(serverName: string, owner: string): any {
    return createSelector(
      [ServerState],
      (state: ServerStateModel): string | null =>
        state.entities.find((x) => x.serverDto.name === serverName && x.owner === owner)?.loginError
    );
  }

  constructor(
    private _loginService: ServicesService,
    private store: Store
  ) { }

  @Action(ServerLogin)
  addLogin(
    ctx: StateContext<ServerStateModel>,
    action: ServerLogin
  ): Observable<any> {
    return this._loginService.login(action.serverDto, action.credentials).pipe(
      tap({
        next: (result: Token) => {
          const user = this.store.selectSnapshot(AuthState.username);
          const state = ctx.getState();
          const connection = state.entities.find(
            (x) => x.serverDto?.name === action.serverDto.name && x.owner === user
          );
          if (connection) {
            ctx.setState(
              patch({
                entities: updateItem<Connection>(
                  (x) => x.serverDto?.name === action.serverDto.name && x.owner === user,
                  {
                    owner: user,
                    serverDto: action.serverDto,
                    baseUrl: buildServerBaseUrl(action.serverDto),
                    username: action.credentials.username,
                    password: action.credentials.password,
                    token: result.token,
                    loginError: null,
                    online: true
                  }
                ),
              })
            );
          } else {
            ctx.setState(
              patch({
                entities: append<Connection>([
                  {
                    owner: user,
                    serverDto: action.serverDto,
                    baseUrl: buildServerBaseUrl(action.serverDto),
                    username: action.credentials.username,
                    password: action.credentials.password,
                    token: result.token,
                    loginError: null,
                    online: true
                  },
                ]),
              })
            );
          }
        },
        error: (err) => {
          ctx.dispatch(
            new ServerError(err, action.serverDto, action.credentials.username)
          )
        }
      }
      )
    );
  }

  @Action(ServerError)
  loginError(ctx: StateContext<ServerStateModel>, action: ServerError): void {
    let errorMsg = 'An error has occurred.';
    let online = false;
    if (action.err instanceof HttpErrorResponse && action.err.status === 400) {
      errorMsg = 'Invalid login data.';
      online = true
    }

    const user = this.store.selectSnapshot(AuthState.username);
    const state = ctx.getState();
    const login = state.entities.find(
      (x) => x.serverDto.name === action.serverDto.name && x.owner === user
    );
    if (login) {
      ctx.setState(
        patch({
          entities: updateItem<Connection>(
            (x) => x.serverDto.name === action.serverDto.name && x.owner === user,
            {
              owner: user,
              serverDto: action.serverDto,
              baseUrl: buildServerBaseUrl(action.serverDto),
              username: action.username,
              password: null,
              token: null,
              loginError: errorMsg,
              online
            }
          ),
        })
      );
    } else {
      ctx.setState(
        patch({
          entities: append<Connection>([
            {
              owner: user,
              serverDto: action.serverDto,
              baseUrl: buildServerBaseUrl(action.serverDto),
              username: action.username,
              password: null,
              token: null,
              loginError: errorMsg,
              online
            },
          ]),
        })
      );
    }
  }

  @Action(ServerStatusUpdate)
  updateServerStatus(ctx: StateContext<ServerStateModel>, action: ServerStatusUpdate): void {
    ctx.setState(
      patch({
        entities: updateItem<Connection>(
          (x) => x.serverDto.name === action.serverDto.name
            && x.owner === this.store.selectSnapshot(AuthState.username),
          patch({
            online: action.online
          })
        ),
      })
    );
  }

  @Action(ServerListUpdate)
  updateServerList(ctx: StateContext<ServerStateModel>, action: ServerListUpdate): void {
    const state = ctx.getState();
    ctx.patchState({
      entities: state.entities.filter((x) => action.serverDtos.some(existing => existing.name === x.serverDto.name))
    })
  }

  @Action(ServerDtoUpdate)
  updateServerDto(ctx: StateContext<ServerStateModel>, action: ServerDtoUpdate): void {
    ctx.setState(
      patch({
        entities: updateItem<Connection>(
          (x) => x.serverDto.name === action.name
            && x.owner === this.store.selectSnapshot(AuthState.username),
          patch({
            serverDto: action.serverDto
          })
        ),
      })
    );
  }

  @Action(ServerLogout)
  removeLogin(ctx: StateContext<ServerStateModel>, action: ServerLogout): void {
    ctx.setState(
      patch({
        entities: removeItem<Connection>(
          (x) => x.serverDto.name === action.serverDto.name
            && x.owner === this.store.selectSnapshot(AuthState.username)
        ),
      })
    );

    /*
    const state = ctx.getState();
    if (state.entities.length === 0) {
      this.router.navigate(['servers']);
    }
    */
  }
}
