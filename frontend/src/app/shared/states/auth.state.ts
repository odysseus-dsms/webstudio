import { State, Action, StateContext, Selector } from '@ngxs/store';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

import { AuthService } from '../services/api/auth.service';
import { Observable } from 'rxjs';
import { TokenResponse } from 'app/shared/dto/token/token-response.dto';
import { UserDto } from '../dto/user/user.dto';

export interface AuthStateModel {
  username: string;
  email: string;
  role: string;
  token: string;
  expiresAt: number;
  loginError: string;
}

export class AuthLogin {
  static readonly type = '[Auth] Login';
  constructor(
    public payload: { username: string; password: string; rememberMe: boolean }
  ) {}
}

export class AuthRememberMe {
  static readonly type = '[Auth] RememberMe';
  constructor(
    public payload: {
      username: string;
      tokenResponse: TokenResponse;
    }
  ) {}
}

export class AuthUserInfo {
  static readonly type = '[Auth] UserInfo';
  constructor(public rememberMe: boolean) {}
}

export class AuthLoginError {
  static readonly type = '[Auth] LoginError';
  constructor(public payload: any) {}
}

export class AuthLogout {
  static readonly type = '[Auth] Logout';
}

@State<AuthStateModel>({
  name: 'auth',
  defaults: {
    username: null,
    email: null,
    role: null,
    token: null,
    expiresAt: null,
    loginError: null,
  },
})
@Injectable()
export class AuthState {
  @Selector()
  static token(state: AuthStateModel): string {
    return state.token;
  }

  @Selector()
  static username(state: AuthStateModel): string {
    return state.username;
  }

  @Selector()
  static loginError(state: AuthStateModel): string {
    return state.loginError;
  }

  @Selector()
  static isAdmin(state: AuthStateModel): boolean {
    return state.role === 'admin';
  }

  @Selector()
  static isAuthenticated(state: AuthStateModel): boolean {
    return !!state.token;
  }

  constructor(private authService: AuthService) {}

  @Action(AuthLogin)
  login(
    ctx: StateContext<AuthStateModel>,
    action: AuthLogin
  ): Observable<TokenResponse> {
    return this.authService.login(action.payload).pipe(
      tap(
        (result: TokenResponse) => {
          if (action.payload.rememberMe) {
            this.authService.storeTokenResponse(result);
          }

          ctx.patchState({
            username: action.payload.username,
            token: result.access_token,
            expiresAt: result.expires_at,
          });

          ctx.dispatch(new AuthUserInfo(action.payload.rememberMe));
        },
        (err) => ctx.dispatch(new AuthLoginError(err))
      )
    );
  }

  @Action(AuthRememberMe)
  rememberMe(ctx: StateContext<AuthStateModel>, action: AuthRememberMe) {
    ctx.patchState({
      username: action.payload.username,
      token: action.payload.tokenResponse.access_token,
      expiresAt: action.payload.tokenResponse.expires_at,
      loginError: null,
    });

    ctx.dispatch(new AuthUserInfo(true));
  }

  @Action(AuthUserInfo)
  userInfo(
    ctx: StateContext<AuthStateModel>,
    action: AuthUserInfo
  ): Observable<UserDto> {
    return this.authService.userinfo().pipe(
      tap(
        (result: UserDto) => {
          if (action.rememberMe) {
            this.authService.storeUsername(result.username);
          }

          ctx.patchState({
            email: result.email,
            role: result.role,
            loginError: null,
          });
        },
        (err) => ctx.dispatch(new AuthLoginError(err))
      )
    );
  }

  @Action(AuthLoginError)
  loginError(ctx: StateContext<AuthStateModel>, action: AuthLoginError): void {
    let errorMsg = 'An error has occurred.';
    if (
      action.payload instanceof HttpErrorResponse &&
      action.payload.status === 400
    ) {
      errorMsg = 'Invalid login data.';
    }

    ctx.patchState({
      token: null,
      username: null,
      loginError: errorMsg,
      role: null,
    });
  }

  @Action(AuthLogout)
  logout(ctx: StateContext<AuthStateModel>, action: AuthLogout): void {
    this.authService.removeTokenResponse();

    ctx.setState({
      username: null,
      email: null,
      role: null,
      token: null,
      expiresAt: null,
      loginError: null,
    });
  }
}
