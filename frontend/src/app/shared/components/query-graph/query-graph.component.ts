import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ServerDto } from 'app/shared/dto/server/server.dto';

import { QueryService } from 'app/shared/services/api/odysseus-core/query.service';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { QueryDetails } from 'app/shared/dto/query/query-details.model';
import { MatLegacyMenuTrigger as MatMenuTrigger } from '@angular/material/legacy-menu';
import { DagreLayout } from '@antv/layout';
import { ServerQueries } from 'app/shared/domain/server-queries.domain';
import { defineNodeWidth } from 'app/util/graph.utils';
import { QueryDetailOperator } from 'app/shared/dto/query/query-detail-operator.model';

import { Graph, Node } from '@antv/x6';
import { Selection } from '@antv/x6-plugin-selection';

@Component({
  selector: 'app-query-graph',
  templateUrl: './query-graph.component.html',
  styleUrls: ['./query-graph.component.scss'],
})
export class QueryGraphComponent implements OnInit, AfterViewInit {
  @Input() serverDto?: ServerDto;
  @Input() query?: QueryPreview;
  @Input() queries?: QueryPreview[];
  @Input() serverQueries?: ServerQueries[];

  @Output() onGraphNodeSelected = new EventEmitter<Node|null>();
  @Output() onGraphNodeHover = new EventEmitter<{ node: Node, hover: boolean }>();

  @ViewChild('graphContainer') graphContainer: ElementRef;
  @ViewChild(MatMenuTrigger, { static: true }) matMenuTrigger: MatMenuTrigger;

  graph: Graph;
  edges = [];
  nodes = [];

  constructor(
    private _queryService: QueryService
  ) {}

  ngOnInit(): void {
    if (this.query) {
      this.loadQueryDetails(this.query);
    } else if (this.queries) {
      this.loadQueryDetails(...this.queries);
    } else if (this.serverQueries) {
      this.serverQueries.forEach(server => {
        server.queries.forEach(query => this.createNodes(query.query, server.serverDto));
      });
      this.createEdges();
    }
  }

  ngAfterViewInit(): void {
    this.initGraph();
  }

  loadQueryDetails(...querys: QueryPreview[]) {
    querys.forEach((query) => {
      this._queryService
        .get(this.serverDto, query.id)
        .subscribe((response: QueryDetails) => {
          this.createNodes(response, this.serverDto);
          this.createEdges();
        });
    });
  }

  initGraph(): void {
    this.graph = new Graph({
      container: this.graphContainer.nativeElement,
      panning: true,
      connecting: {
        allowBlank: false,
        allowLoop: false,
        allowNode: false,
        allowEdge: false,
        allowPort: false,
        router: {
          name: 'manhattan',
        },
        connector: {
          name: 'rounded',
          args: {
            size: 5
          }
        },
      },
      grid: {
        visible: false,
        size: 1,
        type: 'mesh'
      },
      mousewheel: {
        enabled: true
      }
    });

    this.graph.use(
      new Selection({
        enabled: true,
        multiple: false,
        showNodeSelectionBox: true
      })
    );

    this.loadGraphContent();
    this.initEventListeners();
  }

  loadGraphContent() {
    const dagreLayout = new DagreLayout({
      type: 'dagre',
      rankdir: 'LR',
      align: 'UL',
      ranksep: 35,
      nodesep: 15,
      controlPoints: true,
    });

    const model = dagreLayout.layout({
      nodes: this.nodes,
      edges: this.edges
    });
    this.graph.fromJSON(model);
  }

  initEventListeners() {
    this.graph.on('node:selected', ({ node }) => {
      this.onGraphNodeSelected.emit(node);
    });

    this.graph.on('node:mouseenter', ({ node }) => {
      this.onGraphNodeHover.emit({ node, hover: true });
    });

    this.graph.on('node:mouseleave', ({ node }) => {
      this.onGraphNodeHover.emit({ node, hover: false });
    });

    this.graph.on('blank:click', () => {
      this.onGraphNodeSelected.emit(null);
    });

    this.graph.on('edge:click', () => {
      this.onGraphNodeSelected.emit(null);
    });
  }

  createNodes(query: QueryDetails, serverDto: ServerDto) {
    if (!query.queryOperators) {
      return
    }

    query.queryOperators.forEach(operator => {
      this.nodes.push({
        id: operator.operatorName,
        size: {
          width: defineNodeWidth(operator.operatorDisplayName.length)
        },
        label: operator.operatorDisplayName,
        data: {
          operator,
          query,
          serverDto
        },
      });
    });
  }

  createEdges() {
    const senderRecieverNodes = [];

    this.nodes.forEach(node => {
      const operator: QueryDetailOperator = node.data.operator
      if (operator.inputports) {
        Object.keys(operator.inputports).forEach((key) => {
          if (!this.edges.find(edge => edge.source === operator.inputports[key] && edge.target === operator.operatorName)) {
            this.edges.push(
              {
                source: operator.inputports[key],
                target: operator.operatorName,
                data: node.data
              }
            );
          }
        });
      }

      if (operator.operatorDisplayName.startsWith('SND_') || operator.operatorDisplayName.startsWith('RCV_')) {
        senderRecieverNodes.push(node.data);
      }
    });

    // connect distributed queries
    senderRecieverNodes.forEach(node => {
      if (node.operator.operatorDisplayName.startsWith('SND_')) {
        const reciever = senderRecieverNodes.find(n => n.operator.operatorDisplayName.startsWith('RCV_')
          && n.operator.operatorDisplayName.split('_')[1] === node.operator.operatorDisplayName.split('_')[1]);

        if (reciever) {
          this.edges.push(
            {
              source: node.operator.operatorName,
              target: reciever.operator.operatorName,
              data: node
            }
          );
        }
      }
    })

    if (this.graph && this.graph.getNodes().length !== this.nodes.length) {
      this.loadGraphContent();
    }
  }

  toggleNodeVisibility(query: QueryDetails, visible: boolean, serverQueries?: ServerQueries[]) {
    if (!query.queryOperators) {
      return;
    }

    query.queryOperators.forEach(o => {
      let isSharedWithVisibleQuery = false;
      serverQueries.forEach(sq => {
        if (isSharedWithVisibleQuery) {
          return;
        }

        sq.queries.forEach(q => {
          if (q.query !== query
            && q.checked
            && q.query.queryOperators.find(op => op.operatorName === o.operatorName)) {
            isSharedWithVisibleQuery = true;
            return;
          }
        });
      })

      if (!isSharedWithVisibleQuery) {
        this.graph.getNodes()
          .filter(n => n.data.operator.operatorName === o.operatorName)
          .map(node => node.setVisible(visible));
      }
    });
  }

  toggleNodeHighlighting(query: QueryDetails, highlight: boolean) {
    if (!query.queryOperators) {
      return;
    }

    query.queryOperators.forEach(o => {
      this.graph.getCells()
        .filter(c => c.data.operator.operatorName === o.operatorName)
        .map(cell => {
          const cellView = this.graph.findViewByCell(cell);
          if (cellView) {
            highlight ? cellView.highlight() : cellView.unhighlight();
          }
        });
    });
  }
}
