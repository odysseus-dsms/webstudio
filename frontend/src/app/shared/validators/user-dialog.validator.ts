import { UntypedFormControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function stringValidator(invalidStrings: string[]): ValidatorFn {
  return (control: UntypedFormControl): ValidationErrors | null => {
    return control.value && invalidStrings.some((x) => x === control.value)
      ? { stringError: true }
      : null;
  };
}
