import { Attribute } from './attribute.model';
import { Metaschema } from './metaschema.model';

export interface Schema {
  uri?: string;
  typeClass?: string;
  attributes?: Attribute[];
  metaschema?: Metaschema[];
}
