import { Datatype } from './datatype.model';
import { Schema } from './schema.model';

export class Attribute {
  sourcename?: string;
  attributename?: string;
  datatype?: Datatype;
  subschema?: Schema;
}
