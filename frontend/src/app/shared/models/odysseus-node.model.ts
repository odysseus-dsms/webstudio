export interface OdysseusNode {
  id: string;
  name: string;
  properties: {
    serveraddress: string;
    password: string;
    serverport: number;
    nodegroup: string;
    username: string;
  };
  options?: {};
}
