export interface SystemloadMemory {
  available?: number;
  pageSize?: number;
  physicalMemory?: [];
  total?: number;
  virtualMemory?: {
    swapPagesIn?: number;
    swapPagesOut?: number;
    swapTotal?: number;
    swapUsed?: number;
    virtualInUse?: number;
    virtualMax?: number;
  }
}
