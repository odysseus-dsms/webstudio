import { Datatype } from './datatype.model';

export interface IFunction {
  symbol?: string;
  parameters?: Datatype[];
}
