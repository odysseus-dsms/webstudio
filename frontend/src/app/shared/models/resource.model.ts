import { Schema } from 'app/shared/models/schema.model';

export class Resource {
  name?: string;
  owner?: string;
  schema?: Schema;
  type?: string;
}
