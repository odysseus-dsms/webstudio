import { OperatorPort } from './operator-port.model';

export interface Operator {
  operatorName?: string;
  operatorDisplayName?: string;
  operatorType?: string;
  operatorImplementation?: string;
  ports?: OperatorPort[];
}
