import { Schema } from 'app/shared/models/schema.model';

export interface Datatype {
  uri?: string;
  type?: string;
  subtype?: Datatype;
  subschema?: Schema;
}
