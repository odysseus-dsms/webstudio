export interface EventWebSocket {
  type: string;
  description: string;
  websocketUri: string;
}
