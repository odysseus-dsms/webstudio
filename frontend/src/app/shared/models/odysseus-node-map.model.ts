import { OdysseusNode } from "./odysseus-node.model";

export interface OdysseusNodeMap {
  nodemap: OdysseusNode[];
}
