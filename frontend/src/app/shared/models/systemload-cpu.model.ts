export interface SystemloadCpu {
  contextSwitches?: number;
  currentFreq?: number[];
  interrupts?: number;
  logicalProcessorCount?: number;
  logicalProcessors?: {
    numaNode?: number;
    physicalPackageNumber?: number;
    physicalProcessorNumber?: number;
    processorGroup?: number;
    processorNumber?: number;
  }[];
  maxFreq?: number;
  physicalPackageCount?: number;
  physicalProcessorCount?: number;
  processorCpuLoadTicks?: number[][];
  processorIdentifier?: {
    cpu64bit?: boolean;
    family?: string;
    identifier?: string;
    microarchitecture?: string;
    model?: string;
    name?: string;
    processorID?: string;
    stepping?: string;
    vendor?: string;
    vendorFreq?: number;
  },
  systemCpuLoadTicks?: number[];
}
