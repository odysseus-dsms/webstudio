import { Schema } from 'app/shared/models/schema.model';

export interface OperatorPort {
  port?: number;
  schema?: Schema;
  websockets?: Array<{
    protocol: string,
    uri: string
  }>;
}
