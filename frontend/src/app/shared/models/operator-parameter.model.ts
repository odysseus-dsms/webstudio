export interface OperatorParameter {
  parameterType: string;
  parameterName: string;
  list: boolean;
  doc: string;
  mandatory: boolean;
  possibleValues: string[];
  deprecated: boolean;
  value?: any;
}
