export interface SystemloadNetwork {
  bytesRecv?: number;
  bytesSent?: number;
  collisions?: number;
  connectorPresent?: boolean;
  displayName?: string;
  ifAlias?: string;
  ifOperStatus?: string;
  ifType?: number;
  inDrops?: number;
  inErrors?: number;
  index?: number;
  ipv4addr?: string[];
  ipv6addr?: string[];
  knownVmMacAddr?: boolean;
  macaddr?: string;
  mtu?: number;
  name?: string;
  ndisPhysicalMediumType?: number;
  outErrors?: number;
  packetsRecv?: number;
  packetsSent?: number;
  prefixLengths?: [];
  speed?: number;
  subnetMasks?: number[];
  timeStamp?: number;
}
