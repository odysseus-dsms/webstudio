import { Attribute } from './attribute.model';

export interface Metaschema {
  uri?: string;
  typeClass?: string;
  attributes?: Attribute[];
  metaattributeClass?: string;
}
