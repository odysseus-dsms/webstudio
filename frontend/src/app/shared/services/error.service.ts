import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';

@Injectable()
export class ErrorService {
  constructor(private router: Router) { }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error(`An error occured: ${error.error.message}`);
    } else {
      if (error.status !== 0) {
        console.error(
          `Backend returned code ${error.status}, body was ${error.statusText}`
        );

        if (error.status === 401) {
          this.router.navigate(['login']);
        }
      }
    }
    return throwError(() => new Error(error.message));
  }
}
