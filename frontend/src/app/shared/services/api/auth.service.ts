import { ServerLogout } from './../../states/server.state';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { mergeMap, Observable, of } from 'rxjs';
import { Store } from '@ngxs/store';

import { Token } from 'app/shared/models/token.model';
import { TokenResponse } from 'app/shared/dto/token/token-response.dto';

import * as moment from 'moment';
import { environment } from 'environments/environment';
import { ServerService } from './webstudio/server.service';
import { ServerLogin, ServerState } from 'app/shared/states/server.state';
import { ServicesService } from './odysseus-core/services.service';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { UserDto } from 'app/shared/dto/user/user.dto';
import { AuthState } from 'app/shared/states/auth.state';

@Injectable()
export class AuthService {
  constructor(
    private http: HttpClient,
    private store: Store,
    private _serverService: ServerService,
    private _servicesService: ServicesService
  ) { }

  login(payload): Observable<TokenResponse> {
    return this.http.post<TokenResponse>(
      `${environment.authIssuer}/auth/token`,
      payload
    );
  }

  userinfo(): Observable<UserDto> {
    return this.http.get<UserDto>(`${environment.authIssuer}/auth/userinfo`);
  }

  storeTokenResponse(token: TokenResponse) {
    localStorage.setItem('access_token', token.access_token);
    localStorage.setItem('expires_at', `${token.expires_at}`);
    localStorage.setItem('token_type', token.token_type);
  }

  restoreTokenResponseIfValid(): TokenResponse {
    const access_token = localStorage.getItem('access_token');
    const expires_at = +localStorage.getItem('expires_at');
    const token_type = localStorage.getItem('token_type');

    if (
      access_token &&
      expires_at &&
      token_type &&
      moment().isBefore(moment.unix(expires_at))
    ) {
      return {
        access_token,
        expires_at,
        token_type,
      };
    }

    return null;
  }

  storeUsername(username: string) {
    localStorage.setItem('username', username);
  }

  restoreUsername(): string {
    const username = localStorage.getItem('username');
    return username ? username : '';
  }

  removeTokenResponse() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('token_type');
  }

  renewInvalidLoginSessions(): void {
    const connections = this.store.selectSnapshot(
      ServerState.getActiveConnections(this.store.selectSnapshot(AuthState.username))
    );

    connections.forEach((x) => {

      this._serverService
        .get(x.serverDto.name).pipe(
          mergeMap((serverDto) => this._servicesService.renew(serverDto)),
          catchError((error) => {
            if ((error as HttpErrorResponse).status === 401) {
              console.error("Trying to (re-)login to server "+x.serverDto.name);
              this._servicesService.login(x.serverDto,{username: x.username, password: x.password}).subscribe({
               next: (ignore) => this.store.dispatch(
                  new ServerLogin(x.serverDto, {
                    username: x.username,
                    password: x.password,
                  })
                ),
                error: (error) => this.store.dispatch(new ServerLogout(x.serverDto))
                }
              );

            }else if ((error as HttpErrorResponse).status === 404) {
              console.error("error connecting server "+x.serverDto.name+ " removing from local state");
              this.store.dispatch(new ServerLogout(x.serverDto));
           }
            return of();
          })
        ).subscribe({
          next: (res: Token) => {
            if (x.token !== res.token) {
              this.store.dispatch(
                new ServerLogin(x.serverDto, {
                  username: x.username,
                  password: x.password,
                })
              );
            }
          },
          error: (error) => {
            console.log("Errror "+error);
            if (error.status === 401) {
              this.store.dispatch(
                new ServerLogin(x.serverDto, {
                  username: x.username,
                  password: x.password,
                })
              );
            }else if (error.status === 404){
              console.log("TODO");
            }
          }
        });
      }
    );
  }

}
