import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Resource } from '../../../models/resource.model';
import { buildServerBaseUrl } from './utils/server-base-url-builder';
import { ServerDto } from 'app/shared/dto/server/server.dto';

@Injectable()
export class SinkService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'sinks';
  }

  public getAll(server: ServerDto): Observable<Resource[]> {
    return this._http.get<Resource[]>(
      `${buildServerBaseUrl(server)}/${this.resource}`
    );
  }

  public get(server: ServerDto, sink: Resource): Observable<Resource> {
    return this._http.get<Resource>(
      `${buildServerBaseUrl(server)}/${this.resource}/${sink.name}`
    );
  }

  public delete(server: ServerDto, sink: Resource): Observable<any> {
    return this._http.delete<Resource>(
      `${buildServerBaseUrl(server)}/${this.resource}/${sink.name}`
    );
  }
}
