import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ServerDto } from 'app/shared/dto/server/server.dto';
import { buildServerBaseUrl } from './utils/server-base-url-builder';
import { IFunction } from 'app/shared/models/function.model';

@Injectable()
export class FunctionService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'functions';
  }

  getAll(serverDto: ServerDto): Observable<IFunction[]> {
    return this._http.get<IFunction[]>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}`
    );
  }
}
