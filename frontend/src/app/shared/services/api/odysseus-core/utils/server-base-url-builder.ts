import { ServerDto } from 'app/shared/dto/server/server.dto';

export function buildServerBaseUrl(serverDto: ServerDto): string {
  let url = `${serverDto.scheme}://${serverDto.host}`;

  if (serverDto.port) {
    url = `${url}:${serverDto.port}`;
  }

  return url;
}

export function buildServerWebSocketBaseUrl(serverDto: ServerDto): string {
  let url = `ws://${serverDto.host}`;

  if (serverDto.port) {
    url = `${url}:${serverDto.port}`;
  }

  return url;
}
