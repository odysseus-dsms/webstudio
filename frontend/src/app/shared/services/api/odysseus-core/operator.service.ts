import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { OperatorDto } from '../../../dto/operator/operator.dto';
import { buildServerBaseUrl } from 'app/shared/services/api/odysseus-core/utils/server-base-url-builder';
import { ServerDto } from 'app/shared/dto/server/server.dto';

@Injectable()
export class OperatorService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'operators';
  }

  findAllOperators(serverDto: ServerDto): Observable<OperatorDto[]> {
    return this._http.get<OperatorDto[]>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}`
    );
  }
}
