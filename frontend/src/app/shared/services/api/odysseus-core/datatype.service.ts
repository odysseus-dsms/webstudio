import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ServerDto } from 'app/shared/dto/server/server.dto';
import { buildServerBaseUrl } from './utils/server-base-url-builder';
import { Datatype } from 'app/shared/models/datatype.model';

@Injectable()
export class DatatypeService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'datatypes';
  }

  getAll(serverDto: ServerDto): Observable<Datatype[]> {
    return this._http.get<Datatype[]>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}`
    );
  }
}
