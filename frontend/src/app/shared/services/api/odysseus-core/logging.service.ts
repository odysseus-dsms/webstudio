import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ServerDto } from 'app/shared/dto/server/server.dto';
import { buildServerBaseUrl } from './utils/server-base-url-builder';

@Injectable()
export class LoggingService {
  constructor(private _http: HttpClient) {}

  public getAll(server: ServerDto): Observable<string> {
    return this._http.get(`${buildServerBaseUrl(server)}/services/log`, {
      responseType: 'text',
    });
  }
}
