import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ParserTokenDto } from '../../../dto/parser/token.dto';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { buildServerBaseUrl } from './utils/server-base-url-builder';
import { Parser } from 'app/shared/models/parser.model';

@Injectable()
export class ParserService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'parsers';
  }

  getAll(serverDto: ServerDto): Observable<Parser[]> {
    return this._http.get<Parser[]>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}`
    );
  }

  getAllTokens(
    serverDto: ServerDto,
    parser: string
  ): Observable<ParserTokenDto> {
    return this._http.get<ParserTokenDto>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/${parser}/tokens`
    );
  }
}
