import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ServerDto } from 'app/shared/dto/server/server.dto';
import { buildServerBaseUrl } from 'app/shared/services/api/odysseus-core/utils/server-base-url-builder';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { QueryDetails } from 'app/shared/dto/query/query-details.model';

@Injectable()
export class QueryService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'queries';
  }

  public getAll(serverDto: ServerDto): Observable<QueryPreview[]> {
    return this._http.get<QueryPreview[]>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}`
    );
  }

  public get(serverDto: ServerDto, queryId: number): Observable<QueryDetails> {
    return this._http.get<QueryDetails>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/${queryId}`
    );
  }

  public create(
    serverDto: ServerDto,
    query: QueryPreview
  ): Observable<QueryPreview> {
    return this._http.post<QueryPreview>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}`,
      query
    );
  }

  public changeState(
    serverDto: ServerDto,
    query: QueryPreview,
    state: 'RUNNING' | 'INACTIVE' | 'SUSPENDED'
  ): Observable<QueryPreview> {
    const queryState = {
      id: query.id,
      state: state,
    };

    return this._http.put<QueryPreview>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/${query.id}`,
      queryState
    );
  }

  public delete(serverDto: ServerDto, query: QueryPreview): Observable<any> {
    return this._http.delete<QueryPreview>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/${query.id}`
    );
  }
}
