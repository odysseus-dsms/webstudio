import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Token } from 'app/shared/models/token.model';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { buildServerBaseUrl } from './utils/server-base-url-builder';
import { EventWebSocket } from 'app/shared/models/event-web-socket.model';
import { OdysseusNodeMap } from 'app/shared/models/odysseus-node-map.model';
import { SystemloadCpu } from 'app/shared/models/systemload-cpu.model';
import { SystemloadMemory } from 'app/shared/models/systemload-memory.model';
import { SystemloadNetwork } from 'app/shared/models/systemload-network.model';

@Injectable()
export class ServicesService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'services';
  }

  public login(
    serverDto: ServerDto,
    credentials: { username: string; password: string }
  ): Observable<Token> {
    return this._http.post<Token>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/login`,
      credentials
    );
  }

  public renew(serverDto: ServerDto): Observable<Token> {
    return this._http.get<Token>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/session`
    );
  }

  public events(serverDto: ServerDto): Observable<EventWebSocket[]> {
    return this._http.get<EventWebSocket[]>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/events`
    );
  }

  public options(serverDto: ServerDto): Observable<EventWebSocket[]> {
    return this._http.options<EventWebSocket[]>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/login`
    );
  }

  public nodeManager(serverDto: ServerDto): Observable<OdysseusNodeMap> {
    return this._http.get<OdysseusNodeMap>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/information/odysseusnodemanager`
    );
  }

  public odysseusId(serverDto: ServerDto): Observable<any> {
    return this._http.get<any>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/information/odysseusid`
    );
  }

  public systemloadCpu(serverDto: ServerDto): Observable<SystemloadCpu> {
    return this._http.get<SystemloadCpu>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/information/systemload_cpu`
    );
  }

  public systemloadMem(serverDto: ServerDto): Observable<SystemloadMemory> {
    return this._http.get<SystemloadMemory>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/information/systemload_mem`
    );
  }

  public systemloadNet(serverDto: ServerDto): Observable<SystemloadNetwork[]> {
    return this._http.get<SystemloadNetwork[]>(
      `${buildServerBaseUrl(serverDto)}/${this.resource}/information/systemload_net`
    );
  }
}
