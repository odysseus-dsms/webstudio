import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Resource } from '../../../models/resource.model';
import { ServerDto } from 'app/shared/dto/server/server.dto';
import { buildServerBaseUrl } from './utils/server-base-url-builder';

@Injectable()
export class StreamService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'datastreams';
  }

  public getAll(server: ServerDto): Observable<Resource[]> {
    return this._http.get<Resource[]>(
      `${buildServerBaseUrl(server)}/${this.resource}`
    );
  }

  public get(server: ServerDto, stream: Resource): Observable<Resource> {
    return this._http.get<Resource>(
      `${buildServerBaseUrl(server)}/${this.resource}/${stream.name}`
    );
  }

  public delete(server: ServerDto, stream: Resource): Observable<any> {
    return this._http.delete<Resource>(
      `${buildServerBaseUrl(server)}/${this.resource}/${stream.name}`
    );
  }
}
