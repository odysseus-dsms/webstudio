import { Injectable } from '@angular/core';
import { webSocket } from 'rxjs/webSocket';
import { Observable } from 'rxjs';
import { EventWebSocket } from '../../../models/event-web-socket.model';
import { ServerDto } from '../../../dto/server/server.dto';
import { buildServerWebSocketBaseUrl } from './utils/server-base-url-builder';
import { QueryPreview } from 'app/shared/dto/query/query-preview.model';
import { QueryDetailOperator } from 'app/shared/dto/query/query-detail-operator.model';

@Injectable()
export class WebSocketService {
  connectToQueryAddedEvent(serverDto: ServerDto, sockets: EventWebSocket[]) {
    return this.connectToEventByName(serverDto, sockets, 'QUERY_ADDED');
  }

  connectToErrorEvent(serverDto: ServerDto, sockets: EventWebSocket[]) {
    return this.connectToEventByName(serverDto, sockets, 'ERROR_EVENT');
  }

  connectToPlanModificationEvent(
    serverDto: ServerDto,
    sockets: EventWebSocket[]
  ) {
    return this.connectToEventByName(serverDto, sockets, 'PLAN_MODIFICATION');
  }

  connectToSchedulerManagerEvent(
    serverDto: ServerDto,
    sockets: EventWebSocket[]
  ) {
    return this.connectToEventByName(serverDto, sockets, 'SCHEDULER_MANAGER');
  }

  connectToExecutorCommandEvent(
    serverDto: ServerDto,
    sockets: EventWebSocket[]
  ) {
    return this.connectToEventByName(serverDto, sockets, 'EXECUTOR_COMMAND');
  }

  connectToCompilerEvent(serverDto: ServerDto, sockets: EventWebSocket[]) {
    return this.connectToEventByName(serverDto, sockets, 'COMPILER');
  }

  connectToSessionEvent(serverDto: ServerDto, sockets: EventWebSocket[]) {
    return this.connectToEventByName(serverDto, sockets, 'SESSION');
  }

  connectToDataDictionaryEvent(
    serverDto: ServerDto,
    sockets: EventWebSocket[]
  ) {
    return this.connectToEventByName(serverDto, sockets, 'DATADICTIONARY');
  }

  connectToUserEvent(serverDto: ServerDto, sockets: EventWebSocket[]) {
    return this.connectToEventByName(serverDto, sockets, 'USER');
  }

  connectToQueryEvent(serverDto: ServerDto, sockets: EventWebSocket[]) {
    return this.connectToEventByName(serverDto, sockets, 'QUERY');
  }

  connectToSchedulingEvent(serverDto: ServerDto, sockets: EventWebSocket[]) {
    return this.connectToEventByName(serverDto, sockets, 'SCHEDULING');
  }

  connectToEventByName(
    serverDto: ServerDto,
    sockets: EventWebSocket[],
    name: string
  ) {
    return this.connectToEvent(
      serverDto,
      sockets.find((x) => x.type === name)
    );
  }

  connectToEvent(
    serverDto: ServerDto,
    socket: EventWebSocket
  ): Observable<any> {
    return webSocket({
      url: `${buildServerWebSocketBaseUrl(serverDto)}${socket.websocketUri}`,
      deserializer: ({ data }) => data,
    });
  }

  connectToQueryResult(
    serverDto: ServerDto,
    query: QueryPreview
  ): Observable<any> {
    const sockets = query.rootOperators[0].ports[0].websockets;
    const wsUri = sockets.find((socket) => socket.protocol === 'JSON').uri;

    return webSocket({
      url: `${buildServerWebSocketBaseUrl(serverDto)}${wsUri}`,
      deserializer: ({ data }) => data,
    });
  }

  connectToOperatorResult(
    serverDto: ServerDto,
    operator: QueryDetailOperator
  ): Observable<any> {
    const sockets = operator.ports[0].websockets;
    const wsUri = sockets.find((socket) => socket.protocol === 'JSON').uri;

    return webSocket({
      url: `${buildServerWebSocketBaseUrl(serverDto)}${wsUri}`,
      deserializer: ({ data }) => data,
    });
  }
}
