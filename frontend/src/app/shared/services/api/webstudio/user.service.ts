import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'environments/environment';
import { UserDto } from 'app/shared/dto/user/user.dto';

@Injectable()
export class UserService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'users';
  }

  public getAll(): Observable<UserDto[]> {
    return this._http.get<UserDto[]>(`${environment.apiUrl}/${this.resource}`);
  }

  public create(userDto: UserDto): Observable<any> {
    return this._http.post<UserDto>(
      `${environment.apiUrl}/${this.resource}`,
      userDto
    );
  }

  public find(username: string): Observable<UserDto> {
    return this._http.get<UserDto>(
      `${environment.apiUrl}/${this.resource}/${username}`
    );
  }

  public edit(username: string, userDto: UserDto): Observable<UserDto> {
    return this._http.put<UserDto>(
      `${environment.apiUrl}/${this.resource}/${username}`,
      userDto
    );
  }

  public delete(username: string): Observable<any> {
    return this._http.delete(
      `${environment.apiUrl}/${this.resource}/${username}`
    );
  }
}
