import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'environments/environment';
import { ServerDto } from 'app/shared/dto/server/server.dto';

@Injectable()
export class ServerService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'servers';
  }

  public getAll(): Observable<ServerDto[]> {
    return this._http.get<ServerDto[]>(
      `${environment.apiUrl}/${this.resource}`
    );
  }

  public create(server: ServerDto): Observable<ServerDto> {
    return this._http.post<ServerDto>(
      `${environment.apiUrl}/${this.resource}`,
      server
    );
  }

  public get(serverName: string): Observable<ServerDto> {
    return this._http.get<ServerDto>(
      `${environment.apiUrl}/${this.resource}/${serverName}`
    );
  }

  public edit(serverName: string, server: ServerDto): Observable<ServerDto> {
    return this._http.put<ServerDto>(
      `${environment.apiUrl}/${this.resource}/${serverName}`,
      server
    );
  }

  public delete(serverName: string): Observable<any> {
    return this._http.delete(
      `${environment.apiUrl}/${this.resource}/${serverName}`
    );
  }

  public hasName(value: string): Observable<boolean> {
    return this.getAll().pipe(
      map((servers) => {
        const matches = servers.filter((server) => server.name === value);
        return matches.length > 0 ? true : false;
      })
    );
  }
}
