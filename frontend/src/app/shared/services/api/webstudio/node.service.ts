import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'environments/environment';
import { NodeDetails } from '../../../dto/node/node-details';
import { NodeUpdateRequest } from '../../../dto/node/node-update.request';
import { NodeCreateRequest } from '../../../dto/node/node-create.request';
import { ServerDto } from 'app/shared/dto/server/server.dto';

@Injectable()
export class NodeService {
  private resource: string;

  constructor(private _http: HttpClient) {
    this.resource = 'nodes';
  }

  createNode(
    serverDto: ServerDto,
    path: string,
    requestBody: NodeCreateRequest
  ): Observable<NodeDetails> {
    return this._http.post<NodeDetails>(
      `${environment.apiUrl}/${this.resource}/${encodeURIComponent(
        serverDto.name
      )}/${this.escapePath(path)}`,
      requestBody
    );
  }

  findNode(
    serverDto: ServerDto,
    path: string,
    parents: boolean = false
  ): Observable<NodeDetails> {
    return this._http.get<NodeDetails>(
      `${environment.apiUrl}/${this.resource}/${encodeURIComponent(
        serverDto.name
      )}/${this.escapePath(path)}`,
      {
        params: {
          parents: `${parents}`,
        },
      }
    );
  }

  downloadNode(serverDto: ServerDto, path: string): Observable<Blob> {
    return this._http.get(
      `${environment.apiUrl}/${this.resource}/${encodeURIComponent(
        serverDto.name
      )}/${this.escapePath(path)}`,
      {
        responseType: 'blob',
        headers: {
          'Content-Type': 'application/octet-stream',
        },
      }
    );
  }

  updateNode(
    serverDto: ServerDto,
    path: string,
    requestBody: NodeUpdateRequest
  ): Observable<NodeDetails> {
    return this._http.patch<NodeDetails>(
      `${environment.apiUrl}/${this.resource}/${encodeURIComponent(
        serverDto.name
      )}/${this.escapePath(path)}`,
      requestBody
    );
  }

  deleteNode(serverDto: ServerDto, path: string): Observable<any> {
    return this._http.delete<any>(
      `${environment.apiUrl}/${this.resource}/${encodeURIComponent(
        serverDto.name
      )}/${this.escapePath(path)}`
    );
  }

  private escapePath(path) {
    if (path) {
      return path
        .split('/')
        .map((x) => encodeURIComponent(x))
        .join('/');
    }

    return '';
  }
}
