import { Injectable } from '@angular/core';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';

@Injectable()
export class MessageService {
  constructor(private snackBar: MatSnackBar) {}

  public showMessage(msg: string, cssClass?: string): void {
    this.snackBar.open(msg, '', {
      panelClass: ['notification', cssClass ? cssClass : undefined],
      horizontalPosition: 'center',
      verticalPosition: 'top',
      duration: 5000
    });
  }
}
