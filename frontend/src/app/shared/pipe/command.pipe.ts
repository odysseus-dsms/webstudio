import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'command' })
export class CommandPipe implements PipeTransform {
  transform(charCode: string, shift: boolean = false): string {
    let result = navigator.platform.match('Mac') ? '⌘' : 'Ctrl';

    if (shift) {
      result += '+Shift';
    }

    return (result += `+${charCode}`);
  }
}
