import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { UserComponent } from './user.component';
import { UserDialogComponent } from './dialog/user-dialog.component';
import { UserService } from '../shared/services/api/webstudio/user.service';
import { AdminGuard } from 'app/util/guards/admin.guard';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [UserComponent, UserDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([{ path: '', component: UserComponent }]),
    SharedModule,
  ],
})
export class UserModule {}
