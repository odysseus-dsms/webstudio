import { Component, OnInit, ViewChild } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatLegacyPaginator as MatPaginator } from '@angular/material/legacy-paginator';
import { MatSort } from '@angular/material/sort';
import { MatLegacyTableDataSource as MatTableDataSource } from '@angular/material/legacy-table';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { DialogService } from 'app/shared/dialogs/dialog.service';
import { MessageService } from 'app/shared/services/message.service';
import { UserDialogComponent } from './dialog/user-dialog.component';
import { UserService } from '../shared/services/api/webstudio/user.service';
import { UserDto } from 'app/shared/dto/user/user.dto';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = ['username', 'email', 'role', 'actions'];
  dataSource = new MatTableDataSource<UserDto>();

  constructor(
    public dialog: MatDialog,
    private dialogService: DialogService,
    private messageService: MessageService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.updateUsers();
  }

  private updateUsers(): void {
    this.userService.getAll().subscribe((servers) => {
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.data = servers;
    });
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public createUser(): void {
    this.ensureDialog(
      { username: '', email: '', role: '' },
      'create'
    ).subscribe((user: UserDto) =>
      this.userService.create(user).subscribe(() => {
        this.updateUsers();
        this.messageService.showMessage('The user was created successfully.');
      })
    );
  }

  public editUser(userDto: UserDto): void {
    let username = userDto.username;
    this.ensureDialog(userDto, 'edit').subscribe((response: UserDto) =>
      this.userService.edit(username, response).subscribe(() => {
        this.updateUsers();
        this.messageService.showMessage('The user was edited successfully.');
      })
    );
  }

  public deleteUser(userDto: UserDto): void {
    this.dialogService
      .confirm('Delete ' + userDto.username, 'Do you want to continue?')
      .subscribe((res) => {
        if (res) {
          this.userService.delete(userDto.username).subscribe(() => {
            this.updateUsers();
            this.messageService.showMessage(
              'The user was deleted successfully.'
            );
          });
        }
      });
  }

  private ensureDialog(userDto: UserDto, mode: string): Observable<UserDto> {
    return this.dialog
      .open<UserDialogComponent, any>(UserDialogComponent, {
        width: '410px',
        data: {
          userDto,
          mode,
          existingNames: this.dataSource.data.map((x) => x.username),
        },
      })
      .afterClosed()
      .pipe(filter((userDto: UserDto): boolean => !!userDto));
  }
}
