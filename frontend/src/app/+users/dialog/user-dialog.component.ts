import { Component, Inject } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA, MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog';
import { stringValidator } from '../../shared/validators/user-dialog.validator';

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
})
export class UserDialogComponent {
  form: UntypedFormGroup;
  mode: string;

  constructor(
    public dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private readonly fb: UntypedFormBuilder
  ) {
    this.mode = data.mode;
    this.form = this.fb.group({
      email: this.fb.control(data.userDto.email, [
        Validators.required,
        Validators.email,
      ]),
      username: this.fb.control(data.userDto.username, [
        Validators.required,
        stringValidator(
          data.existingNames.filter((x) => x !== data.userDto.username)
        ),
      ]),
      password: this.fb.control(data.userDto.password),
      role: this.fb.control(data.userDto.role, [Validators.required]),
    });

    if (this.mode === 'create') {
      this.form.controls.password.setValidators(Validators.required);
    }
  }

  public cancel(): void {
    this.dialogRef.close();
  }

  public save(): void {
    if (this.form.valid) {
      this.dialogRef.close(this.form.getRawValue());
    }
  }
}
