export const environment = {
  production: true,
  downloadToken: 'CHANGEME',
  authIssuer: 'http://localhost:3000',
  apiUrl: 'http://localhost:3000/v1',
};
